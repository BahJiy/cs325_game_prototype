"use strict"
// @version 1.8

const dotWidth = 50;
const dotHeight = 50;

/**
 * Do an in-place removal from the array
 * @param {any[]} array
 * @param {Number} index
 */
let remove = ( array, index ) => {
	return array.splice( index, 1 );
}

class DotKey {
	/**
	 * @param {Number} x - x index
	 * @param {Number} y - y index
	 * @param {Number} posX - The starting position X
	 * @param {Number} posY - The starting position Y
	 * @param {Number} key - The key to trigger this dot
	 */
	constructor (x, y, posX, posY, key){
		this.sprite = game.add.image( posX + ( x * dotWidth ), posY + ( y * dotHeight ), "dot" );
		this.key = game.input.keyboard.addKey( key );
		this.enable = false;
		this.pen = 0;

		this.sprite.tint = 0x0000FF;

		this.key.onDown.add( () => {
			if (this.enable) {
				// switch from red to white
				this.sprite.tint = this.sprite.tint == 0xFF0000 ? 0xFFFFFF : 0xFF0000;
			} else {
				// if this dot is not even allowed to be trigger, there iis a penalty
				this.pen += 1;
			}
		});
	}

	get penalty () { return this.pen;}
	get trigger () { return this.sprite.tint == 0xFF0000; }

	/**
	 * @param {Boolean} enable - Allow this dot to change color?
	 * @param {Boolean} red - Is this dot initially red?
	 */
	reset (enable, red = false, blue = false) {
		this.enable = enable;
		this.sprite.tint = red ? 0xFF0000 : 0xFFFFFF;
		this.sprite.tint = blue ? 0x0000FF : this.sprite.tint;
		this.pen = 0;
	}
}

/**
 * Create a dot and set its value depending if it is a red dot or not
 * @param {Boolean} red - Is the dot red?
 * @param {Number} x - X index into the grid
 * @param {Number} y - Y index into the grid
 * @param {Number} posX - The starting X location of grid
 * @param {Number} posy - The starting Y location of grid
 * @param {Number} changeColor - Allow dots to change color when clicked?
 */
class Dot {
	constructor ( red, x, y, posX, posY, changeColor, soundAllow ) {
		this.sprite = game.add.image( posX + ( x * dotWidth ), posY + ( y * dotHeight ), "dot" );
		this.click = false;
		this.red = red;
		this.pen = 0;
		this.sprite.inputEnabled = true;
		if ( red ) {
			this.sprite.tint = 0xFF0000;
		}
		this.sound = game.add.audio( "click", 1.0, false );
		this.sprite.events.onInputUp.add( () => {
			if (soundAllow) this.sound.play();
			if ( !this.click ) { this.click = true; } else { this.pen += 1; }
			if ( changeColor && this.red) {this.sprite.tint = 0x0000FF;}
		} );

		this.pos = new Phaser.Point( x, y );

	}
	destroy () { this.sprite.destroy(); }
	get clicked () { return this.click; }
	get position () { return this.pos; }
	get penalty () { return this.pen; }
	get type () { return this.red; }
}

class DotManager {
	constructor () {
		/** @type {Dot[][]} */
		this.grid = [];
		this.pen = 0;
		this.status = undefined;
		this.allowCheck = false;
	}
	DisableCheck () { this.allowCheck = false; }

	/**
	 * @param {Dots[][]} grid - The grid to check dots
	 */
	CheckDots () {
		if (!this.allowCheck || this.status == DotManager.TYPE.REDCLICKED) {
			return this.status;
		} else {
			let finish = undefined
			this.status = DotManager.TYPE.NORED;
			this.pen = 0;
			this.grid.forEach(row => row.forEach(cell => {
				if (cell.type) {
					finish = finish == undefined ?
						cell.clicked : finish && cell.clicked;
					this.status = DotManager.TYPE.REDNOTCLICKED;
				}
				this.pen += cell.penalty;
			}));
			if (finish) {
				this.status = DotManager.TYPE.REDCLICKED;
			}
			return this.status;
		}
	}

	/**
	 * Clear out the grid
	 */
	DeleteGrid ( ) {
		this.grid.forEach(row => row.forEach(cell => cell.destroy()));
		this.grid = [];
	}

	/**
	 * @param {Number} amount - Number of dots to spawn
	 * @param {Number} posX - Starting X location of grid
	 * @param {Number} posY - Starting Y location of grid
	 * @param {Number} changeColor - Allow dot to change color when clicked?
	 */
	CreateDots ( amount, posX, posY, changeColor, sound ) {
		this.status = undefined;
		// Reinitialize the World Grid
		let tempGrid = []
		for ( let i = 0; i < 7; i++ ) {
			this.grid[ i ] = [];
			for ( let j = 0; j < 7; j++ ) {
				tempGrid.push( [ i, j ] );
			}
		}
		console.log( "Created a Grid of " + tempGrid.length );

		let redCount = 0;
		// Math: index - floor(index / amount) * amount
		for ( let i = 0; i < amount; i++ ) {
			let indexSingle = game.rnd.integerInRange( 0, tempGrid.length - 1 );
			let row = tempGrid[ indexSingle ][ 0 ];
			let col = tempGrid[ indexSingle ][ 1 ];
			let status = game.rnd.integerInRange( 0, 100 ) > 25 ? false : true;
			redCount += status ? 1 : 0;

			console.log( "\tSpawning a Dot " + status + " at (" + row + ", " + col + ")" );
			this.grid[ row ][ col ] = new Dot( status, col, row, posX, posY, changeColor, sound );
			remove( tempGrid, indexSingle ); // remove the used cell from temp
		}
		this.allowCheck = true;
		return redCount;
	}

	get penalty () { return this.pen; }

	static get TYPE () {
		return Object.freeze( {
			NORED: 0,
			REDCLICKED: 1,
			REDNOTCLICKED: 2,
		} );
	}
}
