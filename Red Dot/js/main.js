"use strict";
let game = new Phaser.Game( 1000, 600, Phaser.Canvas, 'game' );
window.onload = function () {

	//	Create your Phaser game and inject it into the 'game' div.
	//	We did it in a window.onload event, but you can do it anywhere (requireJS load, anonymous function, jQuery dom ready, - whatever floats your boat)
	/** @type {Phaser.Game} */

	let WebFontConfig = {
		active: function () { game.time.events.add( Phaser.Timer.SECOND, null, this ); },
		google: {
			families: [ 'Inconsolata', 'Gloria Hallelujah' ]
		}
	};

	// Add any shared objects
	let shared = {changeColor: false, sound: false};

	// Add the states
	game.state.add( 'Boot', GameStates.makeBoot( game ) );
	game.state.add( 'MainMenu', GameStates.makeMainMenu( game, shared ) );
	game.state.add( 'Instruction', GameStates.makeInstruction( game, shared ) );
	game.state.add( 'Game_Norm', GameStates.makeGame_Normal( game, shared ) );
	game.state.add( 'Game_MirrorClick', GameStates.makeGame_MirClick( game, shared ) );
	game.state.add( 'Game_MirrorKey', GameStates.makeGame_MirKey( game, shared ) );

	game.state.start( 'Boot' );

};
window.onbeforeunload = function ( e ) {
	game.world.removeAll();
	game.destroy();
};
