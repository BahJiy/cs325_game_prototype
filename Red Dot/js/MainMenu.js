"use strict";

/** @param {Phaser.Game} game */
GameStates.makeMainMenu = function ( game, shared ) {
	let group = [];
	let esc = null;
	let gMode, gameNorm, gameKey, instruction, setting, blueSet, soundSet, title;

	return {
		create: function () { 
			game.camera.flash( 0x000000, 1000, true, 1 );
			esc = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
			esc.enabled = true;
			game.add.image(0,0, "main");
			title = game.add.text(game.world.centerX, game.world.centerY - 150, "Red Dot", {
				font: "75px Gloria Hallelujah",
				fill: "#FF0000",
				stroke: "#00FFFF",
				strokeThickness: 5
			});
			gMode = game.add.text(game.world.centerX - 200, game.world.centerY, "Game Mode:", {
				font: "30px Inconsolata",
				fill: "#FFFFFF",
				stroke: "#000000",
				strokeThickness: 5
			});
			gameNorm = game.add.text(game.world.centerX, game.world.centerY - 30,
				"Normal Version", {
					font: "30px Inconsolata",
					fill: "#FF9910",
					stroke: "#000000",
					strokeThickness: 5
				});
			gameKey = game.add.text(game.world.centerX, game.world.centerY + 30,
				"Mirror Version", {
					font: "30px Inconsolata",
					fill: "#FF2399",
					stroke: "#000000",
					strokeThickness: 5
				});
			instruction = game.add.text(game.world.centerX, game.world.centerY + 120,
				"Instruction", {
					font: "30px Inconsolata",
					fill: "#FFFFFF",
					stroke: "#000000",
					strokeThickness: 5
				});
			setting = game.add.text(game.world.centerX, game.world.centerY + 170, 
				"Settings", {
					font: "30px Inconsolata",
					fill: "#FFFFFF",
					stroke: "#000000",
					strokeThickness: 5
				});
			blueSet = game.add.text(game.world.centerX, game.world.centerY - 30,
				"Allow Blue Dots in Normal: " + shared.changeColor, {
					font: "30px Inconsolata",
					fill: "#FF9910",
					stroke: "#000000",
					strokeThickness: 5
				});
			soundSet = game.add.text(game.world.centerX, game.world.centerY + 30,
				"Allow Sound: " + shared.sound, {
					font: "30px Inconsolata",
					fill: "#FF2399",
					stroke: "#000000",
					strokeThickness: 5
				});
			title.anchor.setTo(0.5);
			setting.anchor.setTo(0.5);
			instruction.anchor.setTo(0.5);
			setting.inputEnabled = instruction.inputEnabled = gameNorm.inputEnabled = gameKey.inputEnabled = true;
			gameNorm.events.onInputDown.add(() => {
				game.state.start("Game_Norm")
			}, this);
			gameKey.events.onInputDown.add(() => {
				game.state.start("Game_MirrorKey")
			}, this);
			instruction.events.onInputDown.add(() => {
				game.state.start("Instruction")
			}, this);
			setting.events.onInputDown.add(() => {
				group.forEach(text => {text.visible = false; text.inputEnabled = false});
				blueSet.inputEnabled = soundSet.inputEnabled = true;
				blueSet.visible = soundSet.visible = true;
			}, this);
			esc.onDown.add( () => {
				// group does not like enabling inputEnabled for some reason
				group.forEach(text => {text.visible = true;});
				gameNorm.inputEnabled = true;
				gameKey.inputEnabled = true;
				instruction.inputEnabled = true;
				setting.inputEnabled = true;
				blueSet.inputEnabled = soundSet.inputEnabled = false;
				blueSet.visible = soundSet.visible = false;
			}, this);
			blueSet.anchor.setTo(0.5);
			soundSet.anchor.setTo(0.5);
			blueSet.inputEnabled = soundSet.inputEnabled = false;
			blueSet.visible = soundSet.visible = false;
			blueSet.events.onInputDown.add(() => {
				shared.changeColor = !shared.changeColor;
				blueSet.setText("Allow Blue Dots in Normal: " + shared.changeColor);
			}, this);
			soundSet.events.onInputDown.add(() => {
				shared.sound = !shared.sound;
				soundSet.setText("Allow Sound: " + shared.sound);
			}, this);
			group.push(gameNorm);
			group.push(gameKey);
			group.push(instruction);
			group.push(gMode);
			group.push(setting);
		},

		render: function () {
			game.debug.text( "Red Dot v1.5", game.width - 175, game.height - 25 );
		},

		update: function () { }
	};
};
