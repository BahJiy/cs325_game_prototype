"use strict";

/** @param {Phaser.Game} game */
GameStates.makeInstruction = function ( game, shared ) {
	const size = [1000, 600];
	let coord = [];

	/** @type {Phaser.Image} */
	let image = null;
	let IC = null;
	return {
		create: function () {
            game.camera.flash( 0x14AF89, 1000, true, 1 );
			image = game.add.image(0,0, "instruct");
			let esc = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
			esc.enabled = true;
			esc.onDown.add( () => {
				game.state.start("MainMenu");
			});
			IC = new InputControl(game, [Phaser.Keyboard.UP, Phaser.Keyboard.DOWN, 
				Phaser.Keyboard.LEFT, Phaser.Keyboard.RIGHT]);

			coord = [0,0];
		},
		update: function () {
			IC.update(game);
			if (IC.isDown(Phaser.Keyboard.UP) && coord[1] > 0) {
				coord = [0, coord[1] - 1];
			} else if (IC.isDown(Phaser.Keyboard.DOWN) && coord[1] < 2) {
				coord = [0, coord[1] + 1];
			} else if (IC.isDown(Phaser.Keyboard.LEFT) && coord[0] > 0) {
				coord = [coord[0] - 1, coord[1]];
			} else if (IC.isDown(Phaser.Keyboard.RIGHT) && coord[0] < 3) {
				coord = [coord[0] + 1, coord[1]];
			}

			// Some animations
			let finalPost = [-coord[0] * size[0], -coord[1] * size[1]];
			let x = image.x + (image.position.x != finalPost[0] ? 
				(finalPost[0] - image.x) / 20 : image.x);
			let y = image.y + (image.y != finalPost[1] ? 
				(finalPost[1] - image.y) / 20 : image.y);

			image.position.setTo(x, y);
		}
	}
}
