"use strict";

let GameStates = {};
/**
 *
 * @param {Phaser.Game} game
 */
GameStates.makeBoot = function ( game ) {
	return {
		init: function () {
			game.stage.disableVisibilityChange = true;
			game.scale.pageAlignHorizontally = true;
			game.scale.pageAlignVertically = true;
		},

		preload: function () {
			game.load.image( "dot", "assets/dot.png" );
			game.load.image( "main", "assets/Main.png" );
			game.load.image( "instruct", "assets/Instruction.png" );
			game.load.image( "gameN", "assets/GameNorm.png" );
			game.load.image( "gameK", "assets/GameKey.png" );
			game.load.audio( "click", "assets/click.wav" );
		},

		create: function () {
			// Go to the actually loader to load the rest
			game.state.start("MainMenu");
			//game.state.start( 'Game_MirrorKey' );
			//game.state.start( 'Game_Norm' );
		}
	};
};
