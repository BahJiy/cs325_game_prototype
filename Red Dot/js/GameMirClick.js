"use strict"

/** @param {Phaser.Game} game */
GameStates.makeGame_MirClick = function ( game, shared ) {
	return {
		create: function () { },

		render: function () {
			game.debug.text( "Mouse X:" + game.input.activePointer.worldX + " Y:" + game.input.activePointer.worldY, 25, 25 );
		},

		update: function () { }
	};
}
