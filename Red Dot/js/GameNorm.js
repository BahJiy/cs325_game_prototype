"use strict"

/**
 * @param {Phaser.Game} game
 * @param {any} shared
 */
GameStates.makeGame_Normal = function ( game, shared ) {

	let score = 0, upToScore = 0, amount = 10, playCount = 1;
	let startTime = -1, endTime = -1, offset = 3000;

	let gameEnd = false;

	/** @type {DotManager} */
	let DM = null;
	/** @type {Phaser.Text} */
	let noDotButton = null;
	/** @type {Phaser.Text} */
	let readyButton = null;
	/** @type {Phaser.Text} */
	let scoreText = null;
	/** @type {Phaser.Text} */
	let animateText = null;

	let animateScore = ( score ) => {
		let symbol = score > 0 ? "+" : "";
		animateText.setText( symbol + score );
		animateText.y = 0;
		animateText.alpha = 1;
		game.add.tween( animateText ).to( { y: 20, alpha: 0 }, 750, Phaser.Easing.Default, true )
		upToScore += score;
	}

	return {
		create: function () {
            game.camera.flash( 0xAC89FF, 1500, true, 1 );
			game.add.image(0,0, "gameN");
			DM = new DotManager();
			animateText = game.add.text( 140, 0, "", {
				fill: "#FFFFFF",
				fontSize: 30
			} );
			scoreText = game.add.text( 50, 25, "", {
				fill: "#FFFFFF",
				fontSize: 30
			} );
			readyButton = game.add.text( game.world.centerX, game.world.centerY,
				"Click Here to Start", {
					fill: "#FFFFFF",
					fontSize: 30
				} );
			noDotButton = game.add.text( game.world.centerX, 30,
				"No Red Dots", {
					fill: "#FFFFFF",
					fontSize: 30
				} );
			readyButton.anchor.setTo( 0.5 );
			readyButton.inputEnabled = true;
			readyButton.events.onInputUp.add( () => {
				let redCount = DM.CreateDots( amount, 325, 125, shared.changeColor, shared.sound )
				offset = 3000 + Math.floor( redCount / 5 ) * 1500;
				endTime = -1;
				playCount++;
				noDotButton.inputEnabled = noDotButton.visible = true;
				readyButton.inputEnabled = readyButton.visible = false;
				startTime = game.time.now;
			} );

			noDotButton.anchor.setTo( 0.5 );
			noDotButton.inputEnabled = noDotButton.visible = false;
			noDotButton.events.onInputUp.add( () => {
				endTime = game.time.now;
				console.log( "No Dots" );
				gameEnd = true;
			} );
			let esc = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
			esc.enabled = true;
			esc.onDown.add( () => {
				game.state.start("MainMenu");
			});
		},

		render: function () {
			game.debug.text( "Game Normal v2.0", game.width - 175, game.height - 25 );
		},

		update: function () {
			if ( !readyButton.visible &&
				( gameEnd || DM.CheckDots() == DotManager.TYPE.REDCLICKED ) ) {
				endTime = endTime == -1 ? game.time.now : endTime;
				DM.DisableCheck( false );
				let dotStatus = DM.CheckDots();
				let adjustScore = 0;
				if ( dotStatus == DotManager.TYPE.REDCLICKED ||
					( dotStatus == DotManager.TYPE.NORED && gameEnd ) ) {
					adjustScore += Math.floor( ( startTime + offset - endTime ) / 5 );
					adjustScore -= DM.penalty * 25;
				} else {
					adjustScore -= Math.floor( ( startTime + 3000 - endTime ) / 7 );
				}
				animateScore( adjustScore );
				startTime = endTime = -1;
				readyButton.inputEnabled = readyButton.visible = true;
				noDotButton.inputEnabled = noDotButton.visible = false;
				gameEnd = false;
				DM.DeleteGrid();

				if ( adjustScore > 300 ) {
					amount += amount < 49 ? 2 : 0;
				} else if ( adjustScore < -150 ) {
					amount -= amount > 10 ? 5 : 0;
				}
			}

			score += score != upToScore ? Math.ceil( ( upToScore - score ) / 100 ) : 0;
			scoreText.setText( "Score: " + score + 
				"\nAverage: " + Math.floor(upToScore / playCount));
		}
	};
}
