"use strict"

/** @param {Phaser.Game} game */
GameStates.makeGame_MirKey = function ( game, shared ) {
	let score = 0, upToScore = 0, amount = 10, penalty = 0, playCount = 1;
	let startTime = -1, endTime = -1, level = 1, redCount = 0;

	let running = false, gameEnd = false;

	/** @type {Phaser.Key} */
	let enterKey = null;

	/** @type {DotKey[]} */
	let rightDots = null;
	/** @type {DotKey[]} */
	let leftDots = null;
	/** @type {Phaser.Text} */
	let readyButton = null;
	/** @type {Phaser.Text} */
	let scoreText = null;
	/** @type {Phaser.Text} */
	let animateText = null;
	/** @type {Phaser.Text} */
	let animateInfo = null;

	let animateScore = ( score ) => {
		let symbol = score > 0 ? "+" : "";
		animateText.setText( symbol + score );
		animateText.y = 0;
		animateText.alpha = 1;
		game.add.tween( animateText ).to( { y: 20, alpha: 0 }, 750, Phaser.Easing.Default, true )

		animateInfo.alpha = 1;
		let text = "On Par";
		if (score < -150) {
			text = "Horrible!";
		} else if (score < 0) {
			text = "Slow!";
		} else if (score > 150) {
			text = "Amazing!";
		} else {
			text = "Good!";
		}
		animateInfo.setText(text);
		game.add.tween( animateInfo ).to( { alpha: 0 }, 750, Phaser.Easing.Default, true )

		upToScore += score;
	}

	/**
	 * Set up the board by disabling and enabling dots
	 */
	let establish = () => {
		let side = game.rnd.integerInRange(1,100) < 50 ? leftDots : rightDots;
		redCount = 0;
		leftDots.forEach(cell => cell.reset(true));
		rightDots.forEach(cell => cell.reset(true));
		side.forEach(cell => {
			let red = game.rnd.integerInRange(1,100) <= 45 ? true : false;
			redCount += red ? 1 : 0;
			cell.reset(false, red);
		});
	}

	/**
	 * Check if the two dot lists are equal
	 */
	let dotEquals = (forceCheck = false) => {
		let equal = undefined;
		penalty = 0;
		for (let i = 0; i < leftDots.length; i++ ){
			if (!forceCheck && !leftDots[i].trigger && !rightDots[i].trigger) {
				// We don't care about white == white
				// The is the player's job
			} else {
				equal = equal == undefined ? (leftDots[i].trigger == rightDots[i].trigger) :
				equal && (leftDots[i].trigger == rightDots[i].trigger);
			}
			penalty += leftDots[i].penalty + rightDots[i].penalty;
		}
		if (startTime != -1) {
			return equal;
		} return false;
	}

	return {
		create: function () {
            game.camera.flash( 0x2988FA, 1500, true, 1 );
			game.add.image(0,0,"gameK");
			animateInfo = game.add.text( game.world.centerX, game.world.centerY - 100, "", {
				fill: "#FFFFFF",
				fontSize: 30
			} );
			animateText = game.add.text( 140, 0, "", {
				fill: "#FFFFFF",
				fontSize: 30
			} );
			scoreText = game.add.text( 50, 25, "", {
				fill: "#FFFFFF",
				fontSize: 30
			} );
			readyButton = game.add.text( game.world.centerX, game.world.centerY + 15,
				"Press Enter to Start", {
					fill: "#FFFFFF",
					fontSize: 30
				} );
			animateInfo.anchor.setTo(0.5);
			readyButton.anchor.setTo( 0.5 );

			leftDots = [
				new DotKey( 0, 0, 150, 275, Phaser.Keyboard.A ),
				new DotKey( 1, 0, 150, 275, Phaser.Keyboard.S ),
				new DotKey( 2, 0, 150, 275, Phaser.Keyboard.D ),
				new DotKey( 3, 0, 150, 275, Phaser.Keyboard.F )
			];
			rightDots = [
				new DotKey( 0, 0, 650, 275, Phaser.Keyboard.J ),
				new DotKey( 1, 0, 650, 275, Phaser.Keyboard.K ),
				new DotKey( 2, 0, 650, 275, Phaser.Keyboard.L ),
				new DotKey( 3, 0, 650, 275, Phaser.Keyboard.COLON )
			];

			enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
			enterKey.enabled = true;
			enterKey.onDown.add( () => {
				if (running){ 
					// game is running but player press enter meaning there are no red dots
					gameEnd = true
				} else {
					// start the game
					readyButton.visible = false;
					playCount++;
					establish();
					running = true; // Change state of the game
					startTime = game.time.now;
				}
			});
			let esc = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
			esc.enabled = true;
			esc.onDown.add( () => {
				game.state.start("MainMenu");
			});
		},

		render: function () {
			game.debug.text( "Game Key v2.0", game.width - 175, game.height - 25 );
		},

		update: function () {
			if (startTime != -1 && (gameEnd || dotEquals())) {
				endTime = game.time.now;
				let adjustScore = 0;
				let adjustOffset = 850 * (1 + redCount / 6) - level * 40;
				console.log("gameEnd: " + gameEnd);
				console.log("startTime: " + startTime + " | offset: " + adjustOffset + 
					" | endTime: " + endTime + " | penalty: " + penalty);
				if (dotEquals(true)) {
					adjustScore += Math.floor( ( startTime + adjustOffset - endTime ));
					adjustScore -= penalty * 15;
					adjustScore *= 1 + (level - 1) / 8;
				} else {
					adjustScore -= Math.ceil(adjustOffset * (0.1 + level / 8) );
				}

				console.log("AdjustScore = " + adjustScore);
				
				// Make it easier or harder
				if ( adjustScore > 175 ) {
					level += level < 5 ? 1 : 0;
				} else if ( adjustScore < -150 ) {
					level -= level > 1 ? 1 : 0;
				}

				animateScore( Math.ceil(adjustScore) );
				startTime = endTime = -1;
				gameEnd = false;
				readyButton.visible = true;
				running = false;

				leftDots.forEach(cell => cell.reset(false, false, true));
				rightDots.forEach(cell => cell.reset(false, false, true));
			}

			score += score != upToScore ? Math.ceil( ( upToScore - score ) / 100 ) : 0;
			scoreText.setText( "Score: " + score + " | Level: " + level + 
				"\nAverage: " + Math.floor(upToScore / playCount));
		}
	};
}
