"use strict";
/**
 * 
 * @param {Phaser.Game} game 
 * @param {*} shared 
 */
GameStates.makeBegin = function (game, shared) {
    /** @type{Object.<string, InputControl>} */
    let inputs = null;
    /** @type {TypeWriter} */
    let typewriter = null;
    /** @type {{start: boolean, text: [[string, string, boolean]], textIndex: number, textFinish: boolean}} */
    let storyInfo = { start: false, textIndex: 0, textFinish: false }

    // current background
    /** @type {Phaser.Sprite} */
    let curBack = null;

    function goToStage() {
        game.camera.fade(0x0, 2500, true);
        // Clear out memory
        typewriter = null;
        inputs = null;
        storyInfo = null;
        curBack.destroy();
        curBack = null;
        // We have no game yet
        game.state.start('Game');
        shared.clickCount = 0;
        shared.backtrack = false;
    }

    /** Contains the logic for Story 1 */
    function story() {
        if (!storyInfo.start) {
            story.part = 0;
            storyInfo.start = true;
        }
        if (game.camera.scale.x < 1.3) {
            game.camera.scale.x += 0.005;
            game.camera.scale.y += 0.005;
            game.camera.x += 100;
            game.camera.y += 5;
            return true;
        }
        if (typewriter.finish) {
            // Should probable optimize this
            switch (story.part) {
                case 0:
                    typewriter.changeMessageList(story1TextA);
                    typewriter.startDisplay(true);
                    story.part += 1;
                    break;
                case 1:
                    if (story.wait == null) {
                        story.wait = game.time.now + 3500;
                        story.newBack = game.add.sprite(0, 0, "nolight_house")
                        story.newBack.alpha = 0;
                        game.world.sendToBack(story.newBack);
                        game.add.tween(curBack).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
                        game.add.tween(story.newBack).to({ alpha: 1 }, 2000, Phaser.Easing.Linear.None, true);
                    }
                    if (game.time.now < story.wait) {
                        break;;
                    }

                    curBack.destroy();
                    curBack = story.newBack;
                    typewriter.changeMessageList(story1TextB);
                    typewriter.startDisplay(true);
                    story.wait = null;
                    story.part += 1;
                    break;
                case 2:
                    if (story.wait == null) {
                        story.wait = game.time.now + 3000;
                        story.newBack = game.add.sprite(0, 0, "morg_house")
                        story.newBack.alpha = 0;
                        game.world.sendToBack(story.newBack);
                        game.add.tween(curBack).to({ alpha: 0 }, 4000, Phaser.Easing.Linear.None, true);
                        game.add.tween(story.newBack).to({ alpha: 1 }, 5000, Phaser.Easing.Linear.None, true);
                    }
                    if (game.time.now < story.wait) {
                        break;
                    }

                    curBack.destroy();
                    curBack = story.newBack;
                    typewriter.changeMessageList(story1TextC);
                    typewriter.startDisplay(true);
                    story.part += 1;
                    break;
                default: return false;
            }
        }
        return true;
    }

    return {

        create: function () {
            curBack = game.add.sprite(0, 0, 'light_house');
            typewriter = new TypeWriter(game);
        },

        render: function () {
            game.debug.text("Mouse Pos: " + game.input.mousePointer.x + " | " + game.input.mousePointer.y, 0, 25);
        },

        update: function () {
            // update all the inputs controls
            if (!story())
                goToStage();
            else
                typewriter.update();
        }
    };
};
