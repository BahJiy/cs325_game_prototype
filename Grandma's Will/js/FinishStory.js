"use strict";
/**
 *
 * @param {Phaser.Game} game
 * @param {*} shared
 */
GameStates.makeFinal = function ( game, shared ) {
	/** @type {Typewriter} */
	let typewriter = null;

	let part = 0;
	let background = null;
	let temp = null;

	return {
		create: function () {
			background = game.add.sprite( 0, 0, "finalStage" );
			typewriter = new TypeWriter( game );
			typewriter.changeMessageList( finalStory1 );
			typewriter.startDisplay( true, new Phaser.Point( 200, 50 ), 25, "#ffff50" );
		},

		update: function () {
			typewriter.update();
			if ( typewriter.finish ) {
				switch ( part ) {
					case 0:
						shared.music.fadeOut();
						game.camera.shake( 0.1, 750, true );
						typewriter.changeMessageList( finalStory2 );
						typewriter.startDisplay( true, new Phaser.Point( 200, 50 ), 25, "#ffff50" );
						part += 1;
						break;
					case 1:
						if ( temp == null ) {
							shared.music = game.add.audio( "blood", 1.0, true );
							shared.music.play();

							game.camera.fade( 0x0, 500, true );
							background.destroy();
							background = game.add.sprite( 0, 0, "bf1" );
							game.world.sendToBack( background );
							background.alpha = 0;
							game.add.tween( background ).to( { alpha: 1 }, 250, "Linear", true );
							temp = game.time.now;
						} else if ( game.time.now + 500 > temp && game.time.now <= temp + 500 ) {
							game.camera.flash( 0x0, 500, true );
						} else if ( game.time.now + 1010 > temp ) {
							game.camera.fade( 0x0, 500, true );
							background.destroy();
							background = game.add.sprite( 0, 0, "bf2" );
							background.alpha = 0;
							game.world.sendToBack( background );
							game.add.tween( background ).to( { alpha: 1 }, 250, "Linear", true );
							game.camera.flash( 0x0, 500, true );

							typewriter.changeMessageList( finalStory3 );
							typewriter.startDisplay( true, new Phaser.Point( 200, 50 ), 25, "#ffff50" );
							game.camera.shake( 0.01, 750, true );
							part += 1;
							temp = null;
						}
						break;
					case 2:
						if ( temp == null ) {
							game.camera.fade( 0x0, 500, true );
							background.destroy();
							background = game.add.sprite( 0, 0, "bf3" );
							background.alpha = 0;
							game.world.sendToBack( background );
							game.add.tween( background ).to( { alpha: 1 }, 250, "Linear", true );
							temp = game.time.now;
						} else if ( game.time.now + 500 > temp && game.time.now <= temp + 500 ) {
							game.camera.flash( 0x0, 350, true );
						} else if ( game.time.now + 750 > temp ) {
							game.camera.fade( 0x0, 500, true );
							background.destroy();
							background = game.add.sprite( 0, 0, "bf4" );
							background.alpha = 0;
							game.world.sendToBack( background );
							game.add.tween( background ).to( { alpha: 1 }, 250, "Linear", true );
							game.camera.flash( 0x0, 500, true );

							typewriter.changeMessageList( finalStory4 );
							typewriter.startDisplay( true, new Phaser.Point( 200, 50 ), 25, "#ffff50" );
							part += 1;
							temp = null;
						}
						break;
					case 3:
						background.destroy();
						shared.backtrack = true;
						shared.gravity = 0.45;
						game.state.start( "Game" );
						break;
				}
			}
		}
	};
}
