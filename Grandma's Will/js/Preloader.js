"use strict";

/**
 *
 * @param {Phaser.Game} game
 */
GameStates.makePreloader = function ( game ) {

	let background = null;
	let preloadBar = null;
	let ready = false;

	return {
		preload: function () {
			game.load.script( 'webfont', "//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" )

			// Set the background and preload bar
			background = game.add.sprite( 0, 0, 'light_house' );
			preloadBar = game.add.sprite( 200, 500, 'loadbar' );

			// Auto crop the load bar
			game.load.setPreloadSprite( preloadBar );

			// Main Menu
			game.load.image( 'nolight_house', 'assets/nolight_house.jpg' );
			game.load.image( 'morg_house', 'assets/morg_house.jpg' );
			game.load.image( 'blood_house', 'assets/blood_house.jpg' );
			game.load.image( 'start_button', 'assets/start_button.png' );
			game.load.image( 'create_button', 'assets/create_button.png' );
			game.load.image( 'leaf1', 'assets/leaves/1.png' );
			game.load.image( 'leaf2', 'assets/leaves/2.png' );
			game.load.image( 'leaf3', 'assets/leaves/3.png' );
			// Game Play
			game.load.image( 'firstStage', 'assets/forest4.jpg' );
			game.load.image( 'secondStage', 'assets/forest1.jpg' ); // third stage too
			game.load.image( 'fourthStage', 'assets/forest2.jpg' );
			game.load.image( 'fifthStage', 'assets/forest3.jpg' ); // sixth stage too
			game.load.image( 'finalStage', 'assets/forest5.jpg' );
			game.load.image( 'firstStageB', 'assets/forest4B.jpg' );
			game.load.image( 'secondStageB', 'assets/forest1B.jpg' ); // third stage too
			game.load.image( 'fourthStageB', 'assets/forest2B.jpg' );
			game.load.image( 'fifthStageB', 'assets/forest3B.jpg' ); // sixth stage too
			// Finial Story
			game.load.image( "bf1", "assets/bloodForest.jpg" );
			game.load.image( "bf2", "assets/bloodForest2.jpg" );
			game.load.image( "bf3", "assets/bloodForest3.jpg" );
			game.load.image( "bf4", "assets/bloodForest4.jpg" );
			// Player
			game.load.image( 'player', 'assets/player.png' );
			game.load.image( 'ball', 'assets/ball.png' );
			game.load.image( 'mom', 'assets/mom.png' );
			game.load.image( 'tiles', 'assets/tiles.png' );

			// Music
			game.load.audio( "intro", "assets/intro.mp3" );
			game.load.audio( "end", "assets/end.mp3" );
			game.load.audio( "forest", "assets/forest.mp3" );
			game.load.audio( "blood", "assets/bForest.mp3" );
		},

		create: function () { },

		update: function () {
			if ( game.cache.isSoundDecoded( "intro" ) &&
				game.cache.isSoundDecoded( "end" ) &&
				game.cache.isSoundDecoded( "forest" ) &&
				game.cache.isSoundDecoded( "blood" ) &&
				!ready ) {
				game.state.start( 'MainMenu' )
				ready = true;
			}
		}
	};
};
