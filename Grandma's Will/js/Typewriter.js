/**
 * A text handler that display text on screen in a typewriter format
 */
class TypeWriter {
	/**
	 * Initialize an instance of Typewriter
	 * @param {Phaser.Game} inputControls 
	 * @param {Phaser.Text} text 
	 */
	constructor(game, text = null) {
		game.input.keyboard.addKeyCapture([Phaser.Keyboard.ENTER]);
		/** @type {Phaser.Text} */
		this.text = null;
		if (text == null) {
			this.text = game.add.text(300, 275, "", {
				font: "20px Gloria Hallelujah",
				fill: "#ff0044",
				align: "left"
			});
		} else this.text = text;
		/** @type {{keyEnter: InputControl, mouseLeft: InputControl}} */
		this.inputs = {
			keyEnter: new InputControl(game.input.keyboard.addKey(Phaser.Keyboard.ENTER)),
			mouseLeft: new InputControl(game.input.activePointer.leftButton)
		};
		/** @type{Phaser.Text} */
		text = null;
		/** @type {number} */
		this.listIndex = 0;
		/** @type {number} */
		this.index = 0;
		/** @type {string} */
		this.message = null
		this.list = null;
		this.complete = true;
		this.start = false;
		this.text.setText("");
	}

	/**
	 * Update the display typewriter's text.
	 * Will only run if start is set to true
	 */
	update() {
		for (let ele in this.inputs) {
			this.inputs[ele].update();
		}
		if (this.start) {
			if (this.message == null) {
				this.changeMessage(this.list[this.listIndex]);
			} else if (this.inputs.keyEnter.isDown || this.inputs.mouseLeft.isDown) {
				if (!this.done())
					this.finishMessage();
				else {
					this.listIndex += 1;
					if (this.listIndex < this.list.length)
						this.changeMessage(this.list[this.listIndex])
					else {
						this.reset();
						return;
					}
				}
			}
			if (!this.done()) {
				this.text.setText(this.text.text + this.message[this.index]);
				this.index += 1;
			}
		}
	}

	/**
	 * Change the list of messages typewriter will display on screen.
	 * Must call startDisplay afterward to start typewriter
	 * @param {[[string, string, boolean]]} list 
	 */
	changeMessageList(list) {
		this.list = list;
		this.text.setText("");
		this.complete = false;
		this.start = false;
	}

	/**
	 * Return whether typewriter has finish display all text it was given
	 */
	get finish() { return this.complete; }
	/**
	 * Return true if typewriter is current displaying text onscreen
	 */
	get startDisplay() { return this.start; }
	/**
	 * Set the start status of typewriter. Set to true to start displaying
	 * @param {boolean} start
	 * @param {Phaser.Point} point
	 * @param {number} size
	 * @param {string} stroke
	 */
	startDisplay(start, point = null, size = null, stroke = null) {
		this.start = start;
		if (point != null)
			this.text.position = point;
		if (size != null)
			this.text.fontSize = size;
		if (stroke != null) {
			this.text.stroke = stroke;
			this.text.strokeThickness = 3;
		}
	}

	reset() {
		this.text.setText("");
		this.listIndex = 0;
		this.index = 0;
		this.message = null
		this.list = null;
		this.complete = true;
		this.start = false;
	}

	changeMessage(data) {
		if (data.length != 3) {
			console.error("Invalid data passed to Typewriter!");
			throw new TypeError("Invalid data passed!");
		}

		this.message = data[0];
		this.text.addColor(data[1], 0);
		this.index = 0;
		if (data[2])
			this.text.setText("");

	}

	finishMessage() {
		this.text.setText(this.message);
		this.index = this.message.length;
	}

	done() { return this.index == this.message.length; }
}
