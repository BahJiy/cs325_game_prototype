"use strict";
/**
 *
 * @param {Phaser.Game} game
 * @param {*} shared
 */
GameStates.makeGame = function ( game, shared ) {

	/** @type {Phaser.Sprite} */
	let background = null;
	/** @type {Phaser.Tilemap} */
	let world = null;
	/** @type {Phaser.TilemapLayer}} */
	let stage = null;
	let tiles = null;
	/** @type {number} */
	let level = 0;

	/** @type {Phaser.Sprite} */
	let player = null;
	/** @type {function(Phaser.Game)} */
	let workFunc = null;

	/**
	 * Holds the recharge timer for click
	 * @type {number}
	*/
	let clickTime = null;
	/** @type {number} */
	let clickCount = 0;
	/** @type {Phaser.Text} */
	let clickText = null;;

	/**
	 * @param {Array<Array<Array<number>>>} tileInfo
	 * @param {{background: string, func: function(Phaser.Game), player: [number, number], camera: [number, number]}} stageInfo
	 */
	function setStage ( tileInfo, stageInfo, reverse = false ) {
		// Fade out to set up the stage
		game.camera.fade( 0x0, 1500, true );
		game.camera.scale.setTo( 1, 1 );

		// Finish up whatever
		if ( workFunc != null )
			workFunc( game, shared, true );

		let speed = 0;
		let angle = 0;
		if ( world != null ) {
			world.destroy();
			stage.destroy();
			background.destroy();

			speed = player.body.speed;
			angle = player.body.angle;
			player.destroy();
		}

		let playerPos = reverse ? stageInfo.reverse : stageInfo.player;

		if ( shared.backtrack )
			background = game.add.sprite( 0, 0, stageInfo.background2 );
		else
			background = game.add.sprite( 0, 0, stageInfo.background );

		player = game.add.sprite( playerPos[ 0 ], playerPos[ 1 ], "player" );
		game.world.sendToBack( background );

		world = game.add.tilemap();
		world.addTilesetImage( "tiles", "tiles", 32, 32 );
		stage = world.create( "stage", 32, 18, 32, 32 );

		let tileIndex = 0;
		tileInfo.forEach( index => {
			index.forEach( tile => {
				world.putTile( tileIndex, tile[ 0 ], tile[ 1 ], stage );
			} )
			tileIndex += 1;
		} )

		let tileShape = [ 1, 2, 3, 4, 5 ]
		tiles = game.physics.ninja.convertTilemap( world, stage, tileShape );
		game.physics.ninja.gravity = shared.gravity;

		game.physics.ninja.enableCircle( player, player.width / 2 );
		player.body.bounce = 0.45;
		player.body.friction = 0.01;
		player.body.collideWorldBounds = false;

		if ( ( shared.backtrack && level == stages.length - 1 ) || ( !shared.backtrack && level == 0 ) ) {
			player.body.moveRight( 0 )
			player.body.moveDown( 0 );
		} else {
			player.body.moveRight( speed * 25 * Math.cos( angle ) )
			player.body.moveDown( speed * 25 * Math.sin( angle ) );
		}

		game.world.bringToTop( player );

		workFunc = stageInfo.func;
		game.camera.flash( 0x0, 500, true );
	}

	return {
		create: function () {
			game.physics.startSystem( Phaser.Physics.NINJA );


			if ( shared.backtrack ) {
				level = stages.length - 1;
				setStage( stages[ level ][ 0 ], stages[ level ][ 1 ], true );
			} else
				setStage( stages[ level ][ 0 ], stages[ level ][ 1 ] );

			clickText = game.add.text( 75, 75, "Click Count: " + 0, {
				font: "36px Gloria Hallelujah",
				fill: "#ffffff",
				algin: "center"
			} )


			clickTime = 0;
			clickCount = shared.clickCount;

			if ( !shared.backtrack ) {
				shared.music.stop();
				shared.music = game.add.audio( "forest", 1.0, true );
				shared.music.play();
			}
		},

		render: function () {
			game.debug.text( "Mouse Pos: " + game.input.mousePointer.x + " | " + game.input.mousePointer.y, 0, 25 );
			game.debug.bodyInfo( player, 50, 50, 0xFFFF );
		},

		update: function () {
			if ( workFunc != null )
				workFunc( game, shared );
			for ( let i = 0; i < tiles.length; i++ ) {
				player.body.circle.collideCircleVsTile( tiles[ i ].tile );
			}

			if ( clickTime < game.time.now && game.input.activePointer.leftButton.isDown ) {
				let distance = game.input.activePointer.position.distance( player.position );
				let angle = game.input.activePointer.position.angle( player.position );
				if ( distance <= 120 ) {
					clickCount += 1;
					let power = 775 - Math.abs( distance );
					player.body.moveRight( power * Math.cos( angle ) )
					player.body.moveDown( power * Math.sin( angle ) );
				}
				clickTime = game.time.now + 1000;

			}

			if ( player.y > game.height ) {
				if ( shared.backtrack )
					setStage( stages[ level ][ 0 ], stages[ level ][ 1 ], true );
				else
					setStage( stages[ level ][ 0 ], stages[ level ][ 1 ] );
			} else if ( !shared.backtrack ) {
				if ( player.x >= game.width ) {
					if ( level < stages.length - 1 ) {
						level += 1;
						setStage( stages[ level ][ 0 ], stages[ level ][ 1 ] );
					} else {
						shared.clickCount = clickCount;
						world.destroy();
						world = null;
						game.camera.fade( 0x0, 1000, true );
						game.state.start( "FinalStory" );
					}
				} else if ( player.x <= 0 && level > 0 ) {
					level -= 1;
					setStage( stages[ level ][ 0 ], stages[ level ][ 1 ], true );
				}
			} else if ( shared.backtrack ) {
				if ( player.x <= 0 ) {
					if ( level > 0 ) {
						level -= 1;
						setStage( stages[ level ][ 0 ], stages[ level ][ 1 ], true );
					} else {
						shared.clickCount = clickCount;
						world.destroy();
						world = null;
						game.camera.fade( 0x0, 1000, true );
						game.state.start( "MainMenu" );
					}
				} else if ( player.x >= game.width && level < stages.length - 1 ) {
					level += 1;
					setStage( stages[ level ][ 0 ], stages[ level ][ 1 ] );
				}

			}
			clickText.setText( "Click Count: " + clickCount );
		}
	}
}
