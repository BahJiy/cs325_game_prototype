
"use strict";
/**
 * 
 * @param {Phaser.Game} game 
 * @param {*} shared 
 */
GameStates.makeStages = function (game, shared) {
	/** @type {Phaser.Tilemap} */
	let world = null;
	/**@type {Phaser.TilemapLayer} */
	let curLayer = null;
	/** @type {Phaser.Rectangle} */
	let tileSize = null;
	/** @type {Phaser.Sprite} */
	let ball = null;
	/** @type {number} */
	let stage = 0;
	/** @type {number} */
	let tileIndex = 0;

	/** @type {Phase.Sprite} */
	let background = null;
	/** @type {Phaser.Input} */
	let k1 = null;
	/** @type {Phaser.Input} */
	let k2 = null;
	/** @type {Phaser.Input} */
	let k3 = null;
	/** @type {Phaser.Input} */
	let k4 = null;
	/** @type {Phaser.Input} */
	let k5 = null;
	/** @type {Phaser.Input} */
	let k6 = null;
	/** @type {Phaser.Input} */
	let kENTER = null;
	/** @type {Phaser.Input} */
	let kESC = null;
	/** @type {Phaser.CursorKeys} */
	let cursor = null;

	/** @type {boolean} */
	let placePlayer = false;
	/** @type {InputControl} */
	let inputControl = null;

	function gotToMain() {
		world.destroy();
		world = null;
		game.state.start("MainMenu");
	}


	function updateTilemap() {
		maker.x = curLayer.getTileX(game.input.activePointer.worldX) * 32;
		maker.y = curLayer.getTileY(game.input.activePointer.worldY) * 32;

		if (!placePlayer) {
			if (game.input.activePointer.leftButton.isDown) {
				world.putTile(tileIndex, curLayer.getTileX(maker.x), curLayer.getTileY(maker.y), curLayer);
			} else if (game.input.activePointer.rightButton.isDown) {
				world.removeTile(curLayer.getTileX(maker.x), curLayer.getTileY(maker.y), curLayer);
			}
		}
	}

	/**
	 * @param {Array<Array<Array<number>>>} tileInfo
	 * @param {{background: string, func: function(Phaser.Game), player: [number, number], camera: [number, number]}} stageInfo
	 */
	function setStage(tileInfo, stageInfo) {
		// Fade out to set up the stage
		game.camera.fade(0x0, 1500, true);
		//		game.camera.scale.setTo(1.3, 1.3);
		//		game.camera.x = stageInfo.camera[0];
		//		game.camera.y = stageInfo.camera[1];

		background = game.add.sprite(0, 0, stageInfo.background);
		game.world.sendToBack(background);

		let tileIndex = 0;
		tileInfo.forEach(index => {
			index.forEach(tile => {
				world.putTile(tileIndex, tile[0], tile[1], stage);
			})
			tileIndex += 1;
		})

		ball.x = stageInfo.player[0];
		ball.y = stageInfo.player[1];
		game.world.bringToTop(ball);

		game.camera.flash(0x0, 500, true);
	}

	function getTileLocation() {
		let i = 0;
		/** @type {Phaser.Tile} */
		let t = null;
		let data = [[], [], [], [], []]
		for (let j = 0; j < 5; j++) {
			t = world.searchTileIndex(j, i);
			while (t != null) {
				i += 1;
				data[j].push([t.x, t.y]);
				t = world.searchTileIndex(j, i);
			}
			i = 0;
		}

		return parseData(data);
	}

	function parseData(data) {
		let str = "[";
		data.forEach(ele => {
			str += "[";
			ele.forEach(tile => {
				str += "[" + tile[0] + "," + tile[1] + "],";
			});
			str += "],";
		});
		str += "]";

		str;
		ball.x;
		ball.y;
		game.camera.x;
		game.camera.y;
	}

	/** @type {Phaser.Graphics} */
	let maker = null;
	return {
		create: function () {
			ball = game.add.sprite(0, 0, "ball");
			// Empty world
			world = game.add.tilemap();
			world.addTilesetImage("tiles", "tiles", 32, 32);
			curLayer = world.create("world", 32, 18, 32, 32);
			maker = game.add.graphics();
			maker.lineStyle(2, 0xFF50, 1);
			maker.drawRect(0, 0, 32, 32);

			// To create a world
			game.input.addMoveCallback(updateTilemap, this);

			game.input.mouse.capture = true;
			game.canvas.oncontextmenu = function (e) { e.preventDefault(); }

			k1 = game.input.keyboard.addKey(Phaser.Keyboard.A);
			k2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
			k3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
			k4 = game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
			k5 = game.input.keyboard.addKey(Phaser.Keyboard.FIVE);
			k6 = game.input.keyboard.addKey(Phaser.Keyboard.SIX);
			kENTER = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
			kESC = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
			game.input.keyboard.addKeyCapture([
				Phaser.Keyboard.ENTER,
				Phaser.Keyboard.ESC,
				Phaser.Keyboard.A,
				Phaser.Keyboard.TWO,
				Phaser.Keyboard.THREE,
				Phaser.Keyboard.FOUR,
				Phaser.Keyboard.FIVE,
				Phaser.Keyboard.SIX,
			]);

			cursor = game.input.keyboard.createCursorKeys();
			inputControl = new InputControl(kENTER);

			setStage(stage4Tile, stage4Info);
		},

		render: function () {
			game.debug.text("Mouse Pos: " + game.input.mousePointer.x + " | " + game.input.mousePointer.y, 0, 25);
		},

		update: function () {
			inputControl.update();
			if (k1.isDown) {
				placePlayer = false;
				tileIndex = 0;
			} else if (k2.isDown) {
				placePlayer = false;
				tileIndex = 1;
			} else if (k3.isDown) {
				placePlayer = false;
				tileIndex = 2;
			} else if (k4.isDown) {
				placePlayer = false;
				tileIndex = 3;
			} else if (k5.isDown) {
				placePlayer = false;
				tileIndex = 4;
			} else if (k6.isDown)
				placePlayer = true;
			else if (inputControl.isDown) {
				if (game.camera.scale.x != 1) {
					game.camera.reset();
					game.camera.scale.setTo(1, 1);
				} else {
					game.camera.x = 0;
					game.camera.y = 165;
					game.camera.scale.setTo(1.3, 1.3);
				}
			} else if (kESC.isDown)
				getTileLocation()


			if (cursor.left.isDown)
				game.camera.x -= 25;
			if (cursor.right.isDown)
				game.camera.x += 25;
			if (cursor.down.isDown)
				game.camera.y += 25;
			if (cursor.up.isDown)
				game.camera.y -= 25;

			if (placePlayer) {
				ball.x = game.input.activePointer.worldX;
				ball.y = game.input.activePointer.worldY;
				if (game.input.activePointer.leftButton.isDown)
					placePlayer = false;
			}
		}
	};
}
