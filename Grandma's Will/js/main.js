"use strict";
var game = new Phaser.Game( 1000, 563, Phaser.AUTO, 'game' );
window.onload = function () {

	//	Create your Phaser game and inject it into the 'game' div.
	//	We did it in a window.onload event, but you can do it anywhere (requireJS load, anonymous function, jQuery dom ready, - whatever floats your boat)
	/** @type {Phaser.Game} */

	let WebFontConfig = {
		active: function () { game.time.events.add( Phaser.Timer.SECOND, null, this ); },
		google: {
			families: [ 'Inconsolata', 'Gloria Hallelujah' ]
		}
	};

	//	Add the States your game has.
	//	You don't have to do this in the html, it could be done in your Boot state too, but for simplicity I'll keep it here.

	// An object for shared variables, so that them main menu can show
	// the high score if you want.
	var shared = {};

	shared.clickCount = 0;
	shared.backtrack = false;
	shared.gravity = 0.35
	/** @type {Phaser.Audio} */
	shared.music = null;

	game.state.add( 'Boot', GameStates.makeBoot( game ) );
	game.state.add( 'Preloader', GameStates.makePreloader( game ) );
	game.state.add( 'MainMenu', GameStates.makeMainMenu( game, shared ) );
	game.state.add( 'BeginStory', GameStates.makeBegin( game, shared ) );
	game.state.add( 'FinalStory', GameStates.makeFinal( game, shared ) );
	game.state.add( 'Creator', GameStates.makeStages( game ) );
	game.state.add( 'Game', GameStates.makeGame( game, shared ) );
	//	Now start the Boot state.
	game.state.start( 'Boot' );

};
window.onbeforeunload = function ( e ) {
	game.world.removeAll();
	game.destroy();
};
