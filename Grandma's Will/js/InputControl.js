/** Helps detect input press. Prevents holding/quick pressing */
class InputControl {
	/**
	 * Get the input to monitor
	 * @param {Phaser.DeviceButton|Phaser.Key} input 
	 */
	constructor(input) {
		this.input = input;
		this.down = false;
	}
	/**
	 * Return whether the input is just pressed and is not held
	 */
	get isDown() {
		if (this.input.isDown && !this.down) {
			this.down = true;
			return true;
		}
	}

	/**
	 * Must be called every cycle to refresh input's status
	 */
	update() {
		if (!this.input.isDown)
			this.down = false;
	}
}
