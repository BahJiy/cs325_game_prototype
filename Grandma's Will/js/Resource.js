/** @type{string} */
let child = "#1aa566";
let child2 = "#f44242";
/** @type{string} */
let mom = "#d31b77"

/** @type {[[string, string, boolean]]} story1TextA */
let story1TextA = [
	["Mom, where is grandma?", child, true],
	["Don't worry about it honey.\nYou just need to go to sleep.\n", mom, true],
	["We got a big day ahead of us tomorrow.", mom, false],
	["But grandma always kiss me goodnight!", child, true],
	["How about a kiss from me instead tonight?", mom, true],
	["Ok...", child, true]
]
/** @type {[[string, string, boolean]]} story1TextB */
let story1TextB = [
	["But I really want to see grandma...", child, true],
	["...\n", child, true],
	["......\n", child, false],
	[".........\n", child, false],
	["I wonder if mom is asleep yet?", child, true],
]

/** @type {[[string, string, boolean]]} story1TextB */
let story1TextC = [
	["Honey!\n", mom, true],
	["Are you ready? We have to go!", mom, false],
	["Coming!", child, true],
	["Are we getting Grandma?", child, true],
	["...\n", mom, true],
	["Kind of. We are going somewhere very\nspecial to Grandma", mom, false],
	["Where is that?", child, true],
	["You'll see.", mom, true],
	["Where's Grandma? Isn't she coming?", child, true],
	["She is...", mom, true],
	["Where is she then?", child, true],
	["I don't see her.", child, false],
	["...", mom, true],
	["......", mom, false],
	["We have to go else it will get dark.", mom, true],
	["Huh? Mom, wait! Where's Grandma?", child, true]
]

let stage1Story = [
	["Hurry! We don't have much time", mom, true],
	["I'm coming!", child, true],
	["I can move if I click near me\n", child, true],
	["However, I must wait a second after each click.\n", child, false],
	["Lets hurry!", child, false]
]

let finalStory1 = [
	["...", mom, true],
	["... And here we are...", mom, true],
	["What is that mom?", child, true],
	["...\n", mom, true],
	["......\n", mom, false],
	["Grandma's ashes...", mom, false],
	["Grandma's ashes... ", child, true],
	["What do you mean?!", child, false],
	["Grandma was just with me two days ago!", child, true],
	["Don't you remember, homey?", mom, true],
	["What do you mean? Mom, what happened?", child, true],
	["Maybe it is best that you don't remember...", mom, true]
];

let finalStory2 = [
	["Tell me mom!", child2, true],
	["Honey, calm down!", mom, true],
	["WHERE ", child2, true],
	["IS ", child2, false],
	["GRANDMA?", child2, false],
	["She is... ____", mom, true],
	["What? That can't be true", child2, true],
	["No... ", child2, true],
	["She's is alive...", child2, false],
	["There's...", child2, false]
];
let finalStory3 = [
	["Grandma?", child2, true],
	["Where are you?", child2, true],
]

let finalStory4 = [
	["Hah...", child2, true],
	["There's no way Grandma's dead.", child2, true],
	["Right mom?", child2, true],
	["...", child2, true],
	["Mom?\n", child2, true],
	["Hah... see, mom is trying to find Grandma right now...\n", child2, true],
	["But I know where Grandma is...\n", child2, true],
	["She is waiting for me at home...\n", child2, true],
]
/**
 * Write a quick story in stage 1
 * @param {Phaser.Game} game 
 */
function stage1Fuc(game, shared, finish = false) {
	if (!shared.backtrack && !this.finish) {
		if (finish) {
			this.finish = finish;
			this.typewriter.reset();
			this.typewriter = null;
			return true;
		}
		if (this.typewriter == null) {
			this.typewriter = new TypeWriter(game);
			this.typewriter.changeMessageList(stage1Story);
			this.typewriter.startDisplay(true, new Phaser.Point(160, 210), 30, "#ffff50");
		}
		this.typewriter.update();
	}
}

/** @type {Array<Array<Array<number>>>]} */
let stage1Tile = [[[0, 17], [1, 17], [2, 17], [3, 17], [4, 17], [5, 17], [6, 17], [7, 17], [8, 17], [9, 17], [10, 17], [11, 17], [12, 17], [13, 17], [14, 17], [15, 17], [16, 17], [17, 17], [18, 17], [19, 17], [20, 17], [21, 17], [22, 17], [23, 17], [24, 17], [25, 17], [25, 17], [26, 17], [26, 17], [27, 17], [28, 17], [29, 17], [30, 17], [31, 17]], [], [], [], [],];
/** @type {{background: string, func: function(Phaser.Game), player: [number, number]}, reverse: [number, number]]} */
let stage1Info = { background: "firstStage", background2: "firstStageB", func: stage1Fuc, player: [44, 421], reverse: [933, 510] };
/** @type {Array<Array<Array<number>>>]} */
let stage2Tile = [[[14, 3], [15, 3], [16, 3], [17, 3], [18, 3], [19, 3], [20, 3], [21, 3], [22, 3], [13, 4], [22, 4], [13, 5], [22, 5], [14, 6], [15, 6], [16, 6], [17, 6], [18, 6], [19, 6], [20, 6], [21, 6], [14, 9], [15, 9], [16, 9], [17, 9], [18, 9], [19, 9], [13, 10], [20, 10], [14, 11], [15, 11], [16, 11], [17, 11], [18, 11], [19, 11], [23, 12], [26, 12], [15, 14], [16, 14], [17, 14], [18, 14], [19, 14], [20, 14], [21, 14], [14, 15], [28, 15], [29, 15], [30, 15], [31, 15], [14, 16], [23, 16], [0, 17], [1, 17], [2, 17], [3, 17], [4, 17], [5, 17], [6, 17], [7, 17], [8, 17], [9, 17], [10, 17], [11, 17], [12, 17], [13, 17], [14, 17], [15, 17], [16, 17], [17, 17], [18, 17], [19, 17], [20, 17], [21, 17], [22, 17], [23, 17], [24, 17], [25, 17], [26, 17],], [[22, 7], [23, 8], [20, 9], [24, 9], [21, 10], [25, 10], [22, 11], [26, 11], [22, 14], [23, 15], [15, 16], [17, 16], [19, 16], [21, 16], [24, 16],], [[13, 3], [13, 9], [14, 14], [27, 15], [16, 16], [18, 16], [20, 16], [22, 16], [26, 16],], [[13, 6], [21, 7], [22, 8], [23, 9], [24, 10], [13, 11], [21, 11], [25, 11], [22, 12], [16, 15], [18, 15], [20, 15], [22, 15],], [[22, 6], [20, 11], [15, 15], [17, 15], [19, 15], [21, 15], [27, 16],],];
/** @type {{background: string, func: function(Phaser.Game), player: [number, number]}} */
let stage2Info = { background: "secondStage", background2: "secondStageB", func: null, player: [44, 510], reverse: [933, 400] };
/** @type {Array<Array<Array<number>>>]} */
let stage3Tile = [[[5, 12], [6, 12], [7, 12], [8, 12], [9, 12], [10, 12], [11, 12], [12, 12], [13, 12], [14, 12], [15, 12], [16, 12], [17, 12], [18, 12], [4, 13], [19, 13], [26, 13], [27, 13], [28, 13], [29, 13], [30, 13], [31, 13], [4, 14], [20, 14], [25, 14], [0, 15], [1, 15], [2, 15], [3, 15], [4, 15], [21, 15], [24, 15], [22, 16], [23, 16],], [[28, 0], [29, 1], [30, 2], [23, 4], [24, 5], [25, 6], [26, 7], [20, 8], [21, 9], [22, 10], [19, 12], [20, 13], [21, 14], [22, 15],], [[25, 0], [24, 1], [23, 2], [22, 4], [30, 4], [21, 5], [29, 5], [20, 6], [28, 6], [19, 7], [27, 7], [25, 8], [24, 9], [23, 10], [4, 12], [25, 13], [24, 14], [23, 15],], [[27, 0], [28, 1], [29, 2], [30, 3], [23, 5], [24, 6], [25, 7], [19, 8], [20, 9], [21, 10], [22, 11], [18, 13], [19, 14], [20, 15], [21, 16],], [[26, 0], [25, 1], [24, 2], [23, 3], [22, 5], [30, 5], [21, 6], [29, 6], [20, 7], [28, 7], [26, 8], [25, 9], [24, 10], [23, 11], [26, 14], [25, 15], [24, 16],],];
/** @type {{background: string, func: function(Phaser.Game), player: [number, number]}} */
let stage3Info = { background: "secondStage", background2: "secondStageB", func: null, player: [31, 443], reverse: [933, 325] };
/** @type {Array<Array<Array<number>>>]} */
let stage4Tile = [[[24, 0], [25, 1], [26, 2], [27, 3], [27, 4], [28, 4], [29, 4], [30, 4], [31, 4], [27, 8], [28, 8], [29, 8], [30, 8], [31, 8], [18, 10], [19, 10], [20, 10], [21, 10], [0, 13], [1, 13], [2, 13], [3, 13], [4, 13], [5, 13], [6, 13], [7, 13], [10, 16], [11, 16], [12, 16],], [[25, 0], [26, 1], [27, 2], [28, 3], [27, 9], [28, 10], [29, 11], [30, 12], [8, 13], [31, 13], [9, 14], [10, 15],], [[26, 4], [26, 8], [21, 9], [17, 10], [16, 11], [15, 12], [14, 13], [29, 13], [13, 14], [28, 14], [30, 14], [31, 14], [12, 15], [27, 15], [29, 15], [26, 16], [28, 16], [30, 16], [25, 17], [27, 17], [29, 17],], [[23, 0], [24, 1], [25, 2], [26, 3], [26, 9], [27, 10], [28, 11], [29, 12], [30, 13], [8, 14], [9, 15],], [[17, 11], [16, 12], [15, 13], [14, 14], [29, 14], [13, 15], [28, 15], [30, 15], [27, 16], [29, 16], [26, 17], [28, 17], [30, 17],],];
/** @type {{background: string, func: function(Phaser.Game), player: [number, number]}} */
let stage4Info = { background: "fourthStage", background2: "fourthStageB", func: null, player: [19, 365], reverse: [910, 202] };

let stageTest = [[[0, 13], [1, 13], [2, 13], [3, 13], [4, 13], [5, 13], [6, 13], [7, 13], [8, 13], [9, 13], [10, 13], [11, 13], [12, 13], [13, 13], [14, 13], [15, 13], [16, 13], [17, 13], [18, 13], [19, 13], [20, 13], [21, 13], [22, 13], [23, 13], [24, 13], [25, 13], [26, 13], [27, 13], [28, 13], [29, 13], [30, 13], [31, 13],], [[11, 8], [12, 9], [13, 10], [14, 11], [15, 12],], [[22, 6], [21, 7], [20, 8], [19, 9], [18, 10],], [[20, 2], [21, 3], [22, 4], [23, 5], [26, 7], [27, 8], [28, 9], [29, 10],], [[19, 2], [18, 3], [17, 4], [16, 5], [15, 6],],];
let stageTestInfo = { background: "secondStage", func: null, player: [50, 0] };

let stages = [[stage1Tile, stage1Info], [stage2Tile, stage2Info], [stage3Tile, stage3Info], [stage4Tile, stage4Info]]
