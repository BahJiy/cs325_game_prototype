"use strict";

/** @param {Phaser.Game} game */
GameStates.makeMainMenu = function ( game, shared ) {
    /**
     * Holds metadata for a leaf
     */
    class Leaf {
        /**
         * Initialize a leaf's metadata
         * @param {Phaser.Sprite} sprite
         */
        constructor ( sprite ) {
            this.sprite = sprite;
            this.xMove = 0.1 * Math.random() > 0.70 ? 1 : -1;
            this.xChange = 0.01;
        }

        /**
         * Must be called every cycle to update the leaf's metadata
         */
        update () {
            this.sprite.angle += 0.2;
            if ( this.xMove < -1 - Math.random() ) this.xChange = -0.01;
            else if ( this.xMove > 1 + Math.random() ) this.xChange = 0.01;
            this.xMove -= this.xChange;
            this.sprite.x += this.xMove;
            this.sprite.y += 0.5;
        }

        /**
         * Deletes and return true if the leaf should be remove
         */
        delete () {
            if ( this.sprite.y > game.height + 50 ) {
                this.sprite.destroy();
                return true;
            } else return false;

        }
    }

    /** @type {Phaser.Button} */
    let playButton = null;
    /** @type {Phaser.Button} */
    let createButton = null;
    /** @type {Array<Leaf>} */
    let leafL = null;

    function startGame ( pointer ) {
        game.state.start( 'BeginStory' );
    }
    function createGame ( pointer ) {
        game.state.start( "Creator" );
    }

    return {
        create: function () {
            game.camera.scale.setTo( 1, 1 );
            game.camera.reset();
            game.camera.flash( 0x0, 1500 );
            if ( shared.backtrack ) {
                game.add.sprite( 0, 0, 'blood_house' );
                let temp = game.add.text( 535, 270, "Grandma's Will", {
                    font: "45px Gloria Hallelujah",
                    fill: "#2b323a",
                    align: "left",
                } );
                temp.angle = -5;

            } else {
                game.add.sprite( 0, 0, 'light_house' );
            }
            playButton = game.add.button( 121, 400, 'start_button', startGame );
            createButton = game.add.button( 125, 460, 'create_button', createGame );

            game.add.text( 875, 540, "Grandma's Will v1.1", {
                font: "12px Inconsolata",
                fill: "#ffffff",
                align: "left"
            } );


            leafL = [];

            // Debugging
            game.input.mouse.capture = true;

            if ( shared.backtrack ) {
                shared.music.stop();
                shared.music = game.add.audio( "end", 1.0, true );
            } else shared.music = game.add.audio( "intro", 1.0, true );
            shared.music.play();
        },

        render: function () {
            game.debug.text( "Mouse Pos: " + game.input.mousePointer.x + " | " + game.input.mousePointer.y, 0, 25 );
        },

        update: function () {
            // Restrict to only 50 leaves at a time
            if ( leafL.length < 50 ) {
                // Randomly choose if a leaf should be created
                let sprite = null;
                switch ( Math.floor( Math.random() * 250 + 1 ) ) {
                    case 1: sprite = game.add.sprite( Math.random() * game.width, -( Math.random() * 50 ), 'leaf1' );
                        break;
                    case 2: sprite = game.add.sprite( Math.random() * game.width, -( Math.random() * 50 ), 'leaf2' );
                        break;
                    case 3: sprite = game.add.sprite( Math.random() * game.width, -( Math.random() * 50 ), 'leaf3' );
                        break;
                }
                // Just in case we actually did not made a leaf
                if ( sprite != null ) {
                    sprite.anchor.setTo( 0.5, 0.5 );
                    sprite.angle = Math.floor( Math.random() * 360 + 1 );
                    leafL.push( new Leaf( sprite ) );
                }
            }

            // Update and remove the leaf as needed
            let toRemove = []
            leafL.forEach( ele => {
                ele.update();

                if ( ele.delete() )
                    toRemove.push( leafL.indexOf( ele ) );
            } );
            toRemove.forEach( ele => {
                leafL.splice( ele, 1 );
            } );
        }
    };
};
