"use strict";

let GameStates = {};

/**
 * 
 * @param {Phaser.Game} game 
 */
GameStates.makeBoot = function (game) {
    return {
        init: function () {
            game.stage.disableVisibilityChange = true;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;


        },

        preload: function () {
            // Quickly load the loading screen and loading bar
            game.load.image('light_house', 'assets/light_house.jpg');
            game.load.image('loadbar', 'assets/loadbar.jpg');

        },

        create: function () {
            // Go to the actually loader to load the rest
            game.state.start('Preloader');
        }
    };
};
