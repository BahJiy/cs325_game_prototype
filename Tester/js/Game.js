"use strict";

window.onload = function () {
	/** @type {Phaser.Game} */
	let game = new Phaser.Game( 850, 800, Phaser.CANVAS, 'game', { preload: preload, create: create, update: update, render: render } );
	let testRect = null, group1;
	let testCircle = null, group2;
	let cursor = null;
	let esc = null;

	function preload () {
		game.scale.pageAlignHorizontally = true;
		game.scale.pageAlignVertically = true;
		game.stage.disableVisibilityChange = true;
		game.load.image( "left", "assets/left.png" );
		// Set the default anchor to be the center
		//PIXI.Sprite.defaultAnchor = { "x": 0.5, "y": 0.5 };
	}
	function create () {
		game.stage.backgroundColor = 0xF2B8DD;
		game.physics.startSystem( Phaser.Physics.P2JS );
		game.physics.p2.gravity.y = 0;
		group1 = game.physics.p2.createCollisionGroup();
		group2 = game.physics.p2.createCollisionGroup();

		testRect = game.add.sprite( 200, 200, "left" );
		testCircle = game.add.sprite( 0, 0 );
		game.physics.p2.enable( testCircle, true );
		game.physics.p2.enable( testRect, true );
		testRect.body.addPolygon( { optimalDecomp: false, skipSimpleCheck: true, removeCollinearPoints: false },
			[ 0,0, 0, 14, 14, 14 ] );

		//testRect.body.setCollisionGroup( group1 );
		testCircle.body.addCircle( 50 );
		testCircle.body.setCollisionGroup( group2 );
		testRect.body.setCollisionGroup( group1 );
		testRect.body.static = true;


		cursor = game.input.keyboard.createCursorKeys();
		esc = game.input.keyboard.addKey( Phaser.Keyboard.ESC );
	}

	function render () {
		game.debug.text( "Mouse X:" + game.input.activePointer.worldX + " Y:" + game.input.activePointer.worldY, 25, 25 );
		game.debug.text( "Contains: " + game.physics.p2.hitTest( game.input.activePointer.position ), 25, 50 );
	}

	function test ( b, b2 ) {
		console.log( "HI" );
	}
	function update () {
		testCircle.body.setZeroVelocity();
		if ( cursor.left.isDown ) {
			testCircle.body.moveLeft( 250 );
		}
		if ( cursor.right.isDown ) {
			testCircle.body.moveRight( 250 );
		}
		if ( cursor.up.isDown ) {
			testCircle.body.moveUp( 250 );
		}
		if ( cursor.down.isDown ) {
			testCircle.body.moveDown( 250 );
		}
		if ( esc.isDown ) {
			testRect.body.collides( group2 );
			testCircle.body.collides( group1 );
			testCircle.body.onBeginContact.add( test );
		}
	}
}
