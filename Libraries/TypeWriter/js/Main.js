﻿"use strict";

window.onload = function () {
	/**
	 * @type {Phaser.Game}
	 */
	let game = new Phaser.Game( 1000, 850, Phaser.AUTO, "game", { preload: preload, create: create, update: update } );
	/** @type {TypeWriter} */
	let test = null;


	// Some color to test with
	let color1 = "#99FF50";
	let color2 = "#50FFFF";
	let color3 = "#FF50FF";

	// Some commands to play with
	let commandList = [
		[ 1, "HELLO WORLD", color1, true ],
		[ 1, "I am alive!", color2, true ],
		[ 1, "What to Do?", color3, true ],
		[ 1, "Line 1 Test\n", color1, true ],
		[ 1, "Line 2 Test\n", color2, false ],
		[ 1, "Line 3 Test", color3, false ],
		[ 1, "Next commands shake\n", color3, true ],
		[ 3, 0.05, 1500, true ],
		[ 3, 0.15, 2500, true, 3000 ],
		[ 3, 0.01, 2500, false, 1500 ],
		[ 1, "How was the shaking?", color1, false ],
		[ 1, "Now we wait for 2 seconds\n", color2, true ],
		[ 2, 2000 ],
		[ 1, "DONE!!", color3, false ],
		[ 1, "END TESTING", color1, true ]
	]


	let WebFontConfig = {
		active: function () { game.time.events.add( 1000, null, this ); },
		google: {
			families: [ "Incosolata", "Gloria Hallelujah" ]
		}
	};

	function preload () {
		game.stage.disableVisibilityChange = true;
		game.scale.pageAlignHorizontally = true;
		game.scale.pageAlignVertically = true;
		game.load.script( 'webfont', "//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" )
	}

	function create () {
		game.add.text( 0, 0, "HIHIH", {
			font: "30px Incosolata",
			fill: "#FFFFFF"
		} );
		test = new TypeWriter( game );
		test.startDisplay( commandList );
	}

	function update () {
		test.update( game );
		if ( test.finished() ) {

			test.startDisplay( commandList, 500 );
		}

	}
};
