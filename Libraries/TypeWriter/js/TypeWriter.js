
"use strict"

/**
 * Basic typewriter functionality for Phaser engine.
 *
 * @author Huu Vu
 * @version 2.5
 * @requires Phaser
 * @requires InputControl
 */
class TypeWriter {
	/**
	 * Initialize TypeWriter. Typewriter takes an array of array with the inner array
	 * containing a TYPE and the required data. i.e. [[TYPE, data1, data2, ...], ...].
	 * Currently, the allowed TYPES are:
	 *
	 * MESSAGE, WAIT, SHAKE
	 * @param {Phaser.Game} game - The current Phaser's game
	 * @param {string} font - The font to use. pText will override this
	 * @param {Phaser.Text} pText - The Phaser.Text to use. The defaults are 12px
	 * Arial, #FFFFFF
	 * @param {InputControl} inControl - InputControl to use. The default are Enter and
	 * Mouse Click.
	 */
	constructor ( game, font = null, pText = null, inControl = null ) {
		// Private Variables
		/** @type {Phaser.Text} */
		let text = pText;
		if ( text == null ) {
			text = game.add.text( game.world.centerX, game.world.centerY, "", {
				font: "15px " + font,
				fill: "#FFFFFF"
			} )
		}
		/** @type {InputControl} */
		let inputs = inControl;
		// If we didn't get an InputControl, just create our own for
		// Enter and Mouse Click
		if ( inputs == null ) {
			inputs = new InputControl( game, Phaser.Keyboard.ENTER, true );
		}

		/** @type {number} */
		let waitTime = 0;
		/** @type {number} */
		let gameWait = 0;
		/** @type {number} */
		let textWait = 0;
		/** @type {string} */
		let message = "";
		/** @type {number} */
		let messageIndex = 73;
		/** @type {number} */
		let commandIndex = 0;
		/** @type {Array<Array<any>>} */
		let commandList = null;
		/** @type {boolean} */
		let finish = true;
		/** @type {boolean} */
		let start = false;
		/** @type {boolean} */
		let next = false;

		/** Are we done with this message */
		let done = function () {
			return messageIndex >= message.length;
		}

		/** Return true if all commands had been processed */
		this.finished = function () {
			return finish;
		}
		/**
		 * Must always be call to update TypeWriter
		 */
		this.update = function () {
			inputs.update( game );
			if ( start && ( game.time.now > gameWait && ( next ||
				inputs.isAnyDown( [ Phaser.Keyboard.ENTER, Phaser.Pointer.LEFT_BUTTON ] ) ) ) ) {
				// If we are not done with the message, but the user wants to go
				// to the next command
				if ( !done() ) {
					text.setText( text.text + message.slice( messageIndex ) );
					messageIndex = message.length;
				} else if ( commandIndex == commandList.length ) {
					// If we finish all commands, reset the TypeWriter
					this.stop();
				} else {
					evaluate();
					commandIndex += 1;
				}
			}
			if ( !done() && game.time.now > textWait ) {
				text.setText( text.text + message[ messageIndex ] );
				messageIndex += 1;
				textWait = game.time.now + waitTime;
			}
		}

		this.stop = function () {
			finish = true;
			gameWait = 0;
			commandIndex = 0;
			message = ""
			messageIndex = 43;
			commandList = [];
			start = false;
			text.setText( "" );
		}

		let evaluate = function () {
			let command = commandList[ commandIndex ];
			let shift = 0;
			switch ( command[ 0 ] ) {
				case TypeWriter.TYPE.WAIT:
					gameWait = game.time.now + command[ 1 ];
					next = true;
					break;
				case TypeWriter.TYPE.SHAKE:
					game.camera.shake( command[ 1 ], command[ 2 ], command[ 3 ] );
					gameWait = game.time.now + ( command[ 4 ] != undefined ? command[ 4 ] : 0 );
					next = true;
					break;
				case TypeWriter.TYPE.MESSAGE:
					// Fallthrough to default
					shift = 1;
				default:
					if ( command.length < 3 ) {
						throw new TypeError( "Invalid command passed" );
					}
					if ( command[ 2 + shift ] ) {
						text.setText( "" );
						text.colors = [];
					}
					message = command[ 0 + shift ];
					// Experimental!!! Allows multi-colored lines. No work
					// text.addColor( command[ 2 ], ( text.text.length == 0 ? 0: text.text.length - 1) );
					// basic single color only
					text.addColor( command[ 1 + shift ], 0 );
					messageIndex = 0;
					next = false;
					break;
			}
		}

		/**
		 * Start the display with the message list. All parameter except message are optional;
		 * if no argument are passed into them, they will use the previous settings
		 * @param {Array<Array<any>>} commands - Text to display on screen. Follow pattern guide
		 * @param {number} wait - Change how long before next character displays
		 * @param {Phaser.Point} position - Change the position for this display
		 * @param {number} size - Change the size for this display
		 * @param {string} stroke - Change the stroke for this display
		 * @param {number} strokeThick - Change the stroke thickness
		 */
		this.startDisplay = function ( commands, wait = 0, position = null, size = null, stroke = null, strokeThick = null ) {
			text.position = position != null ? position : text.position;
			text.fontSize = size != null ? size : text.fontSize;
			text.stroke = stroke != null ? stroke : text.stroke;
			text.strokeThickness = strokeThick != null ? strokeThick : text.strokeThickness;

			finish = false;
			waitTime = wait;
			commandList = commands;
			gameWait = 0;
			commandIndex = 0;
			next = true;
			start = true;
		}
	}

	static get TYPE () {
		return Object.freeze( {
			/**
			 * The message to display onscreen. Required data are the message (string),
			 * color of message in hex (string), whether to clear previous message (boolean)
			 *
			 * [MESSAGE, string, color, boolean],
			 * @example [TypeWriter.TYPE.MESSAGE, "Hello World", "#FF50FF", true]
			 */
			MESSAGE: 1,
			/**
			 * Wait x seconds before continuing to the next command. Required data is
			 * time (number)
			 *
			 * [WAIT, number]
			 * @example [TypeWriter.TYPE.WAIT, 2500]
			*/
			WAIT: 2,
			/**
			 * Shakes the camera. Required data are the intensity (number), duration (number)
			 * force (boolean), optional wait time (number)
			 *
			 * [SHAKE, number, number, boolean, number (optional)]
			 * @example [TypeWriter.TYPE.SHAKE, 0.01, 1000, false, 1000]
			*/
			SHAKE: 3
		} );
	};
}
