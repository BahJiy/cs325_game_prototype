"use strict";

let GameStates = {};
/**
 *
 * @param {Phaser.Game} game
 */
GameStates.makeBoot = function ( game ) {
    return {
        init: function () {
            game.stage.disableVisibilityChange = true;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            PIXI.Sprite.defaultAnchor = { x: 0.5, y: 0.5 };
        },

        preload: function () {
            // Table
            game.load.image( "table", "assets/table.png" );
            game.load.image( "shuffle", "assets/shuffle.png" );
            game.load.image( "swap", "assets/swap.png" );
            game.load.image( "stand", "assets/stand.png" );
            game.load.image( "hit", "assets/hit.png" );
            game.load.image( "up", "assets/up.png" );
            game.load.image( "down", "assets/down.png" );
            game.load.image( "next", "assets/next.png" );
            game.load.image( "instructionText", "assets/instructText.png" );

            // Cards
            game.load.image( "spades", "assets/spade.png" );
            game.load.image( "hearts", "assets/heart.png" );
            game.load.image( "clubs", "assets/club.png" );
            game.load.image( "diamonds", "assets/diamond.png" );
            game.load.image( "cardDown", "assets/cardBack.png" );
        },

        create: function () {
            // Go to the actually loader to load the rest
            game.state.start( 'MainMenu' );
        }
    };
};
