"use strict";

/** @param {Phaser.Game} game */
GameStates.makeMainMenu = function ( game, shared ) {

    let title = null;
    let start = null;
    let instruction = null;
    let back = null;
    let instructionText = null;
    let playerNum = 3;
    let infoText = null;

    let state = 0;

    function askPlayer () {
        state = 1;
        start.visible = false;
        instruction.visible = false
        start.inputEnabled = false;
        instruction.inputEnabled = false;
        title.visible = false;

        infoText.visible = true;
        let up = game.add.button( game.world.centerX + 100, game.world.centerY - 25, "up", () => {
            if ( playerNum + 1 <= 6 ) {
                playerNum++;
            } else playerNum = 6;
        } );
        let down = game.add.button( game.world.centerX + 100, game.world.centerY + 25, "down", () => {
            if ( playerNum > 2 ) {
                playerNum--;
            } else playerNum = 2;
        } );
        let next = game.add.button( game.world.centerX + 190, game.world.centerY, "next", () => {
            shared.playerNum = playerNum;
            up.destroy();
            down.destroy();
            next.destroy();

            askAI();
        } );
    }
    function askAI () {
        state = 2;
        playerNum = 0;

        let up = game.add.button( game.world.centerX + 100, game.world.centerY - 25, "up", () => {
            if ( playerNum + 1 <= shared.playerNum ) {
                playerNum++;
            } else playerNum = shared.playerNum;
        } );
        let down = game.add.button( game.world.centerX + 100, game.world.centerY + 25, "down", () => {
            if ( playerNum > 0 ) {
                playerNum--;
            } else playerNum = 0;
        } );
        let next = game.add.button( game.world.centerX + 190, game.world.centerY, "next", () => {
            shared.aiNum = playerNum;
            up.destroy();
            down.destroy();
            next.destroy();

            game.state.start( "Game" );
        } );
    }

    return {
        create: function () {
            game.add.image( game.world.centerX, game.world.centerY, "table" );

            title = game.add.text( game.world.centerX, game.world.centerY - 100, "ShuffleJack", {
                font: "75px Arial",
                stroke: "#ffffff",
                strokeThickness: 3
            } );
            start = game.add.text( game.world.centerX, game.world.centerY, "Start", {
                font: "32px Arial",
                stroke: "#ffffff",
                strokeThickness: 3
            } );
            start.inputEnabled = true;
            start.events.onInputUp.add( () => {
                askPlayer();
            } );

            instruction = game.add.text( game.world.centerX, game.world.centerY + 50, "Instructions", {
                font: "32px Arial",
                stroke: "#ffffff",
                strokeThickness: 3
            } );
            instruction.inputEnabled = true;
            instruction.events.onInputUp.add( () => {
                start.visible = false;
                instruction.visible = false
                start.inputEnabled = false;
                instruction.inputEnabled = false;
                title.visible = false;

                instructionText.visible = true;
                instructionText.inputEnabled = true;
            } );

            instructionText = game.add.image( game.world.centerX, game.world.centerY, "instructionText" );
            instructionText.visible = false;
            instructionText.events.onInputUp.add( () => {
                title.visible = true;
                start.inputEnabled = true;
                start.visible = true;
                instruction.inputEnabled = true;
                instruction.visible = true


                instructionText.visible = false;
                instructionText.inputEnabled = false;
            } )

            infoText = game.add.text( game.world.centerX - 150, game.world.centerY, "", {
                fill: "#FFFFFF",
                stroke: "#000000",
                strokeThickness: 3,
                font: "45px Arial"
            } );
            infoText.visible = false;
        },

        render: function () {
            //game.debug.text( "Pointer x: " + game.input.position.x + " y: " + game.input.position.y, 25, 25 );
        },

        update: function () {
            if ( state == 1 )
                infoText.setText( "Number of Players: " + playerNum );
            if ( state == 2 )
                infoText.setText( "Number of AI: " + playerNum );
        }
    };
};
