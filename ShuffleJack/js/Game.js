"use strict"

/** @param {Phaser.Game} game */
GameStates.makeGame = function ( game, shared ) {
	/** @type {Array<Cards>} */
	let usedCards = [];
	/** @type {Array<Cards>} */
	let cards = [];
	/** @type {Array<Player>} */
	let players = [];

	// Button
	/** @type {Phaser.Button} */
	let bShuffle;
	/** @type {Phaser.Button} */
	let bHit;
	/** @type {Phaser.Button} */
	let bStand;
	/** @type {Phaser.Button} */
	let bSwap;
	/** @type {Phaser.Button} */
	let up;
	/** @type {Phaser.Button} */
	let down;
	/** @type {Phaser.Button} */
	let next;

	/** @type {Player} */
	let curPlayer = null;
	/** @type {Array<Phaser.Point>} */
	let playerPos = [
		new Phaser.Point( 20, 100 ), new Phaser.Point( 355, 100 ), new Phaser.Point( 695, 100 ),
		new Phaser.Point( 20, 520 ), new Phaser.Point( 355, 520 ), new Phaser.Point( 695, 520 ),
	];
	/** @type {Phaser.Text} */
	let playerTurn = null;

	/** @type {number} */
	let stage = 0;
	/** @type {nubmer} */
	let betAmount = 250;
	/** @type {nubmer} */
	let betTotal = 0;

	/**
	 * The play function for the player
	 * @param {Array<Cards>} cards
	 * @param {Array<number>} values
	 * @param {Status} status
	 * @param {boolean} swap
	 */
	function playerPlay ( player, cards, values, status, swap, chip ) { }

	/**
	 * The play function for the AI
	 * @param {Array<Cards>} pcards
	 * @param {Array<number>} values
	 * @param {Status} status
	 * @param {boolean} swap
	 */
	function AIPlay ( player, pcards, values, status, swap, chip ) {
		if ( status == Status.SAFE ) {
			// Wait until we can act.
			if ( values[ 1 ] >= 17 ) {
				AISwapStand( player );
			} else if ( pcards.length < 3 ) {
				hit();
			} else if ( values[ 0 ] < 17 ) {
				if ( values[ 0 ] <= 11 ) {
					hit();
				} else if ( chip < 5000 ) {
					let offset = 0;
					if ( values[ 0 ] <= 10 ) {
						offset = values[ 0 ] / 15;
					} else if ( values[ 0 ] <= 15 ) {
						offset = values[ 0 ] / 100;
					}
					Math.random() < 0.5 + offset ? hit() : stand();
				} else {
					let offset = 0;
					if ( values[ 0 ] <= 10 ) {
						offset = values[ 0 ] / 15;
					} else {
						offset = ( Math.random() * values[ 0 ] ) / 100;
					}
					Math.random() < 0.75 ? hit() : stand();
				}
			} else {
				// else we just stand
				stand();
			}
		}
	}

	/**
	 * Read the comments
	 * @param {Player} player
	 */
	function AISwapStand ( player ) {
		let curTop = player.getFaceUpTop();
		// Get all the victim
		let exceptPlayer = players.filter( ( ele ) => { return ele != player && ele.getStatus() != Status.KEEP; } );
		// If the player top faceup card is a high value, we have a good chance at swapping without busting
		if ( curTop.getValue() > game.rnd.integerInRange( 7, 10 ) ) {
			// Initially we have a 50/50 to not busting (assume)
			let chance = exceptPlayer.length / 2;
			exceptPlayer.forEach( ele => {
				let value = ele.getFaceUpTop().getValue();
				if ( value + player.getValue() - curTop.getValue() < 21 ) {
					chance++; // If lots have cards that won't bust us, we have a high chance
					if ( value + player.getValue() - curTop.getValue() > player.getValue() ) {
						chance += 0.75; // A little extra if it cause us to be higher than previous values
					}
				} else if ( value + player.getValue() == 21 ) {
					chance += 3; // If there is a card that can get us to 21, we really want to risk it
				} else { // If Everyone has a card that will bust us, we will have negative chance
					chance--;
				}
			} );
			// Calculate the percentage value of all the player to how many won't bust us
			let percent = chance / exceptPlayer.length;
			// The randomizer
			Math.random() < percent ? swap() : stand();
		} else {
			let forceHit = false;
			// If there are players with higher values than us, we should really think about hitting and risking it all
			players.forEach( ele => {
				if ( ele != player && ele.getStatus != Status.BUST && ele.getValue() >= player.getValue() ) {
					forceHit = true;
				}
			} )
			// Should we just risk a hit?
			Math.random() < 0.07 + ( forceHit ? 0.23 : 0 ) ? hit() : stand();
		}
	}

	function betPhase () {
		if ( curPlayer != null ) {
			curPlayer.setBet( betAmount );
		}
		flipBet( true );
		stage = 1;
		betTotal += betAmount;
		betAmount = 250;
		playerTurn.position.setTo( game.world.centerX - 150, game.world.centerY );

		// If we reached the end, just start the game
		if ( players.indexOf( curPlayer ) == players.length - 1 ) {
			return startBJ();
		}
		// Else we get the next player
		curPlayer = players[
			players.indexOf( curPlayer ) == players.length - 1 ? 0 : players.indexOf( curPlayer ) + 1
		];
		// If the player has nothing don't ask.
		if ( curPlayer.getChip() <= 0 ) {
			betPhase();
		} else if ( curPlayer.getPlay() == AIPlay ) { // This is an AI
			let initialBet = 1000;
			if ( curPlayer.getChip() <= initialBet ) {
				initialBet = 250;
			} else if ( curPlayer.getChip() > 8500 ) {
				initialBet = initialBet = 1500;
			}
			// Finish the bet
			betAmount = initialBet;
			// Go to the next player
			betPhase();
		}
	}

	function startBJ () {
		flipBet( false );
		for ( let i = 0; i < players.length; i++ ) {
			usedCards = usedCards.concat( players[ i ].clearHand() );
			// Remove any player that has no more chips
			if ( players[ i ].getChip() <= 0 ) {
				players.splice( i, 1 );
				i--;
			}
		}
		cards = shuffleList( cards, 4 );

		if ( players.length == 1 ) {
			return endgame();
		}

		stage = 2;
		curPlayer = players[ 0 ];
		playerTurn.position.setTo( game.world.centerX, game.world.centerY - 100 );

		if ( cards.length < players.length ) {
			// If we don't have enough cards, shuffle all the cards be in
			cards = shuffleList( cards.concat( usedCards ), 4 );
			usedCards = [];
		}
		flipButton( true );
		showCards( cards.splice( 0, players.length ) );
		let timer = game.time.create( true );
		timer.add( 1250, () => {
			curPlayer.play();
		} );
		timer.start();
		return true;
	}

	/**
	 * End the game
	*/
	function endgame () {

	}

	/**
	 * Turn on/off the buttons
	 * @param {boolean} state
	 */
	function flipButton ( state ) {
		bShuffle.inputEnabled = state;
		bSwap.inputEnabled = state;
		bStand.inputEnabled = state;
		bHit.inputEnabled = state;
		bShuffle.visible = state;
		bSwap.visible = state;
		bHit.visible = state;
		bStand.visible = state;
	}

	function flipBet ( state ) {
		next.position.setTo( game.world.centerX + 190, game.world.centerY );
		up.visible = state;
		down.visible = state;
		next.visible = state;
		up.inputEnabled = state;
		down.inputEnabled = state;
		next.inputEnabled = state;
	}

	/**
	 * Evaluate who is the highest point winner
	*/
	function evaluateWinner () {
		/** @type {Player} */
		let highestPlayer = null;
		let tie = [];
		players.forEach( play => {
			if ( play.getStatus() != Status.BUST ) {
				if ( highestPlayer == null ) {
					highestPlayer = play;
				} else if ( highestPlayer.getValue() < play.getValue() ) {
					tie = [];
					highestPlayer.endgame( false );
					highestPlayer = play;
				} else if ( highestPlayer.getValue() == play.getValue() ) {
					tie.push( play );
				} else {
					play.endgame( false );
				}
			} else play.endgame( false )
		} );

		if ( highestPlayer != null ) {
			tie.push( highestPlayer );
		}
		// Give the winner chips
		tie.forEach( ele => {
			ele.endgame( true, Math.ceil( betTotal / tie.length ) );
		} );

		playerTurn.position.setTo( game.world.centerX, game.world.centerY );
		if ( tie.length == 1 ) {
			playerTurn.setText( "Player " + highestPlayer.getPlayerNumber() + " Won with " + highestPlayer.getValue() + " Points!\n\
			Earned: "+ betTotal + " chips!" );
			betTotal = 0;
		} else if ( tie.length > 1 ) {
			let str = "";
			for ( let i = 0; i < tie.length - 1; i++ ) {
				str += tie[ i ].getPlayerNumber() + ", ";
			}
			str += tie[ tie.length - 1 ].getPlayerNumber();
			playerTurn.setText( "Tie with " + highestPlayer.getValue() + " points are Players " + str + ".\n\
			Each player earns: " + ( Math.ceil( betTotal / tie.length ) ) );
			betTotal = 0;
		} else {
			playerTurn.setText( "Everyone busted...\nfunny how greedy people are." );
			// betotal carries
		}

		next.visible = true;
		next.inputEnabled = true;
		next.position.setTo( game.world.centerX, game.world.centerY + 100 )
		curPlayer = null;
		stage = 3;
	}

	/**
	 * Go to the next possible player. If none, go to evaluateWinner
	 * @param {number} times
	 */
	function nextPlayer ( times = 0 ) {
		// If all players are done, finish the game
		if ( times == 6 ) {
			flipButton( false );
			evaluateWinner();
			return;
		}
		curPlayer = players[
			players.indexOf( curPlayer ) == players.length - 1 ? 0 : players.indexOf( curPlayer ) + 1
		];

		if ( curPlayer.getStatus() == Status.BUST || curPlayer.getStatus() == Status.KEEP ) {
			nextPlayer( times + 1 );
		} else curPlayer.play();
	}

	/**
	 * Give the current player a card. Restock the card pile if needed
	*/
	function hit () {
		if ( cards.length == 0 ) {
			cards = shuffleList( usedCards, 4 );
			usedCards = [];
		}
		curPlayer.takeCard( cards.splice( 0, 1 )[ 0 ] );
		if ( curPlayer.getStatus() == Status.BJ ) {
			flipButton( false );
			return evaluateWinner();
		}
		nextPlayer();
	}

	/**
	 * Shuffle all facedown cards and redistribute them
	*/
	function shuffle () {
		let playerCards = [];
		players.forEach( play => {
			if ( play.getStatus() == Status.KEEP || play.getStatus() == Status.BUST ) {
				return;
			} playerCards.push( play.getFaceDownCard() );
		} )
		showCards( playerCards );

		bShuffle.inputEnabled = false;
		bShuffle.input.enabled = false;
		//showCards( cards.splice( 0, players.length ) );
		let timer = game.time.create( true );
		timer.add( 1250, () => {
			nextPlayer();
		} );
		timer.start();
	}

	/**
	 * Set the current player to keep
	*/
	function stand () {
		let temp = curPlayer.setKeep();
		betTotal -= temp[ 1 ];
		cards.push( temp[ 0 ] );
		nextPlayer();
	}

	function swap () {
		// If current player has a faceup card
		let validPlayers = players.filter( ( player ) => {
			if ( player != curPlayer && player.getStatus() != Status.KEEP ) {
				return true;
			} else {
				return false;
			}
		}, this );
		if ( curPlayer.getValue() > 0 && validPlayers.length != 0 ) {
			let curTop = curPlayer.getFaceUpTop();
			let index = game.rnd.integerInRange( 0, validPlayers.length - 1 );
			// Tell the victim to swap with the current player's top card and give back it's top face card
			let returnCard = validPlayers[ index ].swap( curTop );
			curPlayer.swap( returnCard ); // Current player needs to swap itself
			// If we swap, then go to next player
			nextPlayer();
		}
	}

	/**
	 * Shows the next cards a list of cards
	 * @param {Array<Cards>} cardList
	 */
	function showCards ( cardList ) {
		flipButton( false );
		for ( let i = 0; i != 6 && i < cardList.length; i++ ) {
			cardList[ i ].placeCard( 125 + i * 150, game.world.centerY, true );
		}
		cardList = shuffleList( cardList );
		let timer = game.time.create( true );
		timer.add( 1000, () => { dealCards( cardList ); } );
		timer.start();
	}

	/**
	 * Deal the cards a list of cards
	 */
	function dealCards ( cardList ) {
		flipButton( true );
		players.forEach( play => {
			// If a player kept their card, don't do anything
			if ( play.getStatus() == Status.KEEP || play.getStatus() == Status.BUST ) {
				return;
			}
			play.takeCard( cardList.splice( 0, 1 )[ 0 ] );
		} )
	}

	return {
		create: () => {
			game.add.image( game.world.centerX, game.world.centerY, "table" );
			// x + 250 || y + 100
			bShuffle = game.add.button( 350, 270, "shuffle", shuffle );
			bSwap = game.add.button( 350, 370, "swap", swap );
			bHit = game.add.button( 600, 270, "hit", hit );
			bStand = game.add.button( 600, 370, "stand", stand );
			flipButton( false );

			up = game.add.button( game.world.centerX + 100, game.world.centerY - 25, "up", () => {
				if ( betAmount + 250 < curPlayer.getChip() ) {
					betAmount += 250
				} else betAmount = curPlayer.getChip();
			} );
			down = game.add.button( game.world.centerX + 100, game.world.centerY + 25, "down", () => {
				if ( betAmount > 250 ) {
					betAmount -= 250
				} else betAmount = curPlayer.getChip() > 250 ? 250 : curPlayer.getChip();
			} );
			next = game.add.button( game.world.centerX + 190, game.world.centerY, "next", betPhase );

			[ "clubs", "diamonds", "hearts", "spades" ].forEach( str => {
				for ( let i = 1; i <= 13; i++ ) {
					cards.push( new Cards( game, i, str ) );
				}
			} );
			cards = shuffleList( cards, 4 ); // Shuffle the cards

			for ( let i = 1; i <= shared.playerNum; i++ ) {
				let playFunc = playerPlay;
				if ( shared.aiNum != 0 ) {
					// Allow computer to play also!
					playFunc = AIPlay;
					shared.aiNum--;
				}
				players.push( new Player( game, playFunc, i, playerPos[ i - 1 ] ) );
			}

			playerTurn = game.add.text( game.world.centerX, game.world.centerY - 100, "", {
				fill: "#FFFFFF",
				stroke: "#000000",
				strokeThickness: 3,
				font: "45px Arial"
			} )

			betPhase();
		},

		render: () => {
			//game.debug.text( "Cards Left: " + cards.length, 25, 25 );
			//game.debug.text( "Used Cards: " + usedCards.length, 25, 50 );
			//			game.debug.text( "Mouse X:" + game.input.activePointer.worldX + " Y:" + game.input.activePointer.worldY, 25, 25 );
		},

		update: () => {
			if ( stage == 2 ) {
				playerTurn.setText( "Player " + curPlayer.getPlayerNumber() + "'s turn" );
			} else if ( stage == 1 ) {
				playerTurn.setText( "Player " + curPlayer.getPlayerNumber() + "'s bet: " + betAmount );
			}
		}
	};
}
