let Status = Object.freeze( {
	"SAFE": 1,
	"KEEP": 2,
	"BUST": 3,
	"BJ": 4
} );

/**
 * An implementation of Fisher-Yates shuffling algorithm
 * @param {Array<any>} list
 * @param {number} times
 * @returns Array<any>
 */
function shuffleList ( list, times = 0 ) {
	let shuffledList = [];
	let copyList = list.slice();
	while ( copyList.length != 0 ) {
		shuffledList.push(
			copyList.splice(
				Math.floor(
					Math.random() * copyList.length ),
				1
			)[ 0 ]
		);
	}
	return times <= 0 ? shuffledList : shuffleList( shuffledList, times - 1 );
}

/**
 * A class to hold a card instance
 * @param {Phaser.Game} game
 * @param {number} value
 * @param {string} type
 */
class Cards {
	constructor ( game, value, type ) {
		/** @type {Phaser.Image} */
		let faceImage = null;
		/** @type {Phaser.Image} */
		let downImage = null;
		/** @type {Phaser.Text} */
		let number = null;
		/** @type {Phaser.Point} */
		let position = null;
		/** @type {string} */
		let stringValue = value;
		// For letter cards
		switch ( value ) {
			case 1: stringValue = "A"; break;
			case 11: stringValue = "J"; break;
			case 12: stringValue = "Q"; break;
			case 13: stringValue = "K"; break;
		}

		/**
		 * Place a card at position given, either face up or face down
		 * @param {number} x
		 * @param {number} y
		 * @param {boolean} faceUp
		 */
		this.placeCard = ( x, y, faceUp = false ) => {
			if ( faceImage != null ) {
				this.remove();
			}
			faceImage = game.add.image( x, y, type );
			downImage = game.add.image( x, y, "cardDown" );
			number = game.add.text( x, y, stringValue, {
				fill: 0xFF7550,
				font: "50px Arial",
				stroke: "#FFFFFF",
				strokeThickness: 3
			} );
			position = new Phaser.Point( x, y );

			if ( !faceUp ) {
				faceImage.alpha = 0;
				number.alpha = 0;
			} else {
				downImage.alpha = 0;
			}
		}

		/**
		 * Remove this card from play
		*/
		this.remove = () => {
			faceImage.destroy();
			downImage.destroy();
			number.destroy();
			position = null;
		}

		/**
		 * Flip this card face up
		 */
		this.flipUp = () => {
			downImage.alpha = 0;
			faceImage.alpha = 1;
			number.alpha = 1;
		}
		/**
		 * Get the value of this card
		 */
		this.getValue = () => {
			// Restrict face card to 10 except ACE which the player gets to choose
			return value > 10 ? 10 : value;
		}

		this.getFaceup = () => {
			return faceImage.alpha == 1;
		}
	}
}

/**
 * The player class which holds the logic for changing out cards
 * @param {Phaser.Game} game
 * @param {function(Array<Cards>, Array<number>, Status)} play - A function to call to to change the game state to the player
 */
class Player {
	/**
	 * A class holding the player's information
	 * @param {Phaser.Game} game
	 * @param {function(Array<Cards>, Array<number>, Status, boolean, number)} play - The function to call to update the game state based on player
	 * @param {number} playerNumber
	 * @param {Phaser.Point} playerPos
	 */
	constructor ( game, play, playerNumber, playerPos ) {
		/** @type {number} */
		let chips = 10000;
		/** @type {number} */
		let bet = 0;
		/** @type {Array<Cards>} */
		let cards = [];
		/** @type {Cards} */
		let lastCard = null;
		/** @type {Array<number>} */
		let value = [ 0, 0 ];
		/** @type {Status} */
		let status = Status.SAFE;
		/** @type {function(Player, Array<Cards>, Array<number>, Status, boolean, number)} */
		let update = play;
		let swap = false;
		/** @type {Phaser.Text} */
		let text = game.add.text( playerPos.x, playerPos.y,
			"Player " + playerNumber + "\nChips: " + chips + " / " + bet, {
				stroke: "#FFFFFF",
				strokeThickness: 3
			} );
		text.anchor.setTo( 0 );

		/**
		 * Call the given play function to update the game state
		*/
		this.play = () => {
			update( this, cards, value, status, swap, chips );
		}
		/**
		 * Give this player a card and update the state
		 * @param {Cards} card
		 */
		this.takeCard = ( card ) => {
			this.flipCard();
			lastCard = card;
			cards.push( card );

			lastCard.placeCard( playerPos.x + ( cards.length * 40 ), playerPos.y - 45 );
		}
		/**
		 * Flips the player's face down card and evaluate it
		*/
		this.flipCard = () => {
			if ( lastCard != null ) {
				lastCard.flipUp();
				value[ 0 ] += lastCard.getValue();
				if ( lastCard.getValue() == 1 ) {
					value[ 1 ] += 11;
				} else value[ 1 ] += lastCard.getValue();

				// Check the status of the player's hand
				if ( value[ 0 ] > 21 && value[ 1 ] > 21 ) {
					status = Status.BUST;
				} else if ( cards.length == 2 && ( value[ 0 ] == 21 || value[ 1 ] == 21 ) ) {
					status = Status.BJ;
				} else {
					// If the player swap out a bust, we are safe!
					status = Status.SAFE;
				}
			}
		}
		/**
		 * The given card with top face up card with he given card
		 */
		this.swap = ( card ) => {
			lastCard = null; // to prevent flipping of the facedown card
			let downFace = cards.pop(); // The last card should be the face down card
			let topFace = cards.pop(); // The second card should be the top facing

			// Remove the value of the card from the total
			if ( topFace.getValue() == 1 ) {
				value[ 1 ] -= 11
			} else value[ 1 ] -= topFace.getValue();

			this.takeCard( card ); // put the new card in
			this.takeCard( downFace ); // put the facedown back in and flip the new card
			//topFace.remove(); // remove the card image
			return topFace; // we swap, here is the old card
		}
		/**
		 * Update the player's chip count at the end
		 * @param {boolean} win
		 * @param {number} amount
		 */
		this.endgame = ( win, amount = 0 ) => {
			if ( win ) {
				chips += amount;
			} else chips -= bet;
			this.setBet( 0 );
		}
		/**
		 * Clear the player's hand to restart
		 */
		this.clearHand = () => {
			cards.forEach( ele => {
				ele.remove();
			} );
			swap = false;
			lastCard = null;
			status = Status.SAFE;
			value = [ 0, 0 ];
			let temp = cards.slice();
			cards = [];
			return temp;
		}
		/**
		 * Set the player's status to keep
		 * and remove the facedown card
		*/
		this.setKeep = () => {
			status = Status.KEEP;
			let temp = this.getFaceDownCard();
			temp.remove();
			if ( cards.length == 0 ) {
				let tempBet = bet;
				this.setBet( 0 ); // We cancel our bet
				return [ temp, tempBet ];
			}
			return [ temp, 0 ];
		}
		this.setBet = ( amount ) => {
			if ( chips <= 0 ) {
				return;
			}
			bet = amount;
			text.setText( "Player " + playerNumber + "\nChips: " + chips + " / " + bet );
		}
		/**
		 * Get the player's current status
		 */
		this.getStatus = () => {
			return status;
		}
		/**
		 * Get the instance of the face down card.
		 * Also, remove it from the player hand
		*/
		this.getFaceDownCard = () => {
			if ( lastCard.getFaceup() ) {
				throw new Error( "Last card is not face up" )
			}

			cards.splice( cards.indexOf( lastCard ), 1 );
			let temp = lastCard;
			lastCard = null;
			return temp;
		}
		/**
		 * Get the player's number
		*/
		this.getPlayerNumber = () => {
			return playerNumber;
		}
		/**
		 * Get the highest value
		*/
		this.getValue = () => {
			return value[ 1 ] <= 21 ? value[ 1 ] : value[ 0 ];
		}
		/**
		 * Get the current chip count
		 */
		this.getChip = () => {
			return chips;
		}
		/**
		 * Get the top face UP card
		 */
		this.getFaceUpTop = () => {
			// The last card is the facedown card. The second to last is the faceup top card
			if ( cards.length == 1 ) {
				return null; // We have no faceup cards
			}
			return cards[ cards.length - 2 ]
		}
		this.getPlay = () => {
			return play;
		}
	}
}
