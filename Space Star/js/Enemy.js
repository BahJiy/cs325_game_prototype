"use strict"

class Enemy {
	/**
	 * THe enemy that runs at you
	 * @param {Phaser.Game} game
	 * @param {number} x
	 * @param {number} y
	 * @param {number} hp
	 * @param {number} speed
	 */
	constructor ( game, x, y, hp, speed ) {
		let sEnemy = game.add.sprite( x, y, "enemy" );
		sEnemy.maxHealth = hp;
		sEnemy.health = hp;
		game.physics.arcade.enable( sEnemy );

		this.position = function () {
			return sEnemy.position;
		}
		/**
		 * @param {Array<Stars>} stars
		 * @param {Array<Collector|Base|Cannon|Power>} buildings
		 * @param {function(number)} upEnemyStarCount
		 */
		this.update = function ( stars, buildings, upEnemyStarCount ) {
			// Check for any collision before we start evaluating movement
			// Save some cycle
			stars.forEach( ele => {
				if ( ele.collide( sEnemy ) ) {
					ele.destroy();
					upEnemyStarCount( 10 );
				}
			} );
			buildings.forEach( ele => {
				if ( ele.collide( sEnemy ) ) {
					let temp = sEnemy.health;
					sEnemy.damage( ele.life() );
					ele.damage( temp );
					return;
				}
			} );
			if ( sEnemy.body == null ) {
				return true;
			}

			/** @type {Collector|Base|Cannon|Power|Stars} */
			let object = null;
			// We always wants to go for the star first
			if ( stars.length > 0 ) {
				let tempStar = stars[ 0 ];
				let tempDistance = sEnemy.position.distance( tempStar.position() );
				stars.forEach( ele => {
					// We want to find the closest star
					if ( sEnemy.position.distance( ele.position() ) < tempDistance ) {
						tempStar = ele;
						tempDistance = sEnemy.position.distance( ele.position() );
					}
				} );
				object = tempStar;
			} else if ( buildings.length > 0 ) {
				// Head for a building
				/** @type {Collector|Base|Cannon|Power} */
				let tempBuild = buildings[ 0 ];
				let tempDistance = sEnemy.position.distance( tempBuild.position() );
				buildings.forEach( ele => {
					// We want to find the closest star
					if ( sEnemy.position.distance( ele.position() ) < tempDistance ) {
						tempBuild = ele;
						tempDistance = sEnemy.position.distance( ele.position() );
					}
				} )
				object = tempBuild;
			} else {
				return;
			}
			let angle = object.position().angle( sEnemy.position );
			sEnemy.body.velocity.setTo(
				Math.cos( angle ) * -speed,
				Math.sin( angle ) * -speed,
			);
			sEnemy.rotation = angle + Math.PI;
		}
		this.destroy = function () {
			sEnemy.destroy();
		}
		this.damage = function ( amount ) {
			sEnemy.damage( amount );
		}
		this.status = function () {
			return sEnemy.body == null;
		}
		/** @param {Phaser.Sprite} sprite */
		this.collide = function ( sprite ) {
			return game.physics.arcade.collide( sEnemy, sprite );
		}

		sEnemy.events.onKilled.add( ( obj ) => {
			sEnemy.destroy();
		} );
	}
}

class EnemyGroup {
	/**
	 *
	 * @param {Phaser.Game} game
	 */
	constructor ( game ) {
		/** @type {Array<Enemy>} */
		let enemyArray = [];
		/** @type {Array<Phaser.Point>} */
		let spawnPoint = [ new Phaser.Point( 0, 0 ), new Phaser.Point( 1000, 750 ) ];
		let enemyStar = 0;
		let level = 0;
		let starWait = 60000;
		let starOffset = 2500;
		let enemyWait = 2500;
		let enemyOffset = 15000;

		let upEnemyStarCount = function ( amount ) {
			enemyStar += amount;
		}

		let spawn = function ( amount ) {
			amount = amount > 5 + level / 5 ? 5 + level / 5 : amount;
			for ( let i = 0; i < amount; i++ ) {
				let index = game.rnd.integerInRange( 0, spawnPoint.length - 1 );
				enemyArray.push( new Enemy( game,
					spawnPoint[ index ].x + game.rnd.integerInRange( -50, 50 ),
					spawnPoint[ index ].y + game.rnd.integerInRange( -50, 50 ),
					1 + level * 0.5, // HP
					75 + level * 5 ) );// Speed
			}
		}
		/**
		 * @param {Array<Stars>} stars
		 * @param {Array<Collector|Base|Cannon|Power|Stars>} buildings
		 * @param {Array<Bullet>} bullet
		 * @param {function(number)} upStarCount
		 */
		this.update = function ( stars, buildings, bullet, upStarCount ) {
			if ( game.time.now > starWait ) {
				enemyStar += 2 + level; // natural gain for the enemy also
				starWait = game.time.now + starOffset
			} if ( enemyStar >= 25 + level * 20 ) {
				enemyStar = 0;
				level += 1;
				enemyOffset -= level * 1.1; // go go go!
			} if ( game.time.now > enemyWait ) {
				spawn( 1 + level * Math.round( game.rnd.realInRange( 1, 1.3 ) ) );
				enemyWait = game.time.now + enemyOffset;
			}

			upClean( enemyArray, stars, buildings, upEnemyStarCount );
			/** @type {Array<[number, number]>} */
			let tempRemove = [];
			bullet.forEach( ele => {
				if ( ele.sprite().body == null ) {
					tempRemove.push( [ bullet.indexOf( ele ), -1 ] );
				} else {
					enemyArray.some( function ( en ) {
						if ( en.collide( ele.sprite() ) ) {
							en.damage( ele.power() );
							if ( en.status() ) {
								tempRemove.push( [ bullet.indexOf( ele ), enemyArray.indexOf( en ) ] );
							} else {
								tempRemove.push( [ bullet.indexOf( ele ), -1 ] );
							}
							return true;
						}
					} );
				}
			} );
			tempRemove.forEach( ele => {
				// Just in case we check too fast
				if ( bullet[ ele[ 0 ] ] != undefined ) {
					bullet[ ele[ 0 ] ].destroy();
					bullet.splice( ele[ 0 ], 1 );
					if ( ele[ 1 ] >= 0 ) {
						upStarCount( Math.ceil( level / 5 ) );
						enemyArray.splice( ele[ 1 ], 1 );
					}
				}
			} );
		}

		/**
		 * Get the angle to the closest enemy
		 * @param {Phaser.Point} position
		 */
		this.angleToClosest = function ( position ) {
			let enemy = null;
			if ( enemyArray.length > 0 ) {
				/** @type {Enemy} */
				let temp = enemyArray[ 0 ];
				let distance = enemyArray[ 0 ].position().distance( position );
				enemyArray.forEach( ele => {
					if ( position.distance( ele.position() ) < distance ) {
						temp = ele;
						distance = position.distance( ele.position() );
					}
				} );
				enemy = temp;
			}
			if ( enemy == null ) {
				return 0;
			}
			return enemy.position().angle( position ) - Math.PI / 2;
		}
	}
}
