"use strict";

/**
 * @param {Phaser.Game} game
 * @param {{help: boolean}} shared
 */
GameStates.makeMainMenu = function ( game, shared ) {

    /** @type {Array<Phaser.Image>} */
    let dot = [];
    /** @type {Phaser.Text} */
    let bStart = null;
    /** @type {TypeWriter} */
    let typewriter = null;

    function SkipChoice () {
        let yes = game.add.text( game.world.centerX, game.world.centerY, "I'm ready to start", {
            font: "25px Inconsolata",
            fill: "#FFFFFF"
        } );
        let no = game.add.text( game.world.centerX, game.world.centerY - 50, "I do not", {
            font: "25px Inconsolata",
            fill: "#FFFFFF"
        } );
        yes.inputEnabled = true;
        no.inputEnabled = true;
        yes.events.onInputUp.add( ( obj, pointer, bool ) => {
            StartGame(); // Just go to the game then
        } );
        no.events.onInputUp.add( ( obj, pointer, bool ) => {
            typewriter.startDisplay( intro2, 0, new Phaser.Point( game.world.centerX, game.world.centerY ), 20 );
            typewriter.addCallBack( StartGame );
            shared.help = true;
            yes.destroy();
            no.destroy();
        } );
    }

    function StartGame () {
        game.state.start( "Game" );
    }
    return {
        create: function () {
            game.camera.flash( 0xFFFFFF, 500, true, 1 );
            // Fill the sky with stars
            for ( let i = 0; i < game.rnd.integerInRange( 500, 750 ); i++ ) {
                dot.push( game.add.image( game.world.randomX, game.world.randomY, "dot" ) );
            }

            typewriter = new TypeWriter( game, "Inconsolata", 0.5 );
            bStart = game.add.text( game.world.centerX, game.world.centerY, "Start", {
                fill: "#FFFFFFFF",
                font: "30px Source Code Pro",
                stroke: "#FF9050",
                strokeThickness: 3
            } );

            bStart.inputEnabled = true;
            bStart.events.onInputUp.add( ( obj, sender, over ) => {
                if ( over ) {
                    typewriter.startDisplay( intro1, 0, new Phaser.Point( game.world.centerX, game.world.centerY ), 20 );
                    typewriter.addCallBack( SkipChoice );
                    bStart.destroy(); // Kill the button!
                }
            } );

            game.add.text( game.world.centerX, 147, "Space Star", {
                font: "75px BioRhyme Expanded",
                stroke: "#FFFFFF",
                strokeThickness: 5
            } );

            if (shared.music == null) {
                shared.music = game.add.audio( "music", 0.5, true );
                shared.music.play();
            }
        },

        render: function () {
            //game.debug.text( "Pointer x: " + game.input.position.x + " y: " + game.input.position.y, 25, 25 );
        },

        update: function () {
            typewriter.update();
        }
    };
};
