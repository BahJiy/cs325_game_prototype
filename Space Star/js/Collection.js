"use strict"

class Stars {
	/**
	 *
	 * @param {Phaser.Game} game
	 * @param {number} angle - Angle in radian
	 * @param {number} worth
	 */
	constructor ( game, angle, worth ) {
		let sStar = game.add.sprite( game.width - 30, 30, "stars" );
		game.physics.arcade.enable( sStar );
		let speed = 500;
		let velocity = new Phaser.Point(
			speed * Math.cos( angle ),
			speed * Math.sin( angle )
		)

		// y = mx + b
		let slope = -velocity.y / velocity.x;
		let yIntercept = ( game.height - 30 ) - ( slope * ( game.width - 30 ) );
		let xIntercept = ( 0 - yIntercept ) / slope;
		let upStarCountHack = null;
		sStar.events.onInputDown.add( ( obj, pointer, bool ) => {
			upStarCountHack( worth * 2 );
			sStar.destroy();
		} );

		this.getIntercept = function () {
			return new Phaser.Point( xIntercept, game.height - yIntercept );
		}
		this.y = function () {
			return sStar.position.y;
		}
		this.x = function () {
			return sStar.position.x;
		}
		this.position = function () {
			return sStar.position;
		}
		/** @param {Phaser.Sprite} sprite */
		this.collide = function ( sprite ) {
			return game.physics.arcade.collide( sStar, sprite );
		}
		/**
		 * @param {Collector} collector
		 * @param {number} starCount
		 * @param {function(number)} starCount
		 * @param {Phaser.Sprite} earth
		 */
		this.update = function ( collector, starCount, upStarCount, earth ) {
			if ( sStar.body == null ) {
				return true;
			}
			if ( new Phaser.Rectangle(
				earth.x - earth.width / 2, earth.y - earth.height / 2, // we have to do this b/c anchor = 0.5
				earth.width, earth.height ).contains( sStar.x, sStar.y ) ) {
				sStar.tint = 0xfff026;
				sStar.inputEnabled = true;
				velocity.setMagnitude( 250 );
				upStarCountHack = upStarCount;
			}
			if ( sStar.y > game.height || sStar.x < 0 ) {
				return true;
			} else if ( collector.contains( this ) ) {
				upStarCount( worth );
				return true;
			}
			sStar.body.velocity = velocity;
			sStar.rotation += 0.2;
			return false;
		}
		this.destroy = function () {
			sStar.destroy();
		}
	}
}

class Collector {
	/**
	 * @param {Phaser.Game} game
	 * @param {function(string, number, number, number, number, number)} upgradeText
	 * @param {function(number)} upgradeCheck
	 * @param {function()} lowEnergy
	 */
	constructor ( game, upgradeText, upgradeCheck, lowEnergy ) {
		/** @type {Phaser.Sprite} */
		let sCollector = game.add.sprite( 16, game.world.centerY, "collector" );
		sCollector.maxHealth = 8;
		sCollector.health = sCollector.maxHealth;
		/** @type {Phaser.Rectangle} */
		let rect = new Phaser.Rectangle( sCollector.x - sCollector.width / 2,
			sCollector.y - sCollector.height / 2, sCollector.width, sCollector.height );;

		game.physics.enable( sCollector );

		let healWait = 0;
		let healAmount = 0.5;
		let healOffset = 30000;

		let energyCost = 25;
		let upgradeCost = 50;
		let level = 0;
		let speed = 200;
		/** @type {Phaser.Point} */
		let velocity = new Phaser.Point( 0, speed );
		sCollector.inputEnabled = true;
		// If the player click the collector, we upgrade it
		sCollector.events.onInputUp.add( ( obj, pointer, over ) => {
			if ( level < 10 && upgradeCheck( upgradeCost ) ) {
				level++;
				speed += 50 + level * 4;
				upgradeCost += level * 5 + 50;
				energyCost += level * 5;
				sCollector.maxHealth += 3;
				sCollector.heal( 1.5 );
				healAmount += 0.1;

				// Immediate apply the effect
				let neg = 1;
				if ( velocity.x < 0 || velocity.y < 0 ) {
					neg = -1;
				}
				velocity.x = velocity.x != 0 ? speed * neg : 0;
				velocity.y = velocity.y != 0 ? speed * neg : 0;
			}
		} );
		// Tell the upgrade cost on hover
		sCollector.events.onInputOver.add( ( obj, pointer ) => {
			if ( level < 10 ) {
				upgradeText( "Collector", upgradeCost, 0, ( 1 + level ) * 5, sCollector.health, sCollector.maxHealth );
			} else {
				upgradeText( "Collector", null, null, null, null, null );
			}
		} );
		/**
		 * @param {EnemyGroup} enemies
		 * @param {Array<Stars>} stars
		 */
		this.update = function ( enemies, stars ) {
			sCollector.body.velocity.setTo( 0 );
			if ( game.time.now > healWait ) {
				sCollector.heal( healAmount );
				healWait = game.time.now + healOffset;
			}
			// Go left/right
			if ( sCollector.y > game.height - sCollector.height / 2 + 25 ) {
				sCollector.angle = 90;
				velocity.setTo( -speed, 0 ); // Initial going left
				sCollector.y = game.height - sCollector.width / 2;
				rect.width = sCollector.height;
				rect.height = sCollector.width;
				// Go up/down
			} else if ( sCollector.x < 0 - sCollector.width / 2 + 10 ) {
				sCollector.angle = 0;
				velocity.setTo( 0, speed ); // Initial going down
				sCollector.x = sCollector.width / 2;
				rect.height = sCollector.height;
				rect.width = sCollector.width;
			}
			if ( lowEnergy() ) {
				velocity.setMagnitude( speed - 75 );
			}

			// Logic for movement
			// If there is something to collect
			if ( stars.length > 0 ) {
				/** @type {Stars} */
				let tempStar = stars[ 0 ];
				// We always collect the first one, unless we upgrade?
				/* If the y intercept is greater than the game height,
				 * than that mens that the star will hit the x intercept
				 * first so we need to go there to get the star.
				 * If the collector's angle is 0, than means it is
				 * going up and down based on the if block above.
				 *
				 * Else if the star's y intercept is between the game bound,
				 * it will hit the y axis before the x. So  we need to go
				 * up and down. angle of 90 means we are going left/right
				 */
				if ( ( tempStar.getIntercept().y > game.height && sCollector.angle == 0 ) ||
					( tempStar.getIntercept().y < game.height && sCollector.angle == 90 ) ) {
					sCollector.body.velocity.add( velocity.x, velocity.y );
				} else {
					/* We want to be where the star will arrive ahead of time
					*/
					if ( ( sCollector.angle == 0 && tempStar.getIntercept().y < sCollector.y ) ||
						( sCollector.angle == 90 && tempStar.getIntercept().x > sCollector.x ) ) {
						/* We reverse the original velocity
						 * The original velocity always try to make the collector go towards the origin
						 * But if the star position is on top or right of the collector
						 * we need to move opposite of the original velocity
						 */
						sCollector.body.velocity.add( -velocity.x, -velocity.y );
					} else {
						sCollector.body.velocity.add( velocity.x, velocity.y );
					}
				}
			}

			// Update the rectangle
			rect.x = sCollector.x - rect.width / 2;
			rect.y = sCollector.y - rect.height / 2;
		}
		this.powerCost = function () {
			return energyCost;
		}
		/**
		 * Check if Star is inside collector. Inside is defined as
		 * if the center of the star is inside collector
		 * @param {Stars} star
		 */
		this.contains = function ( star ) {
			return rect.contains( star.x(), star.y() );
		}
		this.rectangle = function () {
			return rect;
		}
		this.position = function () {
			return sCollector.position;
		}
		/** @param {Phaser.Sprite} sprite */
		this.collide = function ( sprite ) {
			// Arcade physic doesn't do rotation collision that well...
			return rect.intersects( new Phaser.Rectangle( sprite.x, sprite.y, sprite.width, sprite.height ), 0 );
		}
		this.damage = function ( amount ) {
			sCollector.damage( amount * 0.5 );
		}
		this.life = function () {
			return sCollector.health;
		}
	}
}
