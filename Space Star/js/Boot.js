"use strict";

let GameStates = {};
/**
 *
 * @param {Phaser.Game} game
 */
GameStates.makeBoot = function ( game ) {
    return {
        init: function () {
            game.stage.disableVisibilityChange = true;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            PIXI.Sprite.defaultAnchor = { x: 0.5, y: 0.5 };
        },

        preload: function () {
            game.load.audio( "music", "assets/music.mp3" );
            game.load.image( "earth", "assets/earth.png" );
            game.load.image( "background", "assets/background.png" );

            game.load.image( "enemy", "assets/enemy.png" );
            game.load.image( "dot", "assets/dot.png" );
            game.load.image( "bullet", "assets/bullet.png" );
            game.load.image( "stars", "assets/star.png" );
            game.load.image( "base", "assets/base.png" );
            game.load.image( "collector", "assets/collector.png" );
            game.load.image( "cannon", "assets/cannon.png" );
            game.load.image( "power", "assets/power.png" );

            game.load.image( "powerB", "assets/powerB.png" );
            game.load.image( "cannonB", "assets/cannonB.png" );
        },

        create: function () {
            // Go to the actually loader to load the rest
            game.state.start( 'MainMenu' );
        }
    };
};
