"use strict"

/**
 * @param {Phaser.Game} game
 * @param {{help: boolean}} shared
 */
GameStates.makeGame = function ( game, shared ) {

	/* Sprites */
	/** @type {Array<Phaser.Sprite>} */
	let dot = [];
	/** @type {Phaser.Image} */
	let earth = null;
	/** @type {Array<Stars>} */
	let stars = [];
	/** @type {Array<Collector|Base|Cannon|Power>} */
	let buildings = [];
	/** @type {EnemyGroup} */
	let enemies = null;
	/** @type {Array<Bullet>} */
	let bullet = [];
	/** @type {Base} */
	let base = null;
	/** @type {Collector} */
	let collector = null;
	/** @type {Phaser.Button} */
	let collectorButton = null;

	/* Number */
	let wishCount = 0;
	let energyCost = 0;
	let buildType = 0;
	let generousWait = 0;
	let dotAdd = 0;

	/* Text */
	/** @type {Phaser.Text} */
	let tScore = null;
	/** @type {Phaser.Text} */
	let tInfo = null;
	/** @type {TypeWriter} */
	let typewriter = null;

	/** @type {number} */
	let basicStage = Infinity;
	/** @type {boolean} */
	let pause = false;
	/** @type {number} */
	let basicWait = 0;

	function lowEnergy () {
		return energyCost < 0;
	}
	function upStarCount ( amount ) {
		wishCount += amount;
	}
	function upgradeCostText ( name, starCost, energyGen, energyCost, curHP, maxHP ) {
		if ( !pause ) {
			if ( starCost == null ) {
				tInfo.setText( name + " is max level!" );
			} else {
				// Add a new text to display the information
				let tempNum = ( energyGen - energyCost );
				let str = tempNum > 0 ? "+" + tempNum : tempNum;
				tInfo.setText( "Upgrade " + name + " >> Cost: " + starCost + " Energy: " + str + "\t\t | Current HP: " +
					curHP + "/" + maxHP );
			}
		}
	}
	function upgradeCheck ( cost ) {
		if ( !pause ) {
			if ( wishCount >= cost ) {
				wishCount -= cost;
				return true;
			}
			return false;
		}
	}
	function unPause () {
		basicStage++;
		game.physics.arcade.isPaused = false;
	}

	return {
		create: function () {
			game.camera.flash( 0xFFFFFF, 2000, true, 1 );
			game.add.image( game.world.centerX, game.world.centerY, "background" );
			game.physics.startSystem( Phaser.Physics.ARCADE );

			// Fill the sky with new stars
			for ( let i = 0; i < game.rnd.integerInRange( 750, 1250 ); i++ ) {
				dot.push(game.add.image( game.world.randomX, game.world.randomY, "dot" ));
			}
			let tempButton = game.add.button( 975, 575, "cannonB", () => {
				if ( upgradeCheck( 50 ) ) {
					buildType = 1;
				} else {
					tInfo.setText( "Not enough stars to build cannon!" );
				}
			} );
			tempButton.inputEnabled = true;
			tempButton.events.onInputOver.add( ( obj, pointer ) => {
				tInfo.setText( "Create a Cannon >> Cost: 50 Energy: -25" );
			} );
			tempButton = game.add.button( 975, 625, "powerB", () => {
				if ( upgradeCheck( 40 ) ) {
					buildType = 2;
				} else {
					tInfo.setText( "Not enough stars to build generator!" );
				}
			} );
			tempButton.inputEnabled = true;
			tempButton.events.onInputOver.add( ( obj, pointer ) => {
				tInfo.setText( "Create a Generator >> Cost: 40 Energy: +60" );
			} );
			collectorButton = game.add.button( 975, 690, "collector", () => {
				// Only 1 collector at a time
				if ( upgradeCheck( 250 ) && collector.life() <= 0 ) {
					collector = new Collector( game, upgradeCostText, upgradeCheck, lowEnergy );
					buildings.push( collector );
				} else {
					tInfo.setText( "Not enough stars to build collector!" );
				}
			} );
			collectorButton.inputEnabled = false;
			collectorButton.events.onInputOver.add( ( obj, pointer ) => {
				tInfo.setText( "Create a Collector >> Cost: 250 Energy: -25" );
			} );
			collectorButton.visible = false;

			enemies = new EnemyGroup( game );
			base = new Base( game, upStarCount, upgradeCostText, upgradeCheck, lowEnergy );
			collector = new Collector( game, upgradeCostText, upgradeCheck, lowEnergy );
			buildings.push( base );
			buildings.push( collector );

			earth = game.add.image( 230, 500, "earth" );

			tScore = game.add.text( 25, 0, "", {
				font: "25px Inconsolata",
				stroke: "#FFFFFFFF",
				strokeThickness: 3
			} );
			tScore.anchor.setTo( 0 );
			tInfo = game.add.text( 25, 35, "", {
				font: "18px Source Code Pro",
				stroke: "#FFFFFFFF",
				strokeThickness: 3
			} );
			tInfo.anchor.setTo( 0 );

			typewriter = new TypeWriter( game, "Source Code Pro" );
			if ( shared.help ) {
				basicStage = 0;
				game.physics.arcade.isPaused = true;
				typewriter.startDisplay( basicText, 0, new Phaser.Point( 50, 100 ), 20 );
				typewriter.addCallBack( unPause );
			}
			generousWait = game.time.now + 30000;
		},

		render: function () {
			if ( basicStage == 0 ) {
				game.debug.rectangle( collector.rectangle(), "#f21359", false );
				game.debug.rectangle( new Phaser.Rectangle( 0, 0, 915, 75 ),
					"#10ef4f", false );
			} else if ( basicStage == 1 && !typewriter.finished() ) {
				game.debug.rectangle( new Phaser.Rectangle( 945, 490, 75, 270 ),
					"#10ef4f", false );
			}
			//game.debug.text( "Mouse X:" + game.input.activePointer.worldX + " Y:" + game.input.activePointer.worldY, 25, 75 );
			//game.debug.text( "Angle: " + base.position().angle( game.input.activePointer.position, true ), 25, 100 );
		},

		update: function () {
			if ( typewriter.finished() ) {
				if ( base.life() <= 0 ) {
					game.state.start( "MainMenu" );
				}
				let tempEnergy = 0;
				if ( basicStage != 2 && basicStage != 4 ) {
					buildings.forEach( ele => {
						if ( ele.life() <= 0 ) {
							buildings.splice( buildings.indexOf( ele ), 1 );
						} else {
							ele.update( enemies, stars )
							tempEnergy -= ele.powerCost();
						}
					} );
					energyCost = tempEnergy;
					upClean( stars, collector, wishCount, upStarCount, earth );
					// Update score text
					tScore.setText( "Wish Count: " + wishCount + "\t\tEnergy: " + energyCost + "\t\tLow Energy: " + lowEnergy() );
				}
				if ( lowEnergy() ) {
					tScore.addColor( "#db002b", 0 );
					tScore.addStrokeColor( "#ffffff", 0 );
					tScore.strokeThickness = 3;
				} else {
					tScore.addColor( "#000000", 0 );
					tScore.addStrokeColor( "#ffffff", 0 );
					tScore.strokeThickness = 3;
				}
				earth.rotation += 0.001;

				if ( game.input.activePointer.leftButton.isDown ) {
					switch ( buildType ) {
						case 1: buildings.push( new Cannon( game,
							game.input.activePointer.position.x,
							game.input.activePointer.position.y,
							bullet, upgradeCostText, upgradeCheck, lowEnergy ) );
							if ( basicStage == 2 ) {
								unPause(); // We built a cannon
							}
							break;
						case 2: buildings.push( new Power( game,
							game.input.activePointer.position.x,
							game.input.activePointer.position.y,
							upgradeCostText, upgradeCheck, lowEnergy ) );
							if ( basicStage == 4 ) {
								unPause(); // We built a generator
								basicWait = game.time.now + 10000;
							}
							break;
						default:
							break;
					}
					buildType = 0;
				}

				let tempRemove = [];
				bullet.forEach( ele => {
					if ( ele.outOfBound() ) {
						ele.destroy();
					}
				} );
				// Only spawn enemies after we introduce them
				if ( basicStage >= 6 && game.time.now > generousWait ) {
					enemies.update( stars, buildings, bullet, upStarCount );
				}
				if ( collector.life() <= 0 ) {
					collectorButton.visible = true;
					collectorButton.inputEnabled = true;
				} else {
					collectorButton.visible = false;
					collectorButton.inputEnabled = false;
				}
				// Continue tutorial
				if ( basicStage == 1 && wishCount >= 50 ) { // cannon
					game.physics.arcade.isPaused = true;
					typewriter.startDisplay( buildText, 0, new Phaser.Point( 50, 100 ), 20 );
					basicStage++;
				} else if ( basicStage == 3 && wishCount >= 40 ) {
					game.physics.arcade.isPaused = true;
					typewriter.startDisplay( energyText, 0, new Phaser.Point( 50, 100 ), 20 );
					basicStage++;
				} else if ( basicStage == 5 && game.time.now > basicWait ) {
					game.physics.arcade.isPaused = true;
					typewriter.startDisplay( enemyText, 0, new Phaser.Point( 50, 100 ), 20 );
					typewriter.addCallBack( unPause );
				} else if ( basicStage == 6 && collector.life() <= 0 ) {
					game.physics.arcade.isPaused = true;
					typewriter.startDisplay( collectorText, 0, new Phaser.Point( 50, 100 ), 20 );
					typewriter.addCallBack( unPause );
				}
			} else {
				typewriter.update();
			}

			if (dotAdd < game.time.now) {
                for ( let i = 0; i <= 10; i++ ) {
                    dot[0].destroy();
                    dot.splice(0, 1);
				    dot.push(game.add.image( game.world.randomX, game.world.randomY, "dot" ));
                }
                dotAdd = game.time.now + 1000;
            }
		}
	};
}
