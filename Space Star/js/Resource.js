/**
 * Call update on all element in array. If the update function
 * returns true, will remove the element from the array
 * @param {Array<any>} array
 * @param {Array<any>} args
 */
function upClean ( array, ...[ a, b, c, d ] ) {
	/** @type {Array<number>} */
	let removeArray = [];

	// Update the elements
	array.forEach( ele => {
		if ( ele.update( a, b, c, d ) == true ) {
			removeArray.push( array.indexOf( ele ) );
		}
	} );
	// Remove from array
	removeArray.forEach( ele => {
		// To prevent too quick of deletion
		if ( array[ ele ] != undefined ) {
			array[ ele ].destroy();
		}
		array.splice( ele, 1 );
	} );
}

let col1 = "#FFFF50";
let col2 = "#FF5090";
let intro1 = [
	[ "Greeting Executor.\n", col1, true ],
	[ "I assume you know why you are called here?", col1, false ]
];
let intro2 = [
	[ "Understood", col1, true ],
	[ "You are called here to oversee this jurisdiction, Earth.\n", col1, true ],
	[ "As you probably know, our race relies on the power of wishes\n", col1, false ],
	[ "However, our race is growing large and thus need to expand.", col1, false ],
	[ "From our sources, this planet Earth has many so called 'Stargazers.'\n", col1, true ],
	[ "These 'Stargazers' appears to have a tradition of making a wish\n\
when they see a shooting star.\n", col1, false ],
	[ "While we cannot grant them their wishes, we can rely on them to give\n\
us wishes to continue expanding.", col1, false ],
	[ "We have already set up a small base for you to start with. Your job is\n\
to keep this base alive as long as possible.", col1, true ],
	[ "You are to keep this base safe to allow it to continually shoot stars.\n", col1, true ],
	[ "While this base shoots star, the 'Stargazers' will make wishes.\n", col1, false ],
	[ "I hope you understand the importance of this task.\n", col1, false ],
	[ "An assistant will help you get started on the basic, but it will\n\
be your responsibility to ensure the survival of this base.", col1, false ],
];

let basicText = [
	[ TypeWriter.TYPE.WAIT, 750 ],
	[ "Greeting Executor. I am your assistance.\n", col2, true ],
	[ "I will walk you through the basic of running a star base.", col2, false ],
	[ "On your top right is Base. The most important building.\n", col2, true ],
	[ "It will shoot out stars at certain intervals for the Stargazers\n\
to wish upon. It may also pick up random wishes from Earth.\n", col2, false ],
	[ "If Base is destroy, you will have failed your mission.\n", col2, false ],
	[ "\nOn your right, highlighted red, is your collector.\n", col2, false ],
	[ "Its job is to move and collect the stars that Base shoots out.\n", col2, false ],
	[ "As long as the collector collects the stars, you will gain wishes.\n", col2, false ],
	[ "While collector is not essential to survival, it is of great importance\n\
for your survival.", col2, false ],
	[ " It is extremely costly to rebuild, so keep it safe.", col2, false ],
	[ "On the top is the information bar.\n", col2, true ],
	[ "It displays your current wish count and energy level.\n", col2, false ],
	[ "Wishes are important to build and upgrade your buildings.\n", col2, false ],
	[ "It is helpful to keep in mind your energy level also.\n\
Negative energy level will slow down your building. Be wary.", col2, false ],
	[ "I will let you get a feel of how every works before continuing.", col2, true ],
	[ "Also, I heard that if a star directly goes above Earth, more stargazers\n\
see it and so it gets more wishes; however, you have to to collect it\n\
yourself by clicking it.\n", col2, true ],
	[ "I don't know if it is true or not, but doesn't hurt to try.", col2, false ],
];

let buildText = [
	[ "Good, it looks like your Base and collector are working perfectly fine.", col2, true ],
	[ "Now that you have some wishes, you should build a cannon to help protect\n\
your base from enemies attacks.", col2, true ],
	[ "On the bottom right side is your build panel.\n", col2, true ],
	[ "The top button is the cannon whereas the second button is the power generator.\n", col2, false ],
	[ "For now, lets build a cannon.", col2, false ],
];

let energyText = [
	[ "It appears that building a cannon required more energy than you have.\n", col2, true ],
	[ "When you are in negative energy, Base will fire less stars and the collector\n\
will move a slower. You cannon will also fire less so be careful!\n", col2, false ],
	[ "To get energy, build a power generator. It is the second button on the\n\
bottom right side.", col2, false ],
];

let enemyText = [
	[ "Warning!! Enemies are slowly approaching! ETA: NOW!\n", col2, true ],
	[ "It appears that the Earthlings are sending aircrafts towards the Base.\n", col2, false ],
	[ "They seems to be interested in dismantling our buildings...\n", col2, false ],
	[ "May be they are trying to steal our technology?\n", col2, false ],
	[ "Regardless! You need to defend the base!\n", col2, true ],
	[ "As time passes, the enemy is sure to send strong ships and in larger quantity.\n", col2, false ],
	[ "You should upgrade your buildings to make it easier to defend. Just click\n\
on the building you want to upgrade and if you have the require number of\n\
wishes, it will upgrade.", col2, false ],
	[ "Generally, this is what happens when you upgrade:\n", col2, true ],
	[ "Cannon: Increase fire rate, bullet speed and damage\n\
Power Generator: Increase energy\n\
Collector: Increase speed\n\
Base: Increase star rate and star value\n", col2, false ],
	[ "\nEach upgrade will increase that building's total health pool and heal\n\
rate also\n", col2, false ],
	[ "Good luck Executor!", col2, false ],
];

let collectorText = [
	[ "So you let the collector died...\n", col2, true ],
	[ "We can give you a new collector, but it will not be cheap.\n", col2, false ],
	[ "Note that you can only have one collector at a time.\n", col2, false ],
	[ "A building collector button has been enable for now.\n", col2, false ],
];
