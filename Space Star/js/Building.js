"use strict"

class Base {
	/**
	 * @param {Phaser.Game} game
	 * @param {function(number)} upStarCount
	 * @param {function(string, number, number, number, number, number)} upgradeText
	 * @param {function(number)} upgradeCheck
	 * @param {function()} lowEnergy
	 */
	constructor ( game, upStarCount, upgradeText, upgradeCheck, lowEnergy ) {
		let sBase = game.add.sprite( game.width - 30, 30, "base" );
		game.physics.arcade.enable( sBase );
		sBase.events.onKilled.add( ( obj ) => {
			game.state.start( "MainMenu" );
		} )
		sBase.body.immovable = true;
		sBase.maxHealth = 10;
		sBase.health = sBase.maxHealth;

		let starSpawn = 5000;
		let naturalSpawn = 3500;
		let starWait = 0;
		let naturalWait = 0;
		const starSpawnOffset = 1000;
		const naturalSpawnOffset = 500;

		let healWait = 0;
		let healAmount = 0.25;
		const healOffset = 35000;

		let energyCost = 10;
		let energyGen = 40;
		let starWeight = 20;
		let upgradeCost = 150;
		let level = 0;
		sBase.inputEnabled = true;
		// If the player click the collector, we upgrade it
		sBase.events.onInputUp.add( ( obj, pointer, over ) => {
			if ( level < 15 && upgradeCheck( upgradeCost ) ) {
				level++;
				starSpawn -= 350;
				starWeight += 4 + level;
				upgradeCost += level * 5 + 150;
				energyCost += level * 2;
				energyGen += level * 3;
				healAmount += 0.1;
				sBase.maxHealth += 5;
				sBase.heal( 3.5 );
			}
		} );
		// Tell the upgrade cost on hover
		sBase.events.onInputOver.add( ( obj, pointer ) => {
			if ( level < 15 ) {
				upgradeText( "Base", upgradeCost, ( 1 + level ) * 3, ( 1 + level ) * 2, sBase.health, sBase.maxHealth );
			} else {
				upgradeText( "Base", null, null, null, null, null );
			}
		} );

		this.powerCost = function () {
			return energyCost - energyGen;
		}
		this.position = function () {
			return sBase.position;
		}
		/** @param {Phaser.Sprite} sprite */
		this.collide = function ( sprite ) {
			return game.physics.arcade.collide( sBase, sprite );
		}
		this.life = function () {
			return sBase.health;
		}
		this.damage = function ( amount ) {
			sBase.damage( amount );
		}

		/**
		 * @param {EnemyGroup} enemies
		 * @param {Array<Stars>} stars
		 */
		this.update = function ( enemies, stars ) {
			if ( game.time.now > starWait + ( lowEnergy() ? 1000 : 0 ) ) {
				stars.push( new Stars( game, Math.PI / 180 * game.rnd.realInRange( 100, 170 ), starWeight ) );
				starWait = game.time.now + game.rnd.integerInRange( starSpawn - starSpawnOffset, starSpawn );
			}
			if ( game.time.now > naturalWait ) {
				// We want to give a bit of help to those who like low energy
				upStarCount( Math.floor( starWeight / 10 ) + ( lowEnergy() ? 3 : 0 ) );
				naturalWait = game.time.now + game.rnd.integerInRange( naturalSpawn - naturalSpawnOffset, naturalSpawn );
			} if ( game.time.now > healWait ) {
				sBase.heal( healAmount );
				healWait = game.time.now + healOffset;
			}
		}
	}
}

class Power {
	/**
	 * @param {Phaser.Game} game
	 * @param {number} x
	 * @param {number} y
	 * @param {function(string, number, number, number, number, number)} upgradeText
	 * @param {function(number)} upgradeCheck
	 * @param {function()} lowEnergy
	 */
	constructor ( game, x, y, upgradeText, upgradeCheck, lowEnergy ) {
		let sPower = game.add.sprite( x, y, "power" );
		game.physics.arcade.enable( sPower );
		sPower.body.immovable = true;
		sPower.maxHealth = 4;
		sPower.health = sPower.maxHealth;

		let healWait = 0;
		const healOffset = 25000;
		let healAmount = 0.1;

		let energyGen = 50;
		let upgradeCost = 25;
		let level = 0;
		sPower.inputEnabled = true;
		// If the player click the collector, we upgrade it
		sPower.events.onInputUp.add( ( obj, pointer, over ) => {
			if ( level < 10 && upgradeCheck( upgradeCost ) ) {
				level++;
				energyGen += 60 + level * 40;
				upgradeCost += level * 3 + 20;
				sPower.maxHealth += 3
				healAmount += 0.05;
				sPower.heal( 1.5 );
			}
		} );
		// Tell the upgrade cost on hover
		sPower.events.onInputOver.add( ( obj, pointer ) => {
			if ( level < 10 ) {
				upgradeText( "Power Generator", upgradeCost, 60 + ( 1 + level ) * 10, 0, sPower.health, sPower.maxHealth );
			} else {
				upgradeText( "Power Generator", null, null, null, null, null );
			}
		} );

		/**
		 * @param {EnemyGroup} enemies
		 * @param {Array<Stars>} stars
		 */
		this.update = function ( enemies, stars ) {
			if ( game.time.now > healWait ) {
				sPower.heal( healAmount );
				healWait = game.time.now + healOffset;
			}
		}

		// This makes energy not use
		this.powerCost = function () {
			return -energyGen;
		}
		this.position = function () {
			return sPower.position;
		}
		/** @param {Phaser.Sprite} sprite */
		this.collide = function ( sprite ) {
			return game.physics.arcade.collide( sPower, sprite );
		}
		this.life = function () {
			return sPower.health;
		}
		this.damage = function ( amount ) {
			sPower.damage( amount * 0.7 );
		}
	}
}
class Bullet {
	constructor ( game, damage, speed, angle, x, y ) {
		/** @type {Phaser.Sprite} */
		let sBullet = game.add.sprite( x, y, "bullet" );
		game.physics.arcade.enable( sBullet );
		sBullet.body.velocity.setTo(
			Math.cos( angle - Math.PI / 2 ) * speed,
			Math.sin( angle - Math.PI / 2 ) * speed,
		)

		this.sprite = function () {
			return sBullet;
		}
		this.power = function () {
			return damage;
		}
		this.destroy = function () {
			if ( sBullet.body != null ) {
				sBullet.destroy();
			}
		}
		this.outOfBound = function () {
			return !new Phaser.Rectangle( 0, 0, game.width, game.height ).contains( sBullet.position.x, sBullet.position.y );
		}
	}
}
class Cannon {
	/**
	 * @param {Phaser.Game} game
	 * @param {number} x
	 * @param {number} y
	 * @param {Array<Bullet>} bullet
	 * @param {function(string, number, number, number, number, number)} upgradeText
	 * @param {function(number)} upgradeText
	 * @param {function()} lowEnergy
	 */
	constructor ( game, x, y, bullet, upgradeText, upgradeCheck, lowEnergy ) {
		let sCannon = game.add.sprite( x, y, "cannon" );
		sCannon.maxHealth = 5;
		sCannon.health = sCannon.maxHealth;
		game.physics.arcade.enable( sCannon );
		sCannon.body.immovable = true;

		let healWait = 0;
		let healAmount = 0.15;
		const healOffset = 30000;

		let shootWait = 700;
		let power = 0.3;
		let speed = 1000;
		let energyCost = 25;
		let upgradeCost = 50;
		let level = 0;
		let shotTime = game.time.now + shootWait;
		sCannon.inputEnabled = true;
		// If the player click the collector, we upgrade it
		sCannon.events.onInputUp.add( ( obj, pointer, over ) => {
			if ( level < 10 && upgradeCheck( upgradeCost ) ) {
				level++;
				power += 0.3;
				speed += 225;
				shootWait -= 50;
				energyCost += level * 3;
				upgradeCost += level * 5 + 25;
				healAmount += 0.1;
				sCannon.maxHealth += 3;
				sCannon.heal( 1.5 );
			}

		} );
		// Tell the upgrade cost on hover
		sCannon.events.onInputOver.add( ( obj, pointer ) => {
			if ( level < 10 ) {
				upgradeText( "Cannon", upgradeCost, 0, ( 1 + level ) * 3, sCannon.health, sCannon.maxHealth );
			} else {
				upgradeText( "Cannon", null, null, null, null, null );
			}
		} );

		this.powerCost = function () {
			return energyCost;
		}
		this.position = function () {
			return sCannon.position;
		}
		/** @param {Phaser.Sprite} sprite */
		this.collide = function ( sprite ) {
			return game.physics.arcade.collide( sCannon, sprite );
		}
		this.damage = function ( amount ) {
			sCannon.damage( amount * 0.8 );
		}
		this.life = function () {
			return sCannon.health;
		}
		/**
		 * @param {EnemyGroup} enemies
		 * @param {Array<Stars>} stars
		 */
		this.update = function ( enemies, stars ) {
			let tempAngle = enemies.angleToClosest( sCannon.position );
			if ( tempAngle != 0 ) {
				sCannon.rotation = tempAngle;
				if ( game.time.now > shotTime + ( lowEnergy() ? 350 : 0 ) ) {
					bullet.push( new Bullet( game, power, speed, sCannon.rotation,
						sCannon.position.x, sCannon.position.y ) );
					shotTime = game.time.now + shootWait;
				}
			} if ( game.time.now > healWait ) {
				sCannon.heal( healAmount );
				healWait = game.time.now + healOffset
			}
		}
	}
}
