"use strict";
let game = new Phaser.Game( 1000, 750, Phaser.CANVAS, 'game' );
window.onload = function () {

	//	Create your Phaser game and inject it into the 'game' div.
	//	We did it in a window.onload event, but you can do it anywhere (requireJS load, anonymous function, jQuery dom ready, - whatever floats your boat)
	/** @type {Phaser.Game} */

	let WebFontConfig = {
		// @ts-ignore
		active: function () { game.time.events.add( Phaser.Timer.SECOND, null, this ); },
		google: {
			families: [ 'Inconsolata', 'BioRhyme Expanded', 'Source Code Pro' ]
		}
	};

	// Add any shared objects
	/** @type {{help: boolean}} */
	let shared = { help: false };

	// Add the states
	game.state.add( 'Boot', GameStates.makeBoot( game ) );
	game.state.add( 'MainMenu', GameStates.makeMainMenu( game, shared ) );
	game.state.add( 'Game', GameStates.makeGame( game, shared ) );

	game.state.start( 'Boot' );

};
window.onbeforeunload = function ( e ) {
	game.world.removeAll();
	game.destroy();
};
