"use strict";

let GameStates = {};
/**
 *
 * @param {Phaser.Game} game
 */
GameStates.makeBoot = function ( game ) {
    return {
        init: function () {
            game.stage.disableVisibilityChange = true;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;

            game.input.mouse.capture = true;
            game.canvas.oncontextmenu = ( e ) => { e.preventDefault(); }
        },

        preload: function () {
            game.load.image( "instruction", "assets/instructions.png" );
            game.load.image( "teapot", "assets/teapot.png" );
            game.load.image( "tea", "assets/tea.png" );
            game.load.image( "tilemap", "assets/tilemap.png" );
            game.load.image( "0", "assets/block.png" );
            game.load.image( "1", "assets/left.png" );
            game.load.image( "2", "assets/right.png" );
            game.load.image( "3", "assets/upL.png" );
            game.load.image( "4", "assets/upR.png" );
            game.load.image( "droplet", "assets/droplet.png" );
        },

        create: function () {
            // Go to the actually loader to load the rest
            game.state.start( 'MainMenu' );
        }
    };
};
