"use strict"

/**
 * @param {Phaser.Game} game
 * @param {{levelIndex: number, tileInfo: Object.<number, number[][]>}} shared
 */
GameStates.Creator = function ( game, shared ) {
	/** @type {Phaser.Group} */
	let tiles
	/** @type {Phaser.Graphics} */
	let square = null, polyGraph = null;
	/** @type {number} */
	let limit = 0;
	/** @type {string} */
	let index = "0";
	/** @type {Phaser.Text} */
	let infoText = null, start = null, clear = null;
	/** @type {TypeWriter} */
	let typewriter = null;
	/** @type {{1: Phaser.Key, 2: Phaser.Key, 3: Phaser.Key, 4: Phaser.Key, 5: Phaser.Key	}} */
	let keySwitch = null;

	/** Update the square marker and handles add/removing tiles */
	let updateSquare = () => {
		square.x = Math.floor( game.input.activePointer.worldX / teDe ) * teDe;
		square.y = Math.floor( game.input.activePointer.worldY / teDe ) * teDe;

		if ( typewriter.finished() && game.input.activePointer.position.y > 80 &&
			limit > 0 && game.input.activePointer.leftButton.isDown ) {
			console.log( "Placing Tile at: " + square.x + ", " + square.y );
			for ( let i = 0; i < tiles.length; i++ ) {
				let temp = tiles.getChildAt( i );
				if ( temp.x == square.x && temp.y == square.y ) {
					console.log( "\tDuplicate Child!" );
					return;
				}
			}
			limit--;
			tiles.add( game.add.sprite( square.x, square.y, index ) );
			console.log( "\tTile Placed!" );
		} else if ( game.input.activePointer.rightButton.isDown ) {
			console.log( "Removing Tile at: " + square.x + ", " + square.y );
			for ( let i = 0; i < tiles.length; i++ ) {
				let temp = tiles.getChildAt( i );
				if ( temp.x == square.x && temp.y == square.y ) {
					console.log( "\tChild Found!" );
					limit++;
					tiles.removeChildAt( i );
					break;
				}
			}
			console.log( "\tTile Removed!" );
		}
	}

	/** Using the tile info from shared, create the world */
	let setTile = () => {
		console.log( "Setting Tiles..." );
		for ( let key in shared.tileInfo ) {
			console.log( "\tSetting Tiles Type: " + key +
				" | Total: " + shared.tileInfo[ key ].length );
			shared.tileInfo[ key ].map( ( position ) => {
				tiles.add( game.add.sprite( position[ 0 ], position[ 1 ], key ) );
			} );
			limit -= shared.tileInfo[ key ].length;
		}
		console.log( "Finished Setting Tiles!" );
	}


	/** Get all the tile's position and store it inside shared */
	let getTileLocation = () => {
		console.log( "Creating Dictionary..." );
		/** @type {Object.<string, number[][]>} */
		let data = { "0": [], "1": [], "2": [], "3": [], "4": [] };
		for ( let i = 0; i < tiles.length; i++ ) {
			let temp = tiles.getChildAt( i );
			data[ temp.key ].push( [ temp.x, temp.y ] );
		}
		console.log( "Dictionary Created!" );
		shared.tileInfo = data;
	}

	let initializeWorld = () => {
		console.log( "Initializing World..." );
		level[ shared.levelIndex ].hit = true;
		limit = level[ shared.levelIndex ].limit;
		tiles = game.add.group();
		console.log( "\tSet Limit to: " + limit );
		console.log( "\tDrawing Avoid Poly..." );
		let poly1 = drawPoly( polyGraph, level[ shared.levelIndex ].avoid, 0xFFFFFF );
		console.log( "\tFinish Avoid Poly (Total Points: " + poly1.points.length + ")" );
		console.log( "Finished Initializing World!" );
	}

	return {
		create: () => {
			game.stage.backgroundColor = 0xF2A89F;
			game.input.addMoveCallback( updateSquare, this );

			polyGraph = game.add.graphics();

			typewriter = new TypeWriter( game, "Inconsolata" );
			typewriter.centerText( true, true );
			if ( !level[ shared.levelIndex ].hit ) {
				console.log( "Running Typewriter..." );
				typewriter.startDisplay( level[ shared.levelIndex ].text, 0,
					new Phaser.Point( game.world.centerX, game.world.centerY ), 25,
					"#b9ffb7", "#000000", 2 );
			}
			initializeWorld();
			setTile();
			keySwitch = {
				1: game.input.keyboard.addKey( Phaser.Keyboard.ONE ),
				2: game.input.keyboard.addKey( Phaser.Keyboard.THREE ),
				3: game.input.keyboard.addKey( Phaser.Keyboard.TWO ),
				4: game.input.keyboard.addKey( Phaser.Keyboard.FOUR ),
				5: game.input.keyboard.addKey( Phaser.Keyboard.FIVE )
			};

			// Set up the square marker
			square = game.add.graphics();
			square.lineStyle( 2, 0xFF50EE, 1 );
			square.drawRect( 0, 0, teDe, teDe );

			start = game.add.text( game.world.centerX, 50, "POUR!", {
				fill: "#AAAAAA",
				font: "30px Arial",
				stroke: "#000000",
				strokeThickness: 3
			} );
			start.anchor.setTo( 0.5 );
			start.inputEnabled = true;
			start.events.onInputUp.add( () => {
				if ( typewriter.finished() ) {
					getTileLocation();
					console.log( "Starting Pour..." );
					game.state.start( "Pour" );
				}
			} );

			clear = game.add.text( 25, 25, "Clear Blocks", {
				fill: "#FF50ED",
				font: "20px Arial",
				backgroundColor: "#F0433C",
				stroke: "#000000",
				strokeThickness: 3
			} );
			clear.inputEnabled = true;
			clear.events.onInputUp.add( () => {
				console.log( "Clearing Blocks..." )
				tiles.removeAll( true );
				limit = level[ shared.levelIndex ].limit;
				initializeWorld();
				console.log( "Done Clearing Blocks!" )
			} );

			infoText = game.add.text( game.world.centerX, 85, "", {
				fill: "#FFFFFF",
				font: "20px Arial",
				stroke: "#000000",
				strokeThickness: 3
			} );
			infoText.anchor.setTo( 0.5 );
		},

		render: () => {
			game.debug.text( "Score: " + shared.score, 735, 470, "#000FF" );
		},
		update: () => {
			typewriter.update();
			infoText.setText( "Blocks Left: " + limit );
			if ( keySwitch[ 1 ].isDown ) {
				index = "0";
			} else if ( keySwitch[ 2 ].isDown ) {
				index = "1";
			} else if ( keySwitch[ 3 ].isDown ) {
				index = "2";

			} else if ( keySwitch[ 4 ].isDown ) {
				index = "3";
			} else if ( keySwitch[ 5 ].isDown ) {
				index = "4";
			}
		}
	}
}
