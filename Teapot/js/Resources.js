let teDe = 20;

let Water = ( () => {
	/** @type {Phaser.Game} */
	let game = null;
	/** @type {number} */
	let timer = 0, timerOffset = 0, waterAmount = 0, waterSpeed = 2500;
	/** @type {Phaser.Point} */
	let position = null;
	/** @type {Phaser.Physics.P2.CollisionGroup} */
	let waterGroup = null, tileGroup = null;;

	/** @type {Phaser.Group} */
	let fluid = null;

	class Water {
		/**
		 * @param {Phaser.Game} pGame
		 * @param {number} timeOffset
		 */
		constructor ( pGame, tGroup, wGroup, timeOffset = 75 ) {
			game = pGame;
			timerOffset = timeOffset;
			waterGroup = wGroup;
			tileGroup = tGroup;

			// Create the liquid effect via blurring and filtering
			fluid = game.add.group();
			let blurShader = game.add.filter( "Blur" );
			blurShader.blur = 35;
			fluid.filters = [ blurShader, game.add.filter( "Threshold" ) ];
			fluid.filterArea = game.camera.view;
		}
		/**
		 * Set up the water
		 * @param {{speed: number, amount: number, position: Phaser.Point}} info
		 */
		setWater ( info ) {
			waterSpeed = info.speed;
			waterAmount = info.amount;
			position = info.position;
			timer = 0;
		}
		/** Remove all water */
		clearWater () {
			waterAmount = 0;
			fluid.removeAll( true );
		}

		/**
		 * Create the water if set
		 * @param {function(boolean)} failed
		*/
		update ( failed ) {
			if ( waterAmount > 0 && timer < game.time.now ) {
				let droplet = game.add.sprite( position.x, position.y, 'droplet' );

				// Enable physics for the droplet
				game.physics.p2.enable( droplet );
				droplet.body.setCircle( droplet.width * 0.3 );
				droplet.body.setCollisionGroup( waterGroup );
				droplet.body.collides( [ tileGroup, waterGroup ] );

				// Add a force that slows down the droplet over time
				droplet.body.damping = 0.3;

				// Make the collision smaller to make it look liquid like
				droplet.body.collideWorldBounds = true;
				droplet.checkWorldBounds = true;
				droplet.events.onOutOfBounds.add( failed, this, 0, true );

				// Add the droplet to the fluid group
				fluid.add( droplet );
				droplet.body.moveRight( waterSpeed );

				timer = game.time.now + timerOffset;
				waterAmount--;
			}
		}

		setCollision ( value, avoidZone ) {
			fluid.forEach( ( ele ) => {
				ele.body.collides( avoidZone );
			}, this );
		}

		get collisionGroup () { return waterGroup; }

		get finishPouring () { return waterAmount <= 0; }
		get waterLeft () { return waterAmount; }
	}
	return Water;
} )();

let drawPoly = ( graphic, points, color ) => {
	let poly = new Phaser.Polygon( points );
	graphic.beginFill( color, 0.7 );
	graphic.drawPolygon( poly.points );
	graphic.endFill();
	return poly;
}

let color = "#FF50EA";
let maxLevel = 8;
/** @type {Object.<number, {position: Phaser.Point, amount:number, speed: number, gravity: number, score: number, limit: number, hit: boolean, text: any[], avoid: number[], fill: number[]}>} */
let level = {
	1: { // Basic fill up DONE
		position: new Phaser.Point( 810, 30 ), amount: 75, speed: -500, gravity: 500, score: 1,
		limit: 65, hit: false, text: [
			[ "First Level! Don't let any tea go out of bound!", true, color ],
			[ "For now, don't worry about zones.", true, color ],
			[ "Left Click to put blocks to trap the tea.", false, color ],
			[ "Right Click to remove a block.", false, color ],
			[ "You only have limited numbers of blocks!", false, color ],
			[ "Using less blocks will net more points.", false, color ],
			[ "Click POUR! to see where the tea comes from.", true, color ],
		], avoid: [ 0, 0, 1, 1, 1, 0 ]
	},
	2: { // Basic fill up into a cup
		position: new Phaser.Point( 810, 30 ), amount: 75, speed: -400, gravity: 500, score: 1,
		limit: 65, hit: false, text: [
			[ "Good job! Now the same thing except make sure\n\
			when the tea calmed down, no tea are in the white zone.", true, color ],
		],
		avoid: [ 0, 0, 850, 0, 850, 500, 580, 500, 580, 275, 400, 275, 400, 500, 0, 500 ],
	},
	3: { // Shoot fast
		position: new Phaser.Point( 810, 30 ), amount: 50, speed: -1000, gravity: 500, score: 2,
		limit: 40, hit: false, text: [
			[ "Let test your physic.", true, color ],
			[ "There are different shapes. Try them out.", true, color ],
			[ "They are numbered 1 - 5. Your current shape is number 1.", true, color ]
		],
		avoid: [ 0, 0, 850, 0, 850, 500, 300, 500, 300, 220, 135, 220, 135, 400, 300, 400, 300, 500, 0, 500 ],
	},
	4: { // Shoot Slow, Row to hold
		position: new Phaser.Point( 810, 30 ), amount: 50, speed: -250, gravity: 1000, score: 2,
		limit: 75, hit: false, text: [
			[ "Pretty simple don't you agree?", true, color ]
		],
		avoid: [ 0, 0, 850, 0, 850, 500, 460, 500, 460, 415, 35, 415, 35, 500, 0, 500 ],
	},
	5: { // Some weird physic DONE
		position: new Phaser.Point( 810, 30 ), amount: 50, speed: -1250, gravity: 1000, score: 3,
		limit: 75, hit: false, text: [
			[ "Nothing new, just contain the tea, avoid white.", true, color ]
		],
		avoid: [ 0, 0, 0, 500, 315, 500, 315, 300, 450, 300, 450, 500, 850, 500, 850, 0 ],
	},
	6: {  // Filling
		position: new Phaser.Point( 810, 30 ), amount: 50, speed: -750, gravity: 750, score: 2,
		limit: 60, hit: false, text: [
			[ "Fill her up", true, color ]
		],
		avoid: [ 0, 0, 850, 0, 850, 500, 470, 500, 470, 285, 450, 215, 100, 390, 120, 460, 470, 285, 470, 500, 0, 500 ],
	},
	7: {  // Filling
		position: new Phaser.Point( 810, 30 ), amount: 50, speed: -850, gravity: 750, score: 2,
		limit: 60, hit: false, text: [
			[ "Fill her up V2", true, color ]
		],
		avoid: [ 0, 0, 850, 0, 850, 500, 650, 500, 650, 440, 670, 350, 390, 240, 370, 330, 650, 440, 650, 500, 0, 500 ],
	},
	8: {  // Some dot physic
		position: new Phaser.Point( 810, 30 ), amount: 5, speed: -1250, gravity: 1500, score: 10,
		limit: 15, hit: false, text: [
			[ "Prefect Dot :|", true, color ]
		],
		avoid: [ 0, 0, 0, 500, 120, 500, 120, 420, 230, 420, 230, 500, 850, 500, 850, 0 ],
	},
}
