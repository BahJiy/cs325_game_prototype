"use strict"

/**
 * @param {Phaser.Game} game
 * @param {{levelIndex: number, tileInfo: Object.<number, number[][]>}} shared
 */
GameStates.Pour = function ( game, shared ) {

	let debug = true;

	/** @type {Water} */
	let water = null;
	/** @type {Phaser.Text} */
	let infoText = null;
	/** @type {Phaser.Timer} */
	let timer = null;
	/** @type {Phaser.Graphics} */
	let polyGraph = null;
	/** @type {Phaser.Physics.P2.CollisionGroup} */
	let zoneGroup = null;
	/** @type {Phaser.Physics.P2.CollisionGroup} */
	let tileGroup = null;
	/** @type {Phaser.Physics.P2.CollisionGroup} */
	let waterGroup = null;
	/** @type {Phaser.Group} */
	let tiles = null;
	/** @type {Phaser.Sprite} */
	let whiteZone = null;

	/** Using the tile info from shared, create the world */
	let initializeWorld = () => {
		console.log( "Setting Tiles..." );
		tileGroup = game.physics.p2.createCollisionGroup();
		waterGroup = game.physics.p2.createCollisionGroup();
		tiles = game.add.group();
		for ( let key in shared.tileInfo ) {
			console.log( "\tSetting Tiles Type: " + key +
				" | Total: " + shared.tileInfo[ key ].length );
			shared.tileInfo[ key ].map( ( position ) => {
				let tile = game.add.sprite( position[ 0 ] + teDe / 2, position[ 1 ] + teDe / 2, key );
				console.log( "\t\t- Type: " + key + " | Position: " + position[ 0 ] + ", " + position[ 1 ] );
				console.log( "\t\t- Finial Position: " + tile.x + ", " + tile.y );
				game.physics.p2.enable( tile, false );
				switch ( key ) {
					case "0": // Block
						tile.body.addPolygon( { optimalDecomp: true, skipSimpleCheck: true, removeCollinearPoints: true },
							[ 0 - teDe / 2, 0 - teDe / 2, 0 - teDe / 2, teDe - teDe / 2, teDe - teDe / 2, teDe - teDe / 2, teDe - teDe / 2, 0 - teDe / 2 ]
						);
						break;
					case "1": // Left
						tile.body.addPolygon( { optimalDecomp: true, skipSimpleCheck: true, removeCollinearPoints: true },
							[ -2 - teDe / 2, -2 - teDe / 2, -2 - teDe / 2, 2 + teDe - teDe / 2, 2 + teDe - teDe / 2, 2 + teDe - teDe / 2 ]
						);
						break;
					case "2": // Right
						tile.body.addPolygon( { optimalDecomp: true, skipSimpleCheck: true, removeCollinearPoints: true },
							[ 2 + teDe - teDe / 2, -2 - teDe / 2, 2 + teDe - teDe / 2, 2 + teDe - teDe / 2, -2 - teDe / 2, 2 + teDe - teDe / 2 ]
						);
						break;
					case "3": // Up Left
						tile.body.addPolygon( { optimalDecomp: true, skipSimpleCheck: true, removeCollinearPoints: true },
							[ -2 - teDe / 2, -2 - teDe / 2, -2 - teDe / 2, 2 + teDe - teDe / 2, 2 + teDe - teDe / 2, -2 - teDe / 2 ]
						);
						break;
					case "4": // Up Right
						tile.body.addPolygon( { optimalDecomp: true, skipSimpleCheck: true, removeCollinearPoints: true },
							[ -2 - teDe / 2, -2 - teDe / 2, 2 + teDe - teDe / 2, -2 - teDe / 2, 2 + teDe - teDe / 2, 2 + teDe - teDe / 2 ]
						);
						break;
				}
				tile.body.static = true;
				tile.body.setCollisionGroup( tileGroup );
				tile.body.collides( waterGroup );
				tiles.add( tile );
			} )
		}
		console.log( "Finished Setting Tiles!" );
		console.log( "Drawing Avoid Poly..." );
		let poly1 = drawPoly( polyGraph, level[ shared.levelIndex ].avoid, 0xFFFFFF );
		console.log( "\tFinish Avoid Poly (Total Points: " + poly1.points.length + ")" );
	}

	let failed = ( ground = false ) => {
		game.physics.p2.pause();
		if ( !ground ) {
			infoText.setText( "There water in the white zone!\n" +
				"Go back to Creator and try again!" );
		} else {
			infoText.setText( "Water went out of bound!\n" +
				"Go back to Creator and try again!" );
		}
	}

	return {
		create: function () {
			game.stage.backgroundColor = 0x2E602C;
			game.physics.startSystem( Phaser.Physics.P2JS );
			// Add bounciness and gravity
			game.physics.p2.restitution = 0.3;
			game.physics.p2.gravity.y = level[ shared.levelIndex ].gravity;

			polyGraph = game.add.graphics();

			initializeWorld(); // Create the blocks the player placed
			game.physics.p2.setBounds( 0, 0, game.world.width, game.world.height, false, false, false, false );

			timer = game.time.create( true );
			// Create the white zone checker
			zoneGroup = game.physics.p2.createCollisionGroup();
			whiteZone = game.add.sprite( 0, 0 );
			game.physics.p2.enable( whiteZone, false );
			whiteZone.body.motionState = Phaser.Physics.P2.Body.STATIC;
			whiteZone.body.addPolygon( { optimalDecomp: false, skipSimpleCheck: true, removeCollinearPoints: false },
				level[ shared.levelIndex ].avoid );
			whiteZone.body.setCollisionGroup( zoneGroup );

			// Create the water
			water = new Water( game, tileGroup, waterGroup );
			water.setWater( level[ shared.levelIndex ] );
			let tea = game.add.image( level[ shared.levelIndex ].position.x + 50, level[ shared.levelIndex ].position.y - 30, "tea" );
			tea.anchor.setTo( 0.5 );

			infoText = game.add.text( game.world.centerX, 80, "", {
				fill: "#000000",
				font: "20px Arial",
				stroke: "#FFFFFF",
				strokeThickness: 3
			} );
			infoText.anchor.setTo( 0.5, 0 );
			infoText.inputEnabled = false;
			infoText.events.onInputUp.add( () => {
				water.clearWater();
				shared.score += ( level[ shared.levelIndex ].limit - tiles.length ) * level[ shared.levelIndex ].score;
				shared.tileInfo = {};
				shared.levelIndex++;
				if ( shared.levelIndex > maxLevel ) {
					game.state.start( "MainMenu" );
				} else {
					game.state.start( "Creator" );
				}
			} );
			let back = game.add.text( game.world.centerX, 50, "Back to Creator", {
				fill: "#000000",
				font: "30px Arial",
				stroke: "#FFFFFF",
				strokeThickness: 3,
				align: "center"
			} );
			back.anchor.setTo( 0.5 );
			back.inputEnabled = true;
			back.events.onInputUp.add( () => {
				water.clearWater();
				game.state.start( "Creator" );
			} );

			game.physics.p2.paused = false;
		},

		render: function () {
			//game.debug.text( "Mouse: " + game.input.activePointer.position, 25, 25 );
			game.debug.text( "Score: " + shared.score, 735, 470, "#000FF" );
		},

		update: function () {
			if ( timer != null && !game.physics.p2.paused ) {
				if ( !timer.running ) {
					infoText.setText( "Water Left: " + water.waterLeft );
				} else if ( timer.running ) {
					infoText.setText( "Wait: " + Math.round( timer.duration / 1000 ) );
				}
				water.update( failed );

				if ( water.finishPouring ) {
					timer.add( 4000, () => {
						// There's no zone in level 1
						// Create the physic object to check collision with water
						game.physics.p2.gravity.y = 0;
						water.setCollision( true, zoneGroup );
						whiteZone.body.collides( water.collisionGroup );
						whiteZone.body.onBeginContact.add( () => {
							failed();
						} );
						let nextTimer = game.time.create( true );
						nextTimer.add( 500, () => {
							if ( !game.physics.p2.paused ) {
								game.physics.p2.pause();
								infoText.setText( "Next Level" );
								infoText.inputEnabled = true;
							}
						} )
						nextTimer.start();
						timer = null;
					} );
					timer.start();
				}
			}
		}
	};
}
