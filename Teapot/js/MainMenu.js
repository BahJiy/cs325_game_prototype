"use strict";

/** @param {Phaser.Game} game */
GameStates.makeMainMenu = function ( game, shared ) {
    let title = null, teapot = null, instruct;
    let start = null, instruction = null;

    return {
        create: function () {
            game.stage.backgroundColor = 0xF2389C;
            if ( shared.score == 0 ) {
                title = game.add.text( game.world.centerX, game.world.centerY - 150, "Pouring Tea", {
                    font: "35px Gloria Hallelujah",
                    stroke: "#FFFFFF",
                    strokeThickness: 2
                } );
                title.anchor.setTo( 0.5 );
                teapot = game.add.image( game.world.centerX, game.world.centerY + 75, "teapot" );
                teapot.anchor.setTo( 0.5 );
                instruct = game.add.image( 0, 0, "instruction" );
                instruct.visible = false;

                start = game.add.text( game.world.centerX - 75, game.world.centerY - 85, "Start", {
                    font: "25px Gloria Hallelujah",
                    stroke: "#FFFFFF",
                    strokeThickness: 2

                } );
                instruction = game.add.text( game.world.centerX + 85, game.world.centerY - 85, "Instruction", {
                    font: "25px Gloria Hallelujah",
                    stroke: "#FFFFFF",
                    strokeThickness: 2

                } );
                start.anchor.setTo( 0.5 );
                start.inputEnabled = true;
                start.events.onInputUp.add( () => {
                    game.state.start( "Creator" );
                } )
                instruction.anchor.setTo( 0.5 );
                instruction.inputEnabled = true;
                instruction.events.onInputUp.add( () => {
                    instruct.visible = true;
                    instruct.inputEnabled = true;
                    title.visible = false;
                    start.visible = false;
                    start.inputEnabled = false;
                    instruction.visible = false;
                    instruction.inputEnabled = false;
                } )

                instruct.events.onInputUp.add( () => {
                    instruct.visible = false;
                    instruct.inputEnabled = false;
                    title.visible = true;
                    start.visible = true;
                    start.inputEnabled = true;
                    instruction.visible = true;
                    instruction.inputEnabled = true;
                } )
            } else {
                let t1 = game.add.text( game.world.centerX, game.world.centerY, "Congrats! You got " + shared.score + " Points.\n\
Now can you get a higher score?\nClick anywhere to start again." );
                t1.anchor.setTo( 0.5 );
            }
        },

        render: function () {
            //game.debug.text( "Pointer x: " + game.input.position.x + " y: " + game.input.position.y, 25, 25 );
        },

        update: function () {
            if ( shared.score != 0 && game.input.activePointer.leftButton.isDown ) {
                shared.score = 0;
                shared.levelIndex = 1
                shared.tileInfo = null;
                game.state.start( "MainMenu" );
            }
        }
    };
};
