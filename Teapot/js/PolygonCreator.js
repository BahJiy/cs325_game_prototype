"use strict"

/**
 * This file is not part of the game. It helps create the poly that the player have
 * to fill/avoid
 * @param {Phaser.Game} game
 * @param {{levelIndex: number, tileInfo: Object.<number, number[][]>}} shared
 */
GameStates.Poly = function ( game, shared ) {
	/** @type {Phaser.Polygon} */
	let poly1 = null;
	/** @type {Phaser.Polygon} */
	let poly2 = null;
	/** @type {Phaser.Polygon} */
	let poly = null;
	/** @type {Phaser.Graphics} */
	let graph = null;
	/** @type {Phaser.Key} */
	let erase = null;
	/** @type {InputControl} */
	let inControl = null;

	function beautify () {

	}
	return {
		create: () => {
			game.stage.backgroundColor = 0x3322AABB;
			poly1 = new Phaser.Polygon( 50, 50, 100, 50, 100, 100, 50, 100 );
			poly2 = new Phaser.Polygon( 150, 150, 200, 150, 200, 200, 150, 200 );
			graph = game.add.graphics( 0, 0 );

			poly = poly1;
			inControl = new InputControl( game, Phaser.Keyboard.Q, true, true );
		},
		update: () => {
			inControl.update();
			if ( inControl.isDown( Phaser.Keyboard.Q ) ) {
				poly = poly == poly1 ? poly2 : poly1;
			}
			if ( inControl.isDown( Phaser.Pointer.LEFT_BUTTON ) ) {
				let temp = poly.toNumberArray();
				poly.setTo( temp.concat( [ game.input.activePointer.x, game.input.activePointer.y ] ) );
			} else if ( inControl.isDown( Phaser.Pointer.RIGHT_BUTTON ) ) {
				let temp = poly.toNumberArray();
				temp.pop();
				temp.pop();
				poly.setTo( temp );
			}
			graph.clear();
			graph.beginFill( 0xFFFFFF );
			graph.drawPolygon( poly1.points );
			graph.endFill();
			graph.beginFill( 0xA32CA9 );
			graph.drawPolygon( poly2.points );
			graph.endFill();
		}
	};
}
