// @ts-nocheck
"use strict";
let game = new Phaser.Game( 850, 500, Phaser.WEBGL, 'game' );
window.onload = function () {

	//	Create your Phaser game and inject it into the 'game' div.
	//	We did it in a window.onload event, but you can do it anywhere (requireJS load, anonymous function, jQuery dom ready, - whatever floats your boat)
	/** @type {Phaser.Game} */

	let WebFontConfig = {
		active: function () { game.time.events.add( Phaser.Timer.SECOND, null, this ); },
		google: {
			families: [ 'Inconsolata', 'Gloria Hallelujah' ]
		}
	};

	/**
	 * @type {{score: number,levelIndex:number,tileInfo: Object.<number, number[][]>}}
	 */
	let shared = { score: 0, levelIndex: 1, tileInfo: null };

	// Add the states
	game.state.add( 'Boot', GameStates.makeBoot( game ) );
	game.state.add( 'MainMenu', GameStates.makeMainMenu( game, shared ) );
	game.state.add( 'Creator', GameStates.Creator( game, shared ) );
	game.state.add( 'Pour', GameStates.Pour( game, shared ) );
	game.state.add( 'Poly', GameStates.Poly( game, shared ) );
	game.state.add( 'EndGame', GameStates.EndGame( game, shared ) );

	game.state.start( 'Boot' );

};
window.onbeforeunload = function ( e ) {
	game.world.removeAll();
	game.destroy();
};

/**
 * OpenGL Required for liquid. This code is copied. See game info
 * for source
 */
Phaser.Filter.Threshold = function ( game ) {
	Phaser.Filter.call( this, game );

	this.fragmentSrc = [
		"precision mediump float;",
		"varying vec2 vTextureCoord;",
		"varying vec4 vColor;",
		"uniform sampler2D uSampler;",

		"void main(void) {",
		"vec4 color = texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y));",
		"float thresh = step(0.3, color.a);",
		"vec4 sum = vec4(thresh * 0.7, thresh * 0.9, thresh, thresh);",
		"gl_FragColor = sum;",

		"}"
	];

};

Phaser.Filter.Threshold.prototype = Object.create( Phaser.Filter.prototype );
Phaser.Filter.Threshold.prototype.constructor = Phaser.Filter.Threshold;

Phaser.Filter.BlurX = function ( game ) {
	Phaser.Filter.call( this, game );

	this.uniforms.blur = { type: '1f', value: 1 / 512 };

	this.fragmentSrc = [
		"precision mediump float;",
		"varying vec2 vTextureCoord;",
		"varying vec4 vColor;",
		"uniform float blur;",
		"uniform sampler2D uSampler;",

		"void main(void) {",
		"vec4 sum = vec4(0.0);",

		"sum += texture2D(uSampler, vec2(vTextureCoord.x - 4.0*blur, vTextureCoord.y)) * 0.05;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x - 3.0*blur, vTextureCoord.y)) * 0.09;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x - 2.0*blur, vTextureCoord.y)) * 0.12;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x - blur, vTextureCoord.y)) * 0.15;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y)) * 0.16;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x + blur, vTextureCoord.y)) * 0.15;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x + 2.0*blur, vTextureCoord.y)) * 0.12;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x + 3.0*blur, vTextureCoord.y)) * 0.09;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x + 4.0*blur, vTextureCoord.y)) * 0.05;",

		"gl_FragColor = sum;",

		"}"
	];

};

Phaser.Filter.BlurX.prototype = Object.create( Phaser.Filter.prototype );
Phaser.Filter.BlurX.prototype.constructor = Phaser.Filter.BlurX;

Object.defineProperty( Phaser.Filter.BlurX.prototype, 'blur', {

	get: function () {
		return this.uniforms.blur.value / ( 1 / 7000 );
	},

	set: function ( value ) {
		this.uniforms.blur.value = ( 1 / 7000 ) * value;
	}

} );

Phaser.Filter.BlurY = function ( game ) {
	Phaser.Filter.call( this, game );

	this.uniforms.blur = { type: '1f', value: 1 / 512 };

	this.fragmentSrc = [
		"precision mediump float;",
		"varying vec2 vTextureCoord;",
		"varying vec4 vColor;",
		"uniform float blur;",
		"uniform sampler2D uSampler;",

		"void main(void) {",
		"vec4 sum = vec4(0.0);",

		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y - 4.0*blur)) * 0.05;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y - 3.0*blur)) * 0.09;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y - 2.0*blur)) * 0.12;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y - blur)) * 0.15;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y)) * 0.16;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y + blur)) * 0.15;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y + 2.0*blur)) * 0.12;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y + 3.0*blur)) * 0.09;",
		"sum += texture2D(uSampler, vec2(vTextureCoord.x, vTextureCoord.y + 4.0*blur)) * 0.05;",

		"gl_FragColor = sum;",

		"}"
	];

};

Phaser.Filter.BlurY.prototype = Object.create( Phaser.Filter.prototype );
Phaser.Filter.BlurY.prototype.constructor = Phaser.Filter.BlurY;

Object.defineProperty( Phaser.Filter.BlurY.prototype, 'blur', {

	get: function () {
		return this.uniforms.blur.value / ( 1 / 7000 );
	},

	set: function ( value ) {
		this.uniforms.blur.value = ( 1 / 7000 ) * value;
	}

} );

Phaser.Filter.Blur = function ( game ) {
	this.blurXFilter = new Phaser.Filter.BlurX();
	this.blurYFilter = new Phaser.Filter.BlurY();

	this.passes = [ this.blurXFilter, this.blurYFilter ];
};

Phaser.Filter.Blur.prototype = Object.create( Phaser.Filter.prototype );
Phaser.Filter.Blur.prototype.constructor = Phaser.Filter.Blur;

Object.defineProperty( Phaser.Filter.Blur.prototype, 'blur', {
	get: function () {
		return this.blurXFilter.blur;
	},
	set: function ( value ) {
		this.blurXFilter.blur = this.blurYFilter.blur = value;
	}
} );

Object.defineProperty( Phaser.Filter.Blur.prototype, 'padding', {
	get: function () {
		return this.blurXFilter.padding;
	},
	set: function ( value ) {
		this.blurXFilter.padding = this.blurYFilter.padding = value;
	}
} );
