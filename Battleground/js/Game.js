/** @param {Phaser.Game} game */
GameStates.makeGame = function ( game, shared ) {
	let STATE = Object.freeze( { START: 0, DRAW: 1, BATTLE: 2, AFTERMATH: 3, WAIT: -1 } );
	let background = null;

	/** @type {Cards[]} */
	let tieDeck = [];
	/** @type {Phaser.Key[]} */
	let keys = [];
	/** @type {number} */
	let state = STATE.WAIT;
	/** @type {number} */
	let tempState = STATE.WAIT;
	/** @type {number} */
	let countDown = 0;
	/** @type {string} */
	let afterText = "";

	/** @type {Player} */
	let player1 = null;
	/** @type {boolean} */
	let p1Hit = false;
	/** @type {Player} */
	let player2 = null;
	/** @type {boolean} */
	let p2Hit = false;

	/** @type {Phaser.Text} */
	let infoText = null;
	/** @type {Phaser.Key} */
	let infoEnter = null;
	/** @type {Phaser.Group} */
	let infoGroup = null;
	/** @type {{name:Phaser.Text, deckCount: Phaser.Text, vicCount: Phaser.Text, score: Phaser.Text}} */
	let p1Text = { name: null, deckCount: null, vicCount: null, score: null };
	/** @type {{name:Phaser.Text, deckCount: Phaser.Text, vicCount: Phaser.Text, score: Phaser.Text}} */
	let p2Text = { name: null, deckCount: null, vicCount: null, score: null };
	/** @type {Phaser.Group} */
	let textGroup = null;
	/** @type {number} */
	let attachCount = 0;
	/** @type {number} */
	let timer = Infinity;
	/** @type {boolean} */
	let ready = false;
	/** @type {function} */
	let act = null;

	function changeBackgroundFade () {
		let temp = randBackground( game, 0 );
		game.add.tween( temp ).to( { alpha: 1 }, 750, Phaser.Easing.Default, true );
		game.add.tween( background ).to( { alpha: 0 }, 750, Phaser.Easing.Default, true ).onComplete.add( () => {
			background = temp;
		} );
	}

	function ActAttachment () {
		console.log( "\tAttachment Phase -> p1: " + p1Hit + " | p2: " + p2Hit );
		player1.putAttachment( !p1Hit );
		player2.putAttachment( !p2Hit );
		p1Hit = false;
		p2Hit = false;
		// Return true if either one is still allowed to put an attachment
		return player1.getAllowAttachment() || player2.getAllowAttachment();
	}
	function ActPotion () {
		console.log( "\tPotion Phase -> p1: " + p1Hit + " | p2: " + p2Hit );
		player1.usePotion( !p1Hit );
		player2.usePotion( !p2Hit );
		p1Hit = false;
		p2Hit = false;
		return false;
	}
	function ActAftermath () {
		let p1Value = player1.characterValue( player2.getFaceCard() );
		let p2Value = player2.characterValue( player1.getFaceCard() );
		let text = ""
		let receiver = null;
		let giver = null;

		if ( p1Value > p2Value ) {
			receiver = player1;
			giver = player2;
			text = "Player 1 Won with " + p1Value + " Points\nagainst Player 2's " + p2Value + " Points!";
		} else if ( p2Value > p1Value ) {
			receiver = player2;
			giver = player1;
			text = "Player 2 Won with " + p2Value + " Points\nagainst Player 1's " + p1Value + " Points!";
		} else {
			tieDeck.concat( player1.getAllCards() );
			tieDeck.concat( player2.getAllCards() );
			text = "Both player tied.\nWinner of next rounds gets it all!";
		}

		// Who won?
		if ( player1.getDeckCount() == 0 && player1.getVicDeckCount() == 0 ) {
			infoText.setText( "Player 2 won the game!" );
		} else if ( player2.getDeckCount() == 0 && player2.getVicDeckCount() == 0 ) {
			infoText.setText( "Player 1 won the game!" );
		} else {
			// No one
			countDown = 0;
			timer = Infinity;
			attachCount = 0;
			state = STATE.WAIT
			infoText.setText( text + "\nReady for Attachment Phase?" );
			infoEnter.enabled = true;
			infoEnter.onDown.addOnce( () => {
				if ( receiver != null ) {
					receiver.giveCards( giver.getAllCards() );
				}
			} );
		}
		//infoText.events.onInputUp.addOnce( () => {
		//	if ( receiver != null ) {
		//		receiver.giveCards( giver.getAllCards() );
		//	}
		//} );
		changeBackgroundFade();
	}

	/**
	 * Work the countdown timer
	 */
	function CountDown () {
		// If we just started the countdown
		if ( countDown == 0 || game.time.now > timer + 1000 * countDown ) {
			countDown++
			infoText.fontSize = 40;
			infoText.alpha = 1;
			switch ( countDown ) {
				case 1: // Initialize the timer
					timer = game.time.now;
				case 2: // Show the countdown text
					infoText.setText( "COUNTDOWN: " + countDown );
					game.add.tween( infoText ).to( { fontSize: 1, alpha: 0 }, 800, Phaser.Easing.Default, true );
					break;
				case 3:
					infoText.setText( "Press Now!" );
					return true;
			}
		}
		return false;
	}

	return {
		create: () => {
			background = randBackground( game );

			console.log( "Initializing Deck" );
			/** @type {Cards[]} */
			let deck = [];
			[ "spades", "hearts", "clubs", "diamonds" ].forEach( suit => {
				for ( let value = 1; value <= 13; value++ ) {
					deck.push( new Cards( game, value, suit ) );
				}
			} );
			deck = shuffleList( deck );
			console.log( "Initializing Players" );
			player1 = new Player( deck.slice( 0, deck.length / 2 ), 1 );
			player2 = new Player( deck.slice( deck.length / 2, deck.length ), 2 );


			infoEnter = game.input.keyboard.addKey( Phaser.KeyCode.ENTER );
			infoText = game.add.text( game.world.centerX, game.world.centerY,
				"Press Enter to start \nAttachment Phase", {
					font: "40px Inconsolata",
					fill: "#28f223",
					stroke: "#FF0000",
					strokeThickness: 3,
					align: "center"
				}
			);
			infoEnter.onDown.add(
				() => { state += 1; ready = true; infoEnter.enabled = false }
			);
			let PlayStyle = { font: " 20px Inconsolata", fill: "#FFFFFF", stroke: "#000000", strokeThickness: 2 };
			let NumberStyle = { font: " 30px Inconsolata", fill: "#FF2029", stroke: "#000000", strokeThickness: 2 };
			textGroup = game.add.group();
			p1Text = {
				name: game.add.text( 200, 365, "Player 1 | Key : " + shared.key1[1], PlayStyle ),
				deckCount: game.add.text( 45, 300, "22", NumberStyle, textGroup ),
				vicCount: game.add.text( 45, 200, "0", NumberStyle, textGroup ),
				score: game.add.text( 300, 420, "Score: ", PlayStyle, textGroup ),
			}
			p2Text = {
				name: game.add.text( 800, 235, "Player 2 | Key : " + shared.key2[1], PlayStyle ),
				deckCount: game.add.text( 955, 300, "22", NumberStyle, textGroup ),
				vicCount: game.add.text( 955, 400, "0", NumberStyle, textGroup ),
				score: game.add.text( 700, 180, "Score: ", PlayStyle, textGroup ),
			}
			textGroup.visible = false;

			// TODO This should be shared.keySetting
			keys.push( game.input.keyboard.addKey( shared.key1[ 0 ] ) );
			keys.push( game.input.keyboard.addKey( shared.key2[ 0 ] ) );
			keys[ 0 ].enabled = keys[ 1 ].enabled = false;
			keys[ 0 ].onDown.add( () => { p1Hit = true } );
			keys[ 1 ].onDown.add( () => { p2Hit = true } );

			//player1.DEBUG();
			//player2.DEBUG();
		},

		render: () => {
			//if ( state >= STATE.START ) {
			//	player1.debugText( 25, 25, player2 );
			//	player2.debugText( 500, 25, player1 );
			//}
			//game.debug.text( "Mouse: " + game.input.activePointer.position,
			//	game.world.centerX, 525, "#FFFFFF" )
			//game.debug.text( "State: " + state, 25, 550, "#FFFFFF" )
			//game.debug.text( "Attachment Count: " + attachCount, 25, 575, "#FFFFFF" )
		},

		update: () => {
			game.world.bringToTop( textGroup );
			game.world.sendToBack( background );
			if ( ready ) {
				switch ( state ) {
					case STATE.START:
						player1.start();
						player2.start();
						state = STATE.DRAW;
					case STATE.DRAW:
						if ( ( player1.getAllowAttachment() || player2.getAllowAttachment() ) &&
							attachCount < 5 ) {
							if ( CountDown() ) {
								keys[ 0 ].enabled = keys[ 1 ].enabled = true;
								tempState = state;
								state = STATE.WAIT;

								act = ActAttachment;
								afterText = "Ready for Potion?"
								countDown = 0;
								attachCount++;
								timer = game.time.now + 1250;
							}
						} else {
							timer = 0;
							afterText = "No players have potion cards!\nEnter to continue"
							tempState = state;
							state = STATE.WAIT;
							act = () => { return false; };
						}
						break;
					case STATE.BATTLE:
						if ( ( player1.getPotionAllowed() || player2.getPotionAllowed() ) ) {
							if ( CountDown() ) {
								keys[ 0 ].enabled = keys[ 1 ].enabled = true;
								tempState = state;
								state = STATE.WAIT;

								act = ActPotion;
								afterText = "Press Enter to see who won!";
								timer = game.time.now + 1250;
							}
						} else {
							timer = 0;
							tempState = state;
							afterText = "Press Enter to see who won!";
							state = STATE.WAIT;
							act = () => { return false; };
						}
						break;
					case STATE.AFTERMATH:
						ActAftermath();
						break;
					case STATE.WAIT:
						if ( game.time.now > timer ) {
							keys[ 0 ].enabled = keys[ 1 ].enabled = false;
							state = tempState;
							timer = Infinity;
							// If act returns false, we cannot do anymore in
							// this state so go to the next state;
							if ( !act() ) {
								ready = false;
								infoText.setText( afterText );
								infoEnter.enabled = true;
							}
						}
						break;
					default:
				}
				textGroup.visible = shared.infoText;
				p1Text.deckCount.setText( "" + player1.getDeckCount() );
				p1Text.vicCount.setText( "" + player1.getVicDeckCount() );
				p1Text.score.setText( "Score: " + player1.characterValue( player2.getFaceCard() ) );

				p2Text.deckCount.setText( "" + player2.getDeckCount() );
				p2Text.vicCount.setText( "" + player2.getVicDeckCount() );
				p2Text.score.setText( "Score: " + player2.characterValue( player1.getFaceCard() ) );
			}
		}
	}


}
