/** @param {Cards[]} deck */
function p1DeckPlace ( deck, victorDeck = false, potionDeck = false ) {
	let x = 50, y = 300, offset = 0;
	y -= victorDeck ? 100 : 0;
	x += potionDeck ? 75 : 0;
	console.log( "Player 1:" );
	for ( let i = 1; i <= deck.length; i++ ) {
		//console.log( "\tCard " + deck[ i ].toString() + " at " + ( x - offset ) );
		deck[ i - 1 ].placeCard( x - offset, y );
		offset = potionDeck ? -i * 10 : Math.floor( i / 2 );
	}
}
/** @param {Cards[]} deck */
function p2DeckPlace ( deck, victorDeck = false, potionDeck = false ) {
	let x = 950, y = 300, offset = 0;
	y += victorDeck ? 100 : 0;
	x -= potionDeck ? 75 : 0;
	console.log( "\nPlayer 2:" );
	for ( let i = 1; i <= deck.length; i++ ) {
		//console.log( "\tCard " + deck[ i ].toString() + " at " + ( x + offset ) );
		deck[ i - 1 ].placeCard( x + offset, y );
		offset = potionDeck ? -i * 10 : Math.floor( i / 2 );
	}
}

function Player ( initDeck, playerNum ) {
	/** @type {[number, number, number][]} */
	let attachPos = [];
	let cPosX = 0, cPosY = 0;
	let pOffset = 0;

	/** @type {Array<Cards>} */
	let deck = [];
	/** @type {Array<Cards>} */
	let victoryPile = shuffleList( initDeck );

	this.playerNum = playerNum;

	/** @type {boolean} */
	let disableAttach = false;
	/** @type {boolean} */
	let disablePotion = false;
	/** @type {Cards} */
	let character = null;
	/** @type {Array<Cards>} */
	let attachments = [];
	/** @type {Array<Cards>} */
	let potions = [];
	/** @type {Cards} */
	let potionCard = null;

	this.DEBUG = ( deck2 = true ) => {
		this.start();
		while ( attachments.length < 5 ) {
			this.putAttachment();
		}
		this.usePotion();
		if ( deck2 ) {
			this.giveCards( [ deck.pop() ] );
			this.DEBUG( false );
		}
	}
	let compareFaceCard = ( faceCard, ourFaceCard ) => {
		return ourFaceCard != null && ( faceCard == null ||
			faceCard.cardValue() < ourFaceCard.cardValue() ) ?
			ourFaceCard : faceCard;
	}
	let swapDeck = () => {
		// We shuffle the victoryPile to the deck
		deck = shuffleList( victoryPile );
		victoryPile = [];

		while ( potions.length < 3 && deck.length > 5 ) {
			potions.push( deck.pop() );
		}

		//let game = new Phaser.Game( 1000, 600, Phaser.WEBGL, 'game' );
		if ( playerNum == 1 ) {
			p1DeckPlace( deck );
			p1DeckPlace( potions, false, true );
			cPosX = 180;
			cPosY = 420;
			pOffset = 35;
			attachPos = [ [ 77, 440, 90 ], [ 84, 473, 67 ], [ 106, 505, 45 ], [ 138, 526, 23 ], [ 175, 534, 0 ] ].reverse();
		} else {
			p2DeckPlace( deck );
			p2DeckPlace( potions, false, true );
			cPosX = 820;
			cPosY = 180;
			pOffset = -35;
			attachPos = [ [ 923, 160, 90 ], [ 916, 127, 67 ], [ 894, 95, 45 ], [ 862, 74, 23 ], [ 825, 66, 0 ] ].reverse();
		}
	}

	this.giveCards = function ( cards ) {
		victoryPile = victoryPile.concat( cards ).concat( this.getAllCards() );
		if ( playerNum == 1 ) {
			p1DeckPlace( victoryPile, true );
		} else {
			p2DeckPlace( victoryPile, true );
		}
	}
	/**
	 * Initialize the player's hand by taking a card
	 * Return true if there are more cards in the deck
	 */
	this.start = function () {
		if ( deck.length == 0 ) {
			swapDeck();
		}
		// Get the character card from the deck
		character = deck.pop();
		character.placeCard( cPosX, cPosY, true );

		// Remove any previous attachments
		attachments = [];
		if ( potionCard != null ) {
			potionCard = null;
		}
		disableAttach = false;
		disablePotion = false;

		return deck.length > 0;
	}

	/**
	 * Get the player an attachment if possible.
	 * Return true if an attachment was placed
	 */
	this.putAttachment = function ( disable = false ) {
		if ( !disable && !disableAttach ) {
			if ( deck.length > 0 && attachments.length < 5 ) {
				attachments.push( deck.pop() );
				let tempos = attachPos[ attachments.length - 1 ];
				attachments[ attachments.length - 1 ].placeCard( tempos[ 0 ], tempos[ 1 ], true, true, tempos[ 2 ] );
				return;
			}
		}
		disableAttach = true;
	}

	/**
	 * Use a potion card
	 * Return true if a potion was used
	 */
	this.usePotion = function ( disable = false ) {
		if ( !disable && potions.length > 0 && potionCard == null ) {
			potionCard = potions.pop();
			potionCard.placeCard( cPosX + pOffset, cPosY, true, false );
		}
		disablePotion = true;
	}

	/**
	 *
	 * @param {Cards} faceCard The highest face card on the field. Null if none
	 */
	this.characterValue = function ( faceCard ) {
		let value = character.cardValue();
		faceCard = compareFaceCard( faceCard, this.getFaceCard() );

		// If the character card is an Ace, it will also be affect
		// by a face card
		if ( value == 1 && faceCard != null ) {
			value += faceCard.cardValue() + 2;
		}

		attachments.forEach( card => {
			// All attachments are ceil(card / 2)
			value += Math.ceil( card.cardValue() / 2.0 );
			if ( card.cardValue() == 1 && faceCard != null ) {
				// adjust the Ace's value. We add 3 because we already added the 1
				value += faceCard.cardValue() + 2;
			}
		} );
		value += potionCard != null ? potionCard.cardValue() : 0;
		return value;
	}

	/**
	 * Return the highest face card if exist. If no face card exists, return null
	 */
	this.getFaceCard = function () {
		/** @type {Cards} */
		let faceCard = null;
		/** @type {Cards} */
		let tempCard = character.cardValue() > 10 ? character : null;
		// Search the attachment for the highest face card
		attachments.forEach( card => {
			if ( card.cardValue() > 10 && ( faceCard == null || card.cardValue() > faceCard.cardValue() ) ) {
				faceCard = card;
			}
		} );

		// If the potion card is great than the tempCard, we want that
		// Since the tempCard original have to be null or a face, this
		// ensure that the tempCard remains a face card
		if ( potionCard != null ) {
			if ( ( tempCard != null && potionCard.cardValue() > tempCard.cardValue() ) ||
				potionCard.cardValue() > 10 ) {
				tempCard = potionCard;
			}
		}
		if ( faceCard == null ||
			( tempCard != null && tempCard.cardValue() > faceCard.cardValue() ) ) {
			faceCard = tempCard;
		}

		return faceCard;
	}
	this.getAllowAttachment = function () {
		return attachments.length < 5 && deck.length > 0 && !disableAttach;
	}
	this.getPotionAllowed = function () { return potions.length > 0 && !disablePotion; }
	this.getCharacterCard = function () { return character; }
	this.getAttachment = function () { return attachments; }
	this.getPotionCard = function () { return potionCard; }
	this.getPotionCount = function () { return potions.length; }
	this.getDeckCount = function () { return deck.length; }
	this.getVicDeckCount = function () { return victoryPile.length; }
	this.getPlayerNumber = function () { return playerNum; }
	this.getAllCards = function () {
		attachments.push( character );
		if ( potionCard != null ) {
			attachments.push( potionCard );
		}
		attachments.forEach( card => { card.remove(); } );
		return attachments;
	}

	this.debugText = function ( x, y, otherPlayer ) {
		game.debug.text( "P " + this.playerNum + " -> Deck: " + this.getDeckCount() +
			" | PotCount: " + this.getPotionCount() +
			" | Value: " + this.characterValue( otherPlayer.getFaceCard() ),
			x, y, "#FFFFFF" );
		game.debug.text( "Char: " + this.getCharacterCard() +
			" | Pot: " + this.getPotionCard() +
			" | Face: " + this.getFaceCard(), x, y + 25, "#FFFFFF" );
		game.debug.text( "Attachment: " + this.getAttachment(), x, y + 50, "#FFFFFF" );
	}


	swapDeck();
}
