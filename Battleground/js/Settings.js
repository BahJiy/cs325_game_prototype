/** @param {Phaser.Game} game */
GameStates.makeSettings = function ( game, shared ) {
	let keyChange = null; // the player to change hotkey
	let infoText = null; // display true if infotext should be displayed
	let hotKeys1 = null; // display hotkey for player 1
	let hotKeys2 = null; // display hokey for player 2
	let tickText = null; // A ticker text like New York Stock market

	// Make the ticker look cool by fading out
	let changeTick = ( message ) => {
		tickText.alpha = 1;
		tickText.setText( message );
		game.add.tween( tickText ).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
	}

	return {
		create: () => {
			randBackground( game );
			game.add.text( game.world.centerX, 100, "Settings", {
				fontSize: 50,
				fill: "#FFFFFF",
				stroke: "#000000",
				strokeThickness: 3
			} );

			let style = { fontSize: 25, fill: "#FFFFFF", stroke: "#000000", strokeThickness: 3 };
			tickText = game.add.text( game.world.centerX, 550, "", style );
			tickText.alpha = 0;
			let backText = game.add.text( 930, 570, "Back", {
				fontSize: 35,
				fill: "#FF0000",
				stroke: "#FFFFFF",
				strokeThickness: 3
			} );
			backText.inputEnabled = true;
			backText.events.onInputUp.add( () => { game.state.start( "MainMenu" ) } );

			let group = game.add.group();
			infoText = game.add.text( 500, 300, "Enable Player Information Text: " + shared.infoText, style, group );
			hotKeys1 = game.add.text( 500, 350, "Player 1 Hotkey: " + String.fromCharCode( shared.key1 ), style, group );
			hotKeys2 = game.add.text( 500, 400, "Player 2 Hotkey: " + String.fromCharCode( shared.key2 ), style, group );
			group.forEach( ele => { ele.inputEnabled = true; ele.anchor.setTo( 1, 1 ) }, this );
			infoText.events.onInputDown.add( () => {
				shared.infoText = !shared.infoText;
				changeTick( "Changed InfoText to " + shared.infoText );
			} );
			hotKeys1.events.onInputDown.add( () => { keyChange = "key1"; changeTick( "Press a key now!" ); } );
			hotKeys2.events.onInputDown.add( () => { keyChange = "key2"; changeTick( "Press a key now!" ); } );

			// detect key press to set the hotkey
			game.input.keyboard.addCallbacks( this, key => {
				if ( keyChange != null ) {
					shared[ keyChange ] = [ key.keyCode, key.key ];
					changeTick( "Changed hotkey to " + key.key );
					keyChange = null;
				}
			} );
		},
		update: () => {
			infoText.setText( "Enable Player Information Text: " + shared.infoText );
			hotKeys1.setText( "Player 1 Hotkey: " + shared.key1[ 1 ] );
			hotKeys2.setText( "Player 2 Hotkey: " + shared.key2[ 1 ] );
			if ( shared.key1[ 0 ] == shared.key2[ 0 ] ) {
				tickText.setText( "Player 1 and Player 2 hotkeys are the same!" );
				tickText.alpha = 1;
			}
		}
	}
}
