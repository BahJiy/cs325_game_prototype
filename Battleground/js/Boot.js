"use strict";

let GameStates = {};
/**
 *
 * @param {Phaser.Game} game
 */
GameStates.makeBoot = function ( game ) {
    return {
        init: function () {
            game.stage.disableVisibilityChange = true;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            PIXI.Sprite.defaultAnchor = { x: 0.5, y: 0.5 };
        },

        preload: function () {
            game.load.image( "background1", "assets/background1.png" );
            game.load.image( "background2", "assets/background2.png" );
            game.load.image( "background3", "assets/background3.png" );
            game.load.image( "background4", "assets/background4.png" );
            game.load.image( "instruction", "assets/instruction.png" );

            // Cards
            game.load.image( "spades", "assets/spade.png" );
            game.load.image( "hearts", "assets/heart.png" );
            game.load.image( "clubs", "assets/club.png" );
            game.load.image( "diamonds", "assets/diamond.png" );
            game.load.image( "cardDown", "assets/cardBack.png" );
        },

        create: function () {
            // Go to the actually loader to load the rest
            game.state.start( 'MainMenu' );
        }
    };
};
