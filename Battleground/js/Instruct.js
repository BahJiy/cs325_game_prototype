/** @param {Phaser.Game} game */
GameStates.makeInstruction = function ( game, shared ) {
	/** @type {Phaser.Image} */
	let instruct = null;
	let move = 0;
	let min = 200, max = -2100;

	return {
		create: () => {
			let image = game.add.image( game.world.centerX, game.world.centerY, "background2" );
			instruct = game.add.image( 0, 75, "instruction" );
			instruct.anchor.setTo( 0, 0 );
			instruct.inputEnabled = true;
			instruct.input.enableDrag();
			instruct.events.onDragUpdate.add( () => { instruct.x = 0 } );

			game.input.mouse.mouseWheelCallback = ( event ) => {
				event.preventDefault();
				move -= event.deltaY;
			}

			let backText = game.add.text( 930, 570, "Back", {
				fontSize: 35,
				fill: "#FF0000",
				stroke: "#FFFFFF",
				strokeThickness: 3
			} );
			backText.inputEnabled = true;
			backText.events.onInputUp.add( () => { game.state.start( "MainMenu" ) } );
		},

		update: () => {
			if ( instruct.y + move / 50 > min ) {
				// Push it up
				move -= Math.ceil( Math.abs( ( instruct.y + move - min ) / 2 ) );
			} else if ( instruct.y - move / 50 < max ) {
				// Push it down
				move += Math.ceil( Math.abs( ( instruct.y + move - max ) / 2 ) );
			}
			instruct.y += move / 5;
			move = move * 0.90;
		}
	}
}
