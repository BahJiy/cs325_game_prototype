/**
 * An implementation of Fisher-Yates shuffling algorithm
 * @param {Array<any>} list
 * @param {number} times
 * @returns Array<any>
 */
function shuffleList ( list, times = 0 ) {
	let shuffledList = [];
	let copyList = list.slice();
	while ( copyList.length != 0 ) {
		shuffledList.push(
			copyList.splice(
				Math.floor(
					Math.random() * copyList.length ),
				1
			)[ 0 ]
		);
	}
	return times <= 0 ? shuffledList : shuffleList( shuffledList, times - 1 );
}

/**
 * This class contains data for one card
 * @param {Phaser.Game} phaserGame
 * @param {number} cardVal The value of the card
 * @param {string} cardType The type of the card
 */
function Cards ( phaserGame, cardVal, cardType ) {

	/** @type {Phaser.Game} */
	let game = phaserGame;
	/** @type {string} */
	let stringRep = "";
	/** @type {number} */
	let value = cardVal;
	switch ( value ) {
		case 1: stringRep = "A"; break;
		case 11: stringRep = "J"; break;
		case 12: stringRep = "Q"; break;
		case 13: stringRep = "K"; break;
		default: stringRep = "" + value; break;
	}
	/** @type {Phaser.Image} */
	let faceImage = game.add.image( 0, 0, cardType );
	/** @type {Phaser.Image} */
	let downImage = game.add.image( 0, 0, "cardDown" );
	/** @type {Phaser.Text} */
	let valueText = game.add.text( 0, 0, stringRep, {
		fill: cardType[ 0 ] == "h" || cardType[ 0 ] == "d" ? "#000000" : "#FFFFFF",
		font: "50px Arial",
		stroke: cardType[ 0 ] == "h" || cardType[ 0 ] == "d" ? "#FFFFFF" : "#000000",
		strokeThickness: 3
	} );
	valueText.visible = false;
	faceImage.visible = false;
	downImage.visible = false;

	faceImage.inputEnabled = true;
	faceImage.input.enableDrag();
	faceImage.events.onDragUpdate.add( ( tp, pointer ) => {
		faceImage.position.setTo( pointer.x, pointer.y );
		valueText.position.setTo( pointer.x, pointer.y );
		downImage.position.setTo( pointer.x, pointer.y );
	} );
	faceImage.events.onDragStop.add( () => {
		console.log( this.toString() + " " + this.cardPosition().x + " | " + this.cardPosition().y );
	} )

	/**
	 * The set the object to given position and set its visibility
	 * @param {Phaser.Image | Phaser.Text} obj The object to set
	 * @param {number} x X position
	 * @param {number} y Y position
	 * @param {boolean} faceUp Should this be visible?
	 */
	let setFlip = ( obj, x, y, faceUp = true, top = true ) => {
		if ( x != null && y != null ) {
			obj.position.setTo( x, y );
		}
		obj.visible = faceUp;
		if ( top ) {
			game.world.bringToTop( obj );
		} else {
			game.world.sendToBack( obj );
		}
	}

	/**
	 * Place the card at this position face up/down
	 * @param {number} x X position
	 * @param {number} y Y Position
	 * @param {boolean} faceUp Should the card be faced up? Default: false
	 */
	this.placeCard = function ( x, y, faceUp = false, top = true, angle = 0 ) {
		setFlip( top ? faceImage : valueText, x, y, faceUp, top );
		setFlip( top ? valueText : faceImage, x, y, faceUp, top );
		setFlip( downImage, x, y, !faceUp, top );

		faceImage.angle = valueText.angle = downImage.angle = angle;
	}

	/**
	 * Flip the card so that it is face up/down
	 * @param {boolean} faceUp Should the card be face up? Default: true
	 */
	this.flip = function ( faceUp = true ) {
		this.placeCard( null, null, faceUp );
	}

	/**
	 * Remove the card from play
	 */
	this.remove = function () {
		setFlip( valueText, 0, 0, false );
		setFlip( faceImage, 0, 0, false );
		setFlip( downImage, 0, 0, false );
	}

	this.toString = function () { return stringRep + " of " + faceImage.key[ 0 ]; }
	this.cardFaceUp = function () { return faceImage.visible; }
	this.cardValue = function () { return value; }
	this.cardPosition = function () { return valueText.position; }
	this.cardVisible = function () { return faceImage.visible; }
}
