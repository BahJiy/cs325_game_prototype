"use strict";

/**
 * Set a random BG
 * @param {Phaser.Game} game
 */
let randBackground = ( game, alpha = 1,  ) => {
    let bg = game.rnd.between( 1, 4 );
    let image = game.add.image( game.world.centerX, game.world.centerY, "background" + bg );
    image.alpha = alpha;
    game.world.sendToBack( image );
    return image;
}

/** @param {Phaser.Game} game */
GameStates.makeMainMenu = function ( game, shared ) {

    return {
        create: function () {
            randBackground( game );
            let style = { font: "30px Gloria Hallelujah", fill: "#000000", stroke: "#FFFFFF", strokeThickness: 3 };
            let title = game.add.text( game.world.centerX, 200, "Battleground", {
                font: "50px Inconsolata", fill: "#000000", stroke: "#FFFFFF", strokeThickness: 3
            } )
            let startText = game.add.text( game.world.centerX, 300, "Start", style );
            let settingText = game.add.text( game.world.centerX, 350, "Settings", style );
            let instructionText = game.add.text( game.world.centerX, 400, "Instruction", style );

            instructionText.inputEnabled = settingText.inputEnabled = startText.inputEnabled = true;
            startText.events.onInputUp.add( () => { game.state.start( "Game" ) } );
            settingText.events.onInputUp.add( () => { game.state.start( "Settings" ) } );
            instructionText.events.onInputUp.add( () => { game.state.start( "Instruction" ) } );
        },
    };
};
