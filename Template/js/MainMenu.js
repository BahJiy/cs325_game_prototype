"use strict";

/** @param {Phaser.Game} game */
GameStates.makeMainMenu = function ( game, shared ) {

    return {
        create: function () { },

        render: function () {
            game.debug.text( "Pointer x: " + game.input.position.x + " y: " + game.input.position.y, 25, 25 );
        },

        update: function () { }
    };
};
