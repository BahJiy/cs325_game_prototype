"use strict";

let GameStates = {};
/**
 *
 * @param {Phaser.Game} game
 */
GameStates.makeBoot = function ( game ) {
    return {
        init: function () {
            game.stage.disableVisibilityChange = true;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
        },

        preload: function () { },

        create: function () {
            // Go to the actually loader to load the rest
            game.state.start( 'MainMenu' );
        }
    };
};
