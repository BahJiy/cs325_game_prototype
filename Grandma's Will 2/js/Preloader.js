"use strict";

/**
 *
 * @param {Phaser.Game} game
 */
GameStates.makePreloader = function ( game ) {

	/** @type {Phaser.Image} */
	let background = null;
	/** @type {Phaser.Image} */
	let preloadBar = null;

	return {
		preload: function () {
			game.load.script( 'webfont', "//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" )

			// Set the background and preload bar
			background = game.add.image( 0, 0, 'pMenu' );
			preloadBar = game.add.image( 200, 500, 'loadbar' );
			// Auto crop the load bar
			game.load.setPreloadSprite( preloadBar );

			game.load.audio( "aMenu", "assets/end.mp3" );
			game.load.audio( "aHouse", "assets/aHouse.mp3" );
			game.load.audio( "aGame", "assets/aGame.mp3" );
			// Main Menu
			game.load.image( "bStart", "assets/start_button.png" );
			game.load.image( "leaf1", "assets/leaves/1.png" );
			game.load.image( "leaf2", "assets/leaves/2.png" );
			game.load.image( "leaf3", "assets/leaves/3.png" );
			// House
			game.load.image( "houseIn", "assets/houseIn.png" );
			game.load.image( "kitchen", "assets/kitchen.png" );
			game.load.image( "dinning", "assets/dinning.png" );
			game.load.image( "living", "assets/living.png" );
			game.load.image( "attic", "assets/attic.png" );
			game.load.image( "houseIn_de", "assets/houseIn_de.png" );
			game.load.image( "kitchen_de", "assets/Kitchen_de.png" );
			game.load.image( "dinning_de", "assets/Dinning_de.png" );
			game.load.image( "living_de", "assets/Living_de.png" );
			game.load.image( "attic_de", "assets/attic_de.png" );
			game.load.image( "grandmaRoom", "assets/grandmaRoom.png" );
			game.load.image( "momRoom", "assets/momRoom.png" );
			game.load.image( "momRoomTV", "assets/momRoomTV.png" );
			// Lit
			game.load.image( "house_lit", "assets/house_lit.png" );
			game.load.image( "kitchen_lit", "assets/kitchen_lit.png" );
			game.load.image( "dinning_lit", "assets/dinning_lit.png" );
			game.load.image( "living_lit", "assets/living_lit.png" );
			game.load.image( "attic_lit", "assets/attic_lit.png" );
			game.load.image( "inAttic_lit", "assets/attic_lit.png" );
			game.load.image( "inKitchen_lit", "assets/kitch_lit.png" );
			game.load.image( "inDinning_lit", "assets/dine_lit.png" );
			game.load.image( "inLiving_lit", "assets/live_lit.png" );
			// Game
			game.load.image( "player", "assets/player.png" );
			game.load.image( "grandma", "assets/grandma.png" );
			game.load.image( "arrowHead", "assets/arrowHead.png" );
			game.load.image( "ball", "assets/ball.png" );
			game.load.image( "cannon", "assets/cannon.png" );
			game.load.image( "forest", "assets/forest.jpg" );
			game.load.image( "bForest", "assets/blood_forest.png" );
		},

		create: function () { },

		update: function () {
			background.destroy();
			preloadBar.destroy();
			game.camera.fade( 0x0, 500 );
			game.state.start( "MainMenu" );
		}
	};
};
