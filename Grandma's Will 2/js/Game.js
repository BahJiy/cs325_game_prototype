"use strict"


/** @param {Phaser.Game} game */
GameStates.makeGame = function ( game, shared ) {

	/** @type {Phaser.Sprite} */
	let player = null;
	/** @type {Phaser.Sprite} */
	let arrowHead = null;
	/** @type {Phaser.Sprite} */
	let cannon = null;
	/** @type {Phaser.Image} */
	let background = null;
	/** @type {Phaser.Physics.P2.CollisionGroup} */
	let playerGroup = null;
	/** @type {Phaser.Physics.P2.CollisionGroup} */
	let ballGroup = null;
	/** @type {{up: Phaser.Keyboard, down: Phaser.Keyboard, left: Phaser.Keyboard, right: Phaser.Keyboard, space: Phaser.Keyboard}} */
	let cursor = null;
	/** @type {{height: number, maxHeight, number, startTime: number, time: number: maxTime: number, shoot: number}} */
	let data = { height: 0, maxHeight: 0, startTime: 0, time: 0, maxTime: 0, shoot: 0 };
	/** @type {Array<Shoot>} */
	let bullet = []
	/** @type {number} */
	let ballScale = 1;
	/** @type {number} */
	let worldPos = 0;
	/** @type {number} */
	let shootWait = 0;
	/** @type {number} */
	let gameState = 0;
	/** @type {Phaser.Text} */
	let instruction = null;
	/** @type {Phaser.Text} */
	let startEasy = null;
	/** @type {Phaser.Text} */
	let startHard = null;
	/** @type {Phaser.Text} */
	let gameContinue = null;
	/** @type {Phaser.Text} */
	let scoreText = null;

	class Shoot {
		/**
		 *
		 * @param {Phaser.Game} game
		 * @param {number} angle
		 * @param {Phaser.Point} position
		 * @param {number} scale
		 */
		constructor ( game, angle, position, scale ) {
			let ball = game.add.sprite( position.x, position.y, "ball" );
			ball.alpha = 0;
			game.physics.p2.enable( ball, true );
			ball.body.data.gravityScale = 1.5;
			ball.body.mass = 0.4;
			ball.body.setCircle( ball.width / ballScale );
			ball.body.collideWorldBounds = false;
			ball.body.setCollisionGroup( ballGroup );
			ball.body.collides( playerGroup );

			game.add.tween( ball ).to( { alpha: 1 }, 500 );

			let speed = 2000 / ( scale / 1.2 );
			let speedX = speed * Math.cos( angle );
			let speedY = speed * Math.sin( angle );
			ball.body.moveRight( speedX );
			ball.body.moveDown( speedY );
			let destroyTime = game.time.now + 2000;

			/**
			 * Check if ball is still in bounds
			 * If ball is out of bound, destroy the ball and
			 * return true to mark for deletion
			 * @param {Phaser.Rectangle} camRect
			*/
			this.update = function () {
				if ( game.time.now > destroyTime ) {
					ball.destroy();
					return true;
				}
				return false;
			}
			this.rBall = ball
		}

	}
	function reset () {
		// We destroy because it is easier to reset up everything from
		// scratch than to adjust the position with the physic engine
		// still running. Slows down the game a tad bit (a few milliseconds)
		player.destroy();
		arrowHead.destroy();
		cannon.destroy();
		// Reset camera
		game.camera.reset();
		game.camera.scale.setTo( 1 );
		game.camera.flash( 0xFFFFFF, 500 );
		// Remove any bullets
		bullet.forEach( ele => {
			ele.rBall.destroy();
		} );
		bullet = [];
		data.height = 0;
		data.time = 0;
		data.shoot = 0;
		// Turn on the text
		instruction.alpha = 1;
		startEasy.alpha = 1;
		startHard.alpha = 1;
		gameContinue.alpha = shared.story ? 1 : 0;
		// Enable clicking
		startEasy.inputEnabled = true;
		startHard.inputEnabled = true;
		gameContinue.inputEnabled = shared.story;

		gameState = 2;
	}

	function start () {
		// Create the static game object and enable physic for them
		player = game.add.sprite( game.width / 1.5, game.height / 1.5, "grandma" );
		arrowHead = game.add.sprite( game.width / 2, game.height - player.width, "arrowHead" );
		cannon = game.add.sprite( game.width / 2, game.height - 50, "cannon" );
		cannon.alpha = 0; // Ask me why I did this. It is because of camera scaling and world positioning
		game.physics.p2.enable( player, true ); // true -> false to disable debugging

		// Set up player's body physic
		player.body.setCircle( player.width / 2 );
		player.body.collideWorldBounds = true;
		player.body.setCollisionGroup( playerGroup );
		player.body.collides( ballGroup );
		player.body.moveUp( 1250 );

		// Set up movement keys
		cursor = game.input.keyboard.createCursorKeys();
		cursor.space = game.input.keyboard.addKey( Phaser.Keyboard.SPACEBAR );
		game.input.keyboard.addKeyCapture( Phaser.Keyboard.SPACEBAR );

		// Set up camera
		game.camera.follow( player, 0, 0, 0.3 );
		game.camera.bounds = null;
		game.camera.deadzone = new Phaser.Rectangle( 0, 50, game.world.width, game.world.height - 50 );

		// Turn off the text
		instruction.alpha = 0;
		startEasy.alpha = 0;
		startHard.alpha = 0;
		gameContinue.alpha = 0;
		// Disable clicking
		startEasy.inputEnabled = false;
		startHard.inputEnabled = false;
		gameContinue.inputEnabled = false;

		gameState = 1;
		data.startTime = game.time.now;
	}

	return {

		create: function () {
			worldPos = game.world.y;
			// Set up the physic system
			game.physics.startSystem( Phaser.Physics.P2JS );
			game.physics.p2.applySpringForces = true;
			// We don't want a top
			game.physics.p2.setBounds( 0, 0, game.world.width, game.world.height, true, true, false, true );
			game.physics.p2.restitution = 0.7;
			game.physics.p2.gravity.y = 500;
			game.physics.p2.applySpringForces = true;
			game.physics.p2.updateBoundsCollisionGroup();
			playerGroup = game.physics.p2.createCollisionGroup();
			ballGroup = game.physics.p2.createCollisionGroup();

			background = game.add.image( 0, 0, "forest" );
			background.anchor.setTo( 0.5, 1 );
			background.position.setTo( background.width / 2, game.height );

			// Create the text
			instruction = game.add.text( game.world.centerX, game.world.centerY,
				"Keep Grandma up in the air as long as possible!\nUse the arrows keys to adjust the crosshair and \nspace to shoot.Be careful to keep track of the crosshair.\n\nStart:",
				{
					fill: 0x0,
					font: "30px Inconsolata",
					stroke: "#FFFFFFFF",
					strokeThickness: 5
				} );
			startEasy = game.add.text( 230, 363, "Easy",
				{
					fill: 0x0,
					font: "30px Inconsolata",
					stroke: "#1723d1",
					strokeThickness: 5
				} );
			startHard = game.add.text( 320, 363, "Hard",
				{
					fill: 0x0,
					font: "30px Inconsolata",
					stroke: "#dd1818",
					strokeThickness: 5
				} );
			gameContinue = game.add.text( 480, 363, "Continue story",
				{
					fill: 0x0,
					font: "30px Inconsolata",
					stroke: "#dd1818",
					strokeThickness: 5
				} );
			scoreText = game.add.text( 480, 363, "",
				{
					fill: "#FF50FF",
					font: "30px Inconsolata",
					stroke: "#FFFFFF",
					strokeThickness: 2
				} );
			// Set the anchor
			instruction.anchor.setTo( 0.5 );
			startEasy.anchor.setTo( 0.5 );
			startHard.anchor.setTo( 0.5 );
			gameContinue.anchor.setTo( 0.5 );
			scoreText.fixedToCamera = true;
			scoreText.cameraOffset.setTo( 0, 0 );
			// Enable inputs
			startEasy.inputEnabled = true;
			startHard.inputEnabled = true;
			gameContinue.inputEnabled = false;
			gameContinue.alpha = 0;
			// Hook the events
			startEasy.events.onInputDown.add( ( obj ) => {
				ballScale = 1; // We make the ball big
				start(); // Start the game
			}, this )
			startHard.events.onInputDown.add( ( obj ) => {
				ballScale = 2; // We make the ball big
				start(); // Start the game
			}, this )
			gameContinue.events.onInputDown.add( ( obj ) => {
				reset();
				instruction.destroy();
				startEasy.destroy();
				startHard.destroy();
				background.destroy();
				gameContinue.destroy();
				shared.death = true;
				shared.music.stop();
				game.state.start( "House" );
			}, this );

			game.camera.flash( 0x0, 1000 );

			shared.music.stop();
			shared.music.destroy();
			shared.music = game.add.audio( "aGame", 1.0, true );
			shared.music.play();
		},

		render: function () {
			//game.debug.camera( game.camera );
			//game.debug.cameraInfo( game.camera, 25, 100 );
			//game.debug.text( "Mouse X:" + game.input.activePointer.worldX + " Y:" + game.input.activePointer.worldY, 25, 25 );
			if ( gameState == 1 ) {
				//game.debug.spriteCoords( player, 25, 50 );
				//game.debug.spriteCoords( arrowHead, 25, 100 );
				//game.debug.text( "World X: " + game.world.x + " Y: " + game.world.y +
				//	"\nWorld Width: " + game.world.width + " Height: " + game.world.height, 25, 150 );
				//game.debug.text( "Scale: " + game.camera.scale, 25, 150 );
			}
		},

		update: function () {
			if ( gameState == 1 ) {
				game.physics.p2.setBounds( 0, 0, game.world.width / game.camera.scale.x, game.world.height / game.camera.scale.y,
					true, true, false, true );
				let scale = player.y >= 0 ? 0 : player.y / 15000;
				scale = Math.abs( scale ) <= 0.6 ? scale : -0.6;
				game.camera.scale.setTo( 1 + scale );

				background.alpha = game.camera.scale.y;

				arrowHead.y += worldPos - game.world.y;
				cannon.y += worldPos - game.world.y;
				worldPos = game.world.y;

				if ( cursor.up.isDown ) {
					arrowHead.y -= 10 / game.camera.scale.y;
				} else if ( cursor.down.isDown ) {
					arrowHead.y += 10 / game.camera.scale.y;
				} if ( cursor.left.isDown ) {
					arrowHead.x -= 10 / game.camera.scale.x;
				} else if ( cursor.right.isDown ) {
					arrowHead.x += 10 / game.camera.scale.x;
				} if ( cursor.space.isDown && game.time.now > shootWait ) {
					let angle = cannon.position.angle( arrowHead.position );
					cannon.rotation = angle;
					bullet.push( new Shoot( game, angle, cannon.position, game.camera.scale.y ) );
					shootWait = game.time.now + 250;
					data.shoot += 1; // Scoreboard
				}
				// game.world.y - const == destination
				if ( arrowHead.world.x < 0 ) {
					arrowHead.x = 10;
				} if ( arrowHead.world.x > game.world.width ) {
					arrowHead.x = game.world.width;
				}

				if ( player.world.y > 500 ) {
					reset()
				}

				data.height = Math.floor( Math.abs( player.world.y ) / 100 ) - 5;
				data.maxHeight = data.maxHeight < data.height ? data.height : data.maxHeight;
				data.time = Math.floor( ( game.time.now - data.startTime ) / 1000 );
				data.maxTime = data.maxTime < data.time ? data.time : data.maxTime;
				scoreText.setText( "Highest Height: " + data.maxHeight + "\tLong Time: " + data.maxTime +
					"\nCurrent Height: " + data.height + "\tCurrent Time: " + data.time + "\n# of Shoots: " + data.shoot );

				let removeAr = [];
				bullet.forEach( ele => {
					if ( ele.update() ) {
						removeAr.push( bullet.indexOf( ele ) );
					}
				} );
				removeAr.forEach( ele => {
					bullet.splice( ele, 1 );
				} );
			}
		}
	};
}
