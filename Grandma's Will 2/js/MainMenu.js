"use strict";

/** @param {Phaser.Game} game */
GameStates.makeMainMenu = function ( game, shared ) {
    /**
     * Holds metadata for a leaf
     */
    class Leaf {
        /**
         * Initialize a leaf's metadata
         * @param {Phaser.Sprite} sprite
         */
        constructor ( sprite ) {
            let xMove = 0.1 * Math.random() > 0.70 ? 1 : -1;
            let xChange = 0.01;

            /**
             * Must be called every cycle to update the leaf's metadata
             */
            this.update = function () {
                sprite.angle += 0.2;
                if ( xMove < -1 - Math.random() ) xChange = -0.01;
                else if ( xMove > 1 + Math.random() ) xChange = 0.01;
                xMove -= xChange;
                sprite.x += xMove;
                sprite.y += 0.5;
            }

            /**
             * Deletes and return true if the leaf should be remove
             */
            this.delete = function () {
                if ( sprite.y > game.height + 50 ) {
                    sprite.destroy();
                    return true;
                } else return false;
            }
        }
    }

    /** @type {Phaser.Button} */
    let playButton = null;
    /** @type {Array<Leaf>} */
    let leafL = null;
    /** @type {Object.<string, Phaser.Image>} */
    let images = null;

    return {
        create: function () {
            game.camera.flash( 0x0, 750 );

            shared.story = false;

            images = {};
            images.background = game.add.image( 0, 0, "pMenu" );
            images.bStart = game.add.button( 121, 400, "bStart", () => { game.state.start( "Game" ) } );
            images.lit = game.add.image( 725, 335, "house_lit" );

            images.lit.alpha = 0;
            images.lit.inputEnabled = true;
            images.lit.events.onInputOver.add( () => {
                game.add.tween( images.lit ).to( { alpha: 0.8 }, 750, "Linear", true );
            }, this );
            images.lit.events.onInputOut.add( () => {
                game.add.tween( images.lit ).to( { alpha: 0 }, 750, "Linear", true );
            }, this );
            images.lit.events.onInputDown.add( () => {
                game.state.start( "House" );
            }, this );

            game.add.text( 875, 540, "Grandma's Will v0.25", { font: "12px Inconsolata", fill: "#ffffff" } );

            leafL = [];

            shared.music = game.add.audio( "aMenu", 1.0, true );
            shared.music.play();
        },

        render: function () {
            //game.debug.text( "Pointer x: " + game.input.position.x + " y: " + game.input.position.y, 25, 25 );
        },

        update: function () {
            // Restrict to only 50 leaves at a time
            if ( leafL.length < 50 ) {
                // Randomly choose if a leaf should be created
                let sprite = null;
                switch ( Math.floor( Math.random() * 250 + 1 ) ) {
                    case 1: sprite = game.add.sprite( Math.random() * game.width, -( Math.random() * 50 ), 'leaf1' );
                        break;
                    case 2: sprite = game.add.sprite( Math.random() * game.width, -( Math.random() * 50 ), 'leaf2' );
                        break;
                    case 3: sprite = game.add.sprite( Math.random() * game.width, -( Math.random() * 50 ), 'leaf3' );
                        break;
                }
                // Just in case we actually did not made a leaf
                if ( sprite != null ) {
                    sprite.anchor.setTo( 0.5, 0.5 );
                    sprite.angle = Math.floor( Math.random() * 360 + 1 );
                    leafL.push( new Leaf( sprite ) );
                }
            }

            // Update and remove the leaf as needed
            let toRemove = []
            leafL.forEach( ele => {
                ele.update();

                if ( ele.delete() )
                    toRemove.push( leafL.indexOf( ele ) );
            } );
            toRemove.forEach( ele => {
                leafL.splice( ele, 1 );
            } );
        }
    };
};
