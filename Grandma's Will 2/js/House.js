"use strict";

/** @param {Phaser.Game} game */
GameStates.makeHouse = function ( game, shared ) {

	/** @type {Object.<string, Phaser.Image> | Phaser.Image} */
	let images = {};
	/** @type {TypeWriter} */
	let typewriter = null;

	/** @type {Object.<string, boolean>} */
	let roomStory = {};
	/** @type {Phaser.Rectangle} */
	let door1 = new Phaser.Rectangle( 130, 50, 182, 330 )
	/** @type {function} */
	let door1Story = null;
	/** @type {Phaser.Rectangle} */
	let door2 = new Phaser.Rectangle( 465, 50, 182, 330 )
	/** @type {function} */
	let door2Story = null;
	/** @type {Phaser.Rectangle} */
	let door3 = new Phaser.Rectangle( 675, 50, 182, 330 )
	/** @type {function} */
	let door3Story = null;

	/** @type {boolean} */
	let inAttic = false;
	/** @type {function} */
	let runStory = null;
	/** @type {number} */
	let gameTime = 0;
	/** @type {number} */
	let roomCount = 0;

	/** Just to choose a random phrase to say in each room */
	function chooseRoomStory ( room ) {
		if ( !shared.death && roomStory[ room ] != true ) {
			typewriter.startDisplay( [
				[ TypeWriter.TYPE.MESSAGE, roomStoryText[ Math.ceil( Math.random() * roomStoryText.length - 1 ) ],
					child, true ],
			], 0, new Phaser.Point( 420, 400 ), 30, stroke, strokeThick );
			roomStory[ room ] = true;
		}
	}

	function mainRoom ( inside = true ) {
		game.camera.flash( 0x0, 500, true );
		images[ shared.death ? "houseIn_de" : "houseIn" ].alpha = inside ? 1 : 0;
		images[ "kitchen_lit" ].inputEnabled = inside;
		images[ "dinning_lit" ].inputEnabled = inside;
		images[ "living_lit" ].inputEnabled = inside;
		images[ "attic_lit" ].inputEnabled = inside;
		// Keep track of how many time we entered a room
		roomCount += inside ? 1 : 0;
	}

	function kitchenRoom ( inside = true ) {
		if ( typewriter.finished() ) {
			chooseRoomStory( "kitchen" );
			// If we are inside Kitchen, we are not in main room
			mainRoom( !inside );
			images[ shared.death ? "kitchen_de" : "kitchen" ].alpha = inside ? 1 : 0;
			images[ "inKitchen_lit" ].inputEnabled = inside;
		}
	}
	function dinningRoom ( inside = true ) {
		if ( typewriter.finished() ) {
			chooseRoomStory( "dinning" );
			// If we are inside Dinning, we are not in main room
			mainRoom( !inside );
			images[ shared.death ? "dinning_de" : "dinning" ].alpha = inside ? 1 : 0;
			images[ "inDinning_lit" ].inputEnabled = inside;
		}
	}
	function livingRoom ( inside = true ) {
		if ( typewriter.finished() ) {
			chooseRoomStory( "living" );
			// If we are inside Living, we are not in main room
			mainRoom( !inside );
			images[ shared.death ? "living_de" : "living" ].alpha = inside ? 1 : 0;
			images[ "inLiving_lit" ].inputEnabled = inside;
		}
	}
	function atticRoom ( inside = true ) {
		if ( typewriter.finished() ) {
			chooseRoomStory( "attic" );
			mainRoom( !inside );
			images[ "attic" ].alpha = inside ? 1 : 0;
			// The attic_lit image is a bit buggy. Not sure why, but destroy it!
			images[ "attic_lit" ].destroy();

			door1Story = inMomRoom( game, shared, typewriter, images[ "attic" ] );
			door2Story = inEmptyRoom( game, typewriter, images[ "attic" ] );
			door3Story = inGrandmaRoom( game, typewriter, images[ "attic" ] );
			gameTime = game.time.now + 750; // Prevent accidental clicks
			inAttic = true;
		}
	}

	function cleanup () {
		for ( let ele in images ) {
			images[ ele ].destroy();
		}
		game.stage.backgroundColor = 0x0;
	}

	return {
		create: function () {
			game.camera.flash( 0x0, 1000 );
			game.stage.backgroundColor = "#380a10";
			// All the pictures we will need to load
			let pictures = [
				[ shared.death ? "houseIn_de" : "houseIn", 0, 0 ],
				[ shared.death ? "kitchen_de" : "kitchen", 0, 0 ],
				[ shared.death ? "dinning_de" : "dinning", 0, 0 ],
				[ shared.death ? "living_de" : "living", 0, 0 ],
				[ shared.death ? "attic_de" : "attic", 0, 0 ],
				[ "grandmaRoom", 0, 0 ],
				[ "momRoom", 0, 0 ],
				[ "kitchen_lit", 0, 64 ],
				[ "dinning_lit", 886, 70 ],
				[ "living_lit", 427, 70 ],
				[ "attic_lit", 220, 0 ],
				[ "inKitchen_lit", 285, 495 ],
				[ "inDinning_lit", 197, 490 ],
				[ "inLiving_lit", -25, 493 ],
				[ "inAttic_lit", 0, 0 ]
			];
			// Set some basic paramter
			pictures.forEach( ele => {
				let temp = game.add.image( ele[ 1 ], ele[ 2 ], ele[ 0 ] );
				temp.alpha = 0;
				if ( ele[ 0 ].includes( "lit" ) ) {
					temp.inputEnabled = false;
					temp.events.onInputOver.add( () => {
						game.add.tween( temp ).to( { alpha: 1 }, 250, "Linear", true );
					}, this );
					temp.events.onInputOut.add( () => {
						game.add.tween( temp ).to( { alpha: 0 }, 250, "Linear", true );
					}, this );
				}
				images[ ele[ 0 ] ] = temp;
			} );

			// Have to manually add the event handler for going in and out of rooms
			images[ "kitchen_lit" ].events.onInputDown.add( () => {
				kitchenRoom();
			} );
			images[ "dinning_lit" ].events.onInputDown.add( () => {
				dinningRoom();
			} );
			images[ "living_lit" ].events.onInputDown.add( () => {
				livingRoom();
			} );
			images[ "attic_lit" ].events.onInputDown.add( () => {
				if ( !shared.death ) {
					atticRoom();
				} else {
					typewriter.startDisplay( [
						[ "Grandma is waiting for me...", child, true ] ],
						0, new Phaser.Point( 420, 400 ), 30, stroke, strokeThick );
				}
			} ); // Attic is a one way road
			images[ "inKitchen_lit" ].events.onInputDown.add( () => {
				kitchenRoom( false );
			} );
			images[ "inDinning_lit" ].events.onInputDown.add( () => {
				dinningRoom( false );
			} );
			images[ "inLiving_lit" ].events.onInputDown.add( () => {
				livingRoom( false );
			} );

			// Set up typewriter and display a message!
			typewriter = new TypeWriter( game, "Gloria Hallelujah" );
			if ( !shared.death ) {
				typewriter.startDisplay( [
					[ TypeWriter.TYPE.MESSAGE, "Mom! Grandma! Are you home?", child, true ],
				], 0, new Phaser.Point( 435, 400 ), 30, stroke, strokeThick );
			}

			shared.music.stop();
			if ( !shared.death ) {
				// We first enter the main room
				mainRoom();
				shared.music.destroy();
				shared.music = game.add.audio( "aHouse", 1.0, true );
				shared.music.play();
			} else {
				// We start in the attic after the game is over
				inAttic = true;
				roomCount = 0;
				runStory = deathDoor( game, typewriter );
			}
		},

		render: function () {
			//game.debug.text( "Pointer x: " + game.input.position.x + " y: " + game.input.position.y, 25, 25 );
		},

		update: function () {
			typewriter.update();

			// We dance in the attic
			if ( inAttic ) {
				if ( !shared.death ) {
					// If we are not in a story and we click on a door
					if ( runStory == null && gameTime < game.time.now && game.input.activePointer.isDown ) {
						if ( Phaser.Rectangle.containsPoint( door1, game.input.activePointer ) ) {
							runStory = door1Story;
						} else if ( Phaser.Rectangle.containsPoint( door2, game.input.activePointer ) ) {
							runStory = door2Story;
						} else if ( Phaser.Rectangle.containsPoint( door3, game.input.activePointer ) ) {
							runStory = door3Story;
						}
					} else if ( runStory != null ) {
						let temp = runStory.update();
						if ( temp == true ) {
							runStory = null;
							// Prevent accidental clicks
							gameTime = game.time.now + 750;
						} else if ( temp == 1 ) { // A bit hackish
							game.state.start( "Game" );
						}
					}
				} else {
					let temp = runStory.update();
					if ( temp == true ) {
						// We go to the main room
						gameTime = 0;
						mainRoom();
						inAttic = false;
					}
				}
			} else if ( shared.death && roomCount == 5 ) {
				if ( typewriter.finished() && gameTime == 0 ) {
					cleanup();
					// Reuse some variables
					images = game.add.image( 0, 0, "houseIn_de" );
					game.world.sendToBack( images );
					typewriter.startDisplay( [
						[ "Grandma is not in here...", child, true ],
						[ "She must be in the forest", child, true ]
					], 0, new Phaser.Point( 435, 400 ), 30, stroke, strokeThick );
					gameTime = 1;
				} else if ( typewriter.finished() && gameTime == 1 ) {
					game.camera.flash( 0x0, 500, true );
					images.destroy();
					images = game.add.image( 0, 0, "pMenu" );
					game.add.tween( images ).to( { alpha: 0 }, 250, "Linear", true, 1500 );
					gameTime = 2;
				} else if ( typewriter.finished() && gameTime == 2 ) {
					let guy = "#541e25";
					typewriter.startDisplay( [
						[ TypeWriter.TYPE.WAIT, 1500 ],
						[ "Hehe. What do we have here.", guy, true ],
						[ "A nice circle ripe for pickin'", guy, true ],
						[ TypeWriter.TYPE.SHAKE, 0.01, 500, true ],
						[ "Oh! Don't want you to escape now do I?", guy, true ],
						[ "You are going to be my ticket to fortune!", guy, true ],
						[ "HAHAHAHA!", guy, true ]
					], 0, new Phaser.Point( 100, 200 ), 30, stroke, strokeThick );
					gameTime = null;
					// And we are done!
				}
			}
		}
	};
}
