"use strict"
let child = "#f44242";
let child2 = "##fff95b";
let grandma = "#eb5bff";
let mom = "#d31b77"
let stroke = '#500050';
let strokeThick = 2;

/**
 * @param {Phaser.Game} game
 * @param {TypeWriter} typewriter - An initialized instance of TypeWriter
 * @param {Phaser.Image} curBackground - Current background object
 */
function inMomRoom ( game, shared, typewriter, curBackground ) {
	return {
		update: function () {
			// Display the story text. Mom room is a multi part room
			if ( typewriter.finished() && this.part == null ) {
				// Quick flash to change the background
				game.camera.flash( 0x0, 500 );
				curBackground.alpha = 0;
				this.newBackground = game.add.image( 0, 0, "momRoom" );
				game.world.sendToBack( this.newBackground );

				typewriter.startDisplay( inMomRoomStory1, 0, new Phaser.Point( 150, 80 ), 30, stroke, strokeThick );
				//typewriter.startDisplay( [], 0, new Phaser.Point( 150, 80 ), 30, stroke, strokeThick );
				this.part = 1;
			} else if ( typewriter.finished() && this.part == 1 ) {
				// If we are ready to display text and already started
				// Clean up and get out of here
				game.camera.flash( 0x0, 1000 );
				this.newBackground.destroy();
				this.newBackground = game.add.image( 0, 0, "momRoomTV" );
				game.world.sendToBack( this.newBackground );

				typewriter.startDisplay( inMomRoomStory2, 0, new Phaser.Point( 331, 396 ), 30, stroke, strokeThick );
				this.part = 2;
			} else if ( typewriter.finished() && this.part == 2 ) {
				game.camera.flash( 0x0, 1000, true );
				this.newBackground.destroy();
				this.newBackground = game.add.image( 0, 0, "forest" );
				this.newBackground.anchor.setTo( 0, 1 );
				this.newBackground.position.setTo( 0, game.height );
				this.grandma = game.add.image( 565, 420, "grandma" );
				this.player = game.add.image( 320, 500, "player" );
				game.world.sendToBack( this.newBackground );

				game.add.tween( this.newBackground ).to( { x: -900 }, 180000, "Linear", true, 1500 );
				game.add.tween( this.player ).to( { x: 310 }, 180000, "Linear", true, 1500 );

				typewriter.startDisplay( inForestStory, 0, new Phaser.Point( 100, 140 ), 30, stroke, strokeThick + 2 );
				//typewriter.startDisplay( [ [ "", child, true ] ], 0, new Phaser.Point( 100, 140 ), 30, stroke, strokeThick + 2 );
				this.part = 3; // Start game
			} else if ( typewriter.finished() && this.part == 3 ) {
				shared.story = true;
				game.state.start( "Game" );
				return true;
			}
			return false;
		}
	}
}

/**
 * @param {Phaser.Game} game
 * @param {TypeWriter} typewriter - An initialized instance of TypeWriter
 * @param {Phaser.Image} curBackground - Current background object
 */
function inEmptyRoom ( game, typewriter, curBackground ) {
	return {
		update: function () {
			if ( typewriter.finished() && this.started != true ) {
				typewriter.startDisplay( [
					[ TypeWriter.TYPE.SHAKE, 0.005, 500 ],
					[ TypeWriter.TYPE.WAIT, 1000 ],
					[ TypeWriter.TYPE.MESSAGE, "This door is locked...", child, true ]
				], 0, new Phaser.Point( 360, 420 ), 30, stroke, strokeThick );

				this.started = true;
			} else if ( typewriter.finished() && this.started ) {
				this.started = false;
				return true;
			}
			return false;
		}
	};
}

/**
 * @param {Phaser.Game} game
 * @param {TypeWriter} typewriter - An initialized instance of TypeWriter
 * @param {Phaser.Image} curBackground - Current background object
 */
function inGrandmaRoom ( game, typewriter, curBackground ) {
	return {
		update: function () {
			// If we are ready to display text and we haven't started
			// Display the story text
			if ( typewriter.finished() && this.started != true ) {
				// Quick flash to change the background
				game.camera.flash( 0x0, 500 );
				curBackground.alpha = 0;
				this.newBackground = game.add.image( 0, 0, "grandmaRoom" );
				game.world.sendToBack( this.newBackground );

				// Depending if this is first or second, we display different text
				if ( this.visit != true ) {
					typewriter.startDisplay( inGrandmaStory1, 0, new Phaser.Point( 150, 370 ), 30, stroke, strokeThick );
				} else if ( this.visit == true ) {
					typewriter.startDisplay( inGrandmaStory2, 0, new Phaser.Point( 150, 370 ), 30, stroke, strokeThick );
				}

				// Mark as we visited and has started
				this.started = true;
				this.visit = true;
			} else if ( typewriter.finished() && this.started ) {
				// If we are ready to display text and already started
				// Clean up and get out of here
				game.camera.flash( 0x0, 500 );
				curBackground.alpha = 1;
				this.newBackground.alpha = 0;
				this.started = false;
				return true;
			}
			return false;
		}
	}
}

/**
 * @param {Phaser.Game} game
 * @param {TypeWriter} typewriter - An initialized instance of TypeWriter
 */
function deathDoor ( game, typewriter ) {
	let part = 0;
	return {
		update: function () {
			if ( typewriter.finished() ) {
				game.camera.flash( 0x0, 500 )
				if ( part == 0 ) {
					this.newBackground = game.add.sprite( 0, 0, "bForest" );
					game.world.sendToBack( this.newBackground );
					typewriter.startDisplay( inBloodForestStory, 50, new Phaser.Point( 100, 140 ), 30, stroke, strokeThick );
					part = 1;
				} else if ( part == 1 ) {
					this.newBackground.destroy();
					this.newBackground = game.add.sprite( 0, 0, "momRoom" );
					game.world.sendToBack( this.newBackground );
					typewriter.startDisplay( [ [ "Haha... That can't be true", child, true ] ],
						0, new Phaser.Point( 100, 140 ), 30, stroke, strokeThick );

					part = 2;
				} else if ( part == 2 ) {
					this.newBackground.destroy();
					this.newBackground = game.add.sprite( 0, 0, "attic_de" );
					game.world.sendToBack( this.newBackground );
					typewriter.startDisplay( [
						[ "Grandma is waiting for me...", child, true ],
						[ "This is a lie...", child, true ],
						[ TypeWriter.TYPE.SHAKE, 0.05, 1000 ],
						[ "THis must be a lie!", child, true ],
					], 0, new Phaser.Point( 100, 140 ), 30, stroke, strokeThick );

					part = 3;
				} else if ( part == 3 ) {
					this.newBackground.destroy();
					return true;
				}
			}
		}
	};
}

let inGrandmaStory1 = [
	[ TypeWriter.TYPE.MESSAGE, "...", child, true ],
	[ TypeWriter.TYPE.MESSAGE, ".......", child, true ],
	[ TypeWriter.TYPE.WAIT, 1000 ],
	[ TypeWriter.TYPE.MESSAGE, "Grandma... ?", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "What happened to your room?", child, true ],
	[ TypeWriter.TYPE.WAIT, 500 ],
	[ TypeWriter.TYPE.MESSAGE, "Are you here Grandma?", child, true ],
	[ TypeWriter.TYPE.SHAKE, 0.01, 750 ],
	[ TypeWriter.TYPE.MESSAGE, "GRANDMA!", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "...", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "I guess Grandma's not in her room...\n", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "...", child, false ],
	[ TypeWriter.TYPE.MESSAGE, "I know! ", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "She is probably with mom in her room!", child, true ]

];

let inGrandmaStory2 = [
	[ TypeWriter.TYPE.MESSAGE, "Grandma?\n", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "Grandma is not in her room right now.", child, false ],
	[ TypeWriter.TYPE.MESSAGE, "She is probably with mom in her room.", child, true ],
];

let inMomRoomStory1 = [
	[ TypeWriter.TYPE.MESSAGE, "Mom? Are you in here?", child, true ],
	[ TypeWriter.TYPE.WAIT, 1000 ],
	[ TypeWriter.TYPE.MESSAGE, "Grandma?", child, true ],
	[ TypeWriter.TYPE.WAIT, 1000 ],
	[ TypeWriter.TYPE.SHAKE, 0.01, 750 ],
	[ TypeWriter.TYPE.MESSAGE, "Is anyone here?", child, true ],
	[ TypeWriter.TYPE.WAIT, 1000 ],
	[ TypeWriter.TYPE.MESSAGE, "Where is everyone?", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "Mom's not home...\n", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "Grandma's not home either...\n", child, false ],
	[ TypeWriter.TYPE.MESSAGE, "...", child, false ],
	[ TypeWriter.TYPE.MESSAGE, "Mom is always home though...", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "Are they playing hide and seek?", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "...", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "Mom! ", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "Grandma!\n", child, false ],
	[ TypeWriter.TYPE.MESSAGE, "I give up! Come out!", child, false ],
	[ TypeWriter.TYPE.WAIT, 1000 ],
	[ TypeWriter.TYPE.MESSAGE, "Mom... ?", child, true ],
	[ TypeWriter.TYPE.MESSAGE, "The TV is still on...", child, true ],
];

let inMomRoomStory2 = [
	[ TypeWriter.TYPE.MESSAGE, "What was mom watching?", child, true ],
];

let inForestStory = [
	// We are gonna use TypeWriter 2.5 :)
	[ "Grandma? What are you doing here?", child2, true ],
	[ "What are you doing out here in the forest\n\
	this early, honey?", grandma, true ],
	[ "That's what I am asking you Grandma!", child2, true ],
	[ "Is it now?", grandma, true ],
	[ " I'm just out here for a roll.", grandma, false ],
	[ "You know, Grandpa and I love this forest.", grandma, true ],
	[ "We used to come here every day.", grandma, true ],
	[ "What is there to even do out here?\n", child2, true ],
	[ "There's nothing but birds and trees out here!", child2, true ],
	[ "That is true, but you see...\n", grandma, true ],
	[ ".. even nothing but birds and trees can be nice.", grandma, false ],
	[ "Really?!?! That boring!", child2, true ],
	[ "Haha... Just like your Grandpa.", grandma, true ],
	[ "Really?", child2, true ],
	[ "Yup. Een though he would follow me out here,\n\
	he would always complain later\n\
	about how boring it is out here", grandma, true ],
	[ "See! Even Grandpa agrees!", child2, true ],
	[ "There's no way Grandpa would keep coming out here\n\
	if it is this boring!", child2, true ],
	[ "No he won't...", grandma, true ],
	[ "So why did he keep coming?", child2, true ],
	[ "Well... he decided to try flying...", grandma, true ],
	[ "Flying?\n", child2, true ],
	[ "That's impossible! Balls can't fly. We only roll around!\n", child2, false ],
	[ "I mean, sometime we can hop onto something,\n\
	but that's about it!", child2, false ],
	[ "That's true, but what if you can shoot something below\n\
	you to keep you in the air?", grandma, true ],
	[ "Grandpa was an adventurous ball. Whatever it is,\n\
	he always tries it out...", grandma, true ],
	[ "Oh!! So how did he did it?", child2, true ],
	[ "You really want to know?", grandma, true ],
	[ TypeWriter.TYPE.SHAKE, 0.005, 500 ],
	[ "Yes!", child2, true ],
	[ "... Well, this cannon here is used to shoots some\n\
	plastic balls up.", grandma, true ],
	[ "Then Grandpa would then try to land on the ball to\n\
	go higher...", grandma, true ],
	[ "Can we try it Grandma?", child2, true ],
	[ "What? NO", grandma, true ],
	[ "Please.", child2, true ],
	[ "...", grandma, true ],
	[ "Pretty please! Youc an control the cannon and I can be\n\
	in the air!", child2, true ],
	[ "Come on Grandma!", grandma, true ],
	[ "...\n", grandma, true ],
	[ "... Ok, but you will control the cannon instead.", grandma, false ],
	[ "Try not to go overboard ok?", grandma, true ],
	[ "OK!", child2, true ],
	[ "...\n", grandma, true ],
	[ "......\n", grandma, false ],
	[ TypeWriter.TYPE.WAIT, 250 ],
	[ "Grandpa... is this OK?", grandma, true ],
];

let inBloodForestStory = [
	[ TypeWriter.TYPE.SHAKE, 0.001, 10000 ],
	[ "Oh my god...", mom, true ],
	[ "What did I just saw...", mom, true ],
	[ "Did my child just...", mom, true ],
	[ "Oh ", mom, true ],
	[ "My ", mom, false ],
	[ TypeWriter.TYPE.SHAKE, 0.02, 250, true ],
	[ "God", mom, false ],
	[ TypeWriter.TYPE.SHAKE, 0.5, 500, true ],
	[ "AHHHHH!!!!", child2, true ],
	[ "GRANDMA!!!", child2, true ],
	[ TypeWriter.TYPE.SHAKE, 0.001, 60000, true ],
	[ "What... what... happened?\n", child2, true ],
	[ "Hey... ", child2, false ],
	[ "Hey! ", child2, false ],
	[ "HEY GRANDMA!\n", child2, false ],
	[ "why aren't you moving?\n", child2, false ],
	[ "hey grandma, what happen?\n", child2, false ],
	[ "hey grandma...\n", child2, false ],
	[ "grandma...", child2, false ],
	[ TypeWriter.TYPE.SHAKE, 0.5, 500, true ],
	[ "GRANDMA!!!", child2, true ],
	[ "Mom... ", mom, true ],
	[ TypeWriter.TYPE.SHAKE, 0.001, 10000, true ],
	[ "I'm sorry...", mom, false ]
];

/** @type {Array<string>} */
let roomStoryText = [
	"Anyone here?",
	"Mom?",
	"Grandma?",
	"Hello... ?",
	"Mom? Grandma?",
	"..."
]
