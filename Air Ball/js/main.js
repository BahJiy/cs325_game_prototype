"use strict";
var game = new Phaser.Game( 1000, 563, Phaser.Canvas, 'game' );
window.onload = function () {

	//	Create your Phaser game and inject it into the 'game' div.
	//	We did it in a window.onload event, but you can do it anywhere (requireJS load, anonymous function, jQuery dom ready, - whatever floats your boat)
	/** @type {Phaser.Game} */

	let WebFontConfig = {
		active: function () { game.time.events.add( Phaser.Timer.SECOND, null, this ); },
		google: {
			families: [ 'Inconsolata', 'Gloria Hallelujah' ]
		}
	};

	//	Add the States your game has.
	//	You don't have to do this in the html, it could be done in your Boot state too, but for simplicity I'll keep it here.

	// An object for shared variables, so that them main menu can show
	// the high score if you want.

	game.load.script( 'webfont', "//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" )
	game.state.add( 'Game', GameStates.makeGame( game ) );

	//	Now start the Boot state.
	game.state.start( 'Game' );

};
window.onbeforeunload = function ( e ) {
	game.world.removeAll();
	game.destroy();
};
