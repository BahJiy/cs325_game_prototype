"use strict"


/** @param {Phaser.Game} game */
GameStates.makeGame = function ( game, shared ) {
	/** @type {Phaser.Sprite} */
	let ball = null;
	/** @type {Object,<Phaser.Keyboard>} */
	let cursor = null;
	/** @type {Array<Ball>} */
	let arBall = [];
	/** @type {number} */
	let gameWait = 0;
	/** @type {number} */
	let pickWait = 17500;
	/** @type {Phaser.Sprite} */
	let cannon = null;
	/** @type {Phaser.Sprite} */
	let line = null;
	/** @type {Phaser.Point} */
	let screenCenter = 0;
	/** @type {{player: Phaser.Physics.P2.CollisionGroup, ball: Phaser.Physics.P2.CollisionGroup}} */
	let colGroup = { player: null, ball: null };
	/** @type {TypeWriter} */
	let typewriter = null;
	let done = 0;
	let back = null;

	class Ball {
		/**
		 *
		 * @param {Phaser.Game} game
		 * @param {number} x
		 * @param {number} y
		 */
		constructor ( game, x, y, angle ) {
			let ball = game.add.sprite( x, y, "ball" );
			ball.anchor = new Phaser.Point( 0.5, 0.5 );
			game.physics.p2.enable( ball, true );
			ball.body.data.gravityScale = 1.5;
			ball.body.mass = 10;
			ball.body.setCircle( ball.width / 2 );
			ball.body.collideWorldBounds = false;
			ball.body.setCollisionGroup( colGroup.ball );
			ball.body.collides( [ colGroup.ball, colGroup.player ] );

			let speed = 1700;
			let speedX = speed * Math.cos( angle );
			let speedY = speed * Math.sin( angle );
			ball.body.moveRight( speedX );
			ball.body.moveDown( speedY );
			let destroyTime = game.time.now + 2000;

			/**
			 * Check if ball is still in bounds
			 * If ball is out of bound, destroy the ball and
			 * return true to mark for deletion
			 * @param {Phaser.Rectangle} camRect
			*/
			this.update = function () {
				if ( game.time.now > destroyTime ) {
					ball.destroy();
					return true;
				}
				return false;
			}
			this.rBall = ball;
		}
	}

	class PickUp {
		/**
		 *
		 * @param {Phaser.Sprite} ball
		 * @param {Phaser.Game} game
		 */
		constructor ( ball, game ) {
			let pick = game.add.sprite( game.width - game.width * Math.random(), ball.y - Math.random() * 500 - 500, "pickup" );
			this.update = function () {
				if ( new Phaser.Rectangle( ball.x, ball.y, ball.width, ball.heal ).intersection(
					new Phaser.Rectangle( pick.x, pick.y, pick.width, pick.heal ) ) ) {
					ball.body.moveLeft( 250 * Math.random() );
					ball.body.moveDown( 250 * Math.random() );
					pick.destroy();
					return true;
				}
				return false;
			}
		}
	}

	return {
		preload: function () {
			game.load.image( "ball", "assets/ball.png" );
			game.load.image( "cannon", "assets/cannon.png" );
			game.load.image( "grandma", "assets/grandma.png" );
			game.load.image( "forest", "assets/forest.jpg" );
			game.load.image( "pickup", "assets/pickup.png" );
			game.load.image( "line", "assets/line.png" );
			game.load.script( 'webfont', "//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" )
		},
		create: function () {
			game.physics.startSystem( Phaser.Physics.P2JS );
			game.physics.p2.setBounds( 0, -9500, game.world.width, game.height + 9500 );
			game.physics.p2.restitution = 0.2;
			game.physics.p2.gravity.y = 300;
			game.physics.p2.applySpringForces = true;
			game.physics.p2.updateBoundsCollisionGroup();
			colGroup.player = game.physics.p2.createCollisionGroup();
			colGroup.ball = game.physics.p2.createCollisionGroup();

			back = game.add.image( 0, 0, "forest" );
			back.position = new Phaser.Point( 0, -( back.height - game.height ) );
			//back.position = new Phaser.Point( 0, 0 );

			cannon = game.add.sprite( 0, 0, "cannon" );
			cannon.anchor = new Phaser.Point( 0.5, 0.5 );
			cannon.position = new Phaser.Point( game.world.centerX, game.world.height - 50 );
			cannon.angle = -90;
			line = game.add.sprite( cannon.x, cannon.y, "line" );
			line.anchor = new Phaser.Point( 0.5, 0.5 );

			ball = game.add.sprite( 500, game.world.height - 500, "grandma" );
			ball.anchor = new Phaser.Point( 0.5, 0.5 );
			game.physics.p2.enable( ball, true );
			ball.body.setCircle( ball.width / 2 );
			ball.body.gravityScale = 1.5;
			ball.body.collideWorldBounds = true;
			ball.body.setCollisionGroup( colGroup.player );
			ball.body.collides( colGroup.ball, ( obj1, obj2 ) => { console.log( "HIHIHIT" ); }, this );
			ball.body.moveUp( 1000 );


			game.camera.follow( ball, 0, 0, 0.2 );
			game.camera.deadzone = new Phaser.Rectangle( 0, 50, game.world.width, game.world.height - 250 );
			game.camera.bounds = new Phaser.Rectangle( 0, -95000, game.world.width, game.height + 95000 );

			cursor = game.input.keyboard.createCursorKeys();
			cursor[ "space" ] = ( game.input.keyboard.addKey( Phaser.Keyboard.SPACEBAR ) );
			game.input.keyboard.addKeyCapture( Phaser.Keyboard.SPACEBAR );

			typewriter = new TypeWriter( game, "Gloria Halleluja" );
		},

		render: function () {
		},

		update: function () {
			screenCenter = new Phaser.Point( game.world.centerX, ( game.camera.position.y + game.camera.height ) / 2 );
			//cannon.angle = cannon.position.angle( screenCenter, true );
			let removeAr = [];
			arBall.forEach( ele => {
				if ( ele.update() ) {
					removeAr.push( arBall.indexOf( ele ) );
				} else {
					ball.body.collides( ele.rBall );
				}
			} );
			removeAr.forEach( ele => {
				arBall.splice( ele, 1 );
			} );
			let scale = 1;
			cannon.alpha = 1;
			if ( ball.y < 0 ) {
				scale = 1 - ( Math.abs( ball.y ) / 10000 );
				scale = scale < 0.2 ? 0.2 : scale;
				cannon.alpha = 0;
			}
			if ( ball.y < game.world.height ) {
				game.camera.scale.y = scale;
				game.camera.scale.x = scale;
				cannon.y = ball.y + game.world.height - 250;
				if ( cursor.left.isDown && cannon.angle > -180 ) {
					cannon.angle -= 2;
				} else if ( cursor.right.isDown && cannon.angle < -10 ) {
					cannon.angle += 2;
				} if ( cursor.space.isDown && game.time.now > gameWait ) {
					gameWait = game.time.now + 1000;
					arBall.push( new Ball( game, cannon.x, cannon.y, cannon.rotation ) );
				}
				line.y = cannon.y;
				line.angle = cannon.angle;
				if ( game.time.now > pickWait ) {
					pickWait = game.time.now + 500 * Math.random() + 10000;
					arBall.push( new PickUp( ball, game ) );
				}
			} else {
				if ( done == 0 ) {
					game.camera.fade( 0x0, 1000 );
					gameWait = game.time.now + 1000;
					done = 1;
				} else if ( gameWait < game.time.now && done == 1 ) {
					back.destroy();
					line.destroy();
					game.physics.destroy();
					game.camera.flash( 0x0, 500, true );
					typewriter.startDisplay( [
						[ TypeWriter.TYPE.MESSAGE, "And so the ball falls...", "#FF5090", true ],
						[ TypeWriter.TYPE.MESSAGE, "And it falls...", "#FF5090", true ],
						[ TypeWriter.TYPE.MESSAGE, "Ever since then, no one found the ball", "#FF5090", true ],
						[ TypeWriter.TYPE.MESSAGE, "Even in death, the ball probably still reject who it is...", "#FF5090", true ],
						[ TypeWriter.TYPE.MESSAGE, "No matter how many time those around told it", "#FF5090", true ],
						[ TypeWriter.TYPE.MESSAGE, "It is a ball yet it tries to fly.", "#FF5090", true ],
						[ TypeWriter.TYPE.MESSAGE, "Maybe this will be a lesson for the other balls", "#FF5090", true ],
						[ TypeWriter.TYPE.MESSAGE, "Don't forgot, you are a ball, not a bird. Don't forgot who you are", "#FF5090", true ]
					], 0, new Phaser.Point( 214, 190 ), 25 );
					done = -99;
				}
				typewriter.update();
			}
		}
	};
}
